\section{Cloud Computing and Virtualization}

Cloud computing, as we understand and use the term today,
did not even exist 10 years ago.
When Amazon, until then primarily known as an online book shop,
started selling its excess compute capacity over the internet in 2006,
it started a new trend that has shaped the industry since then and is now a billion dollar business~\footnote{\url{http://bits.blogs.nytimes.com/2014/08/03/cloud-revenue-jumps-led-by-microsoft-and-ibm/}}.

Cloud computing, essentially, is the delivery of IT resources over the internet with a pay-per-use accounting model.
While Amazon originally may have only been interested in improving the return on investments of its IT infrastructure,
which is sized to cope with massive sales on two occasions per year, Black Friday and Christmas,
Cloud computing is now an entire new branch within the IT industry.
Although the benefits are sometimes hard to quantify,
for example, if and when it reduces the IT related costs,
they are obvious in other cases:
a few years ago, the New York Times newspaper converted full page images of its newspapers from 1851 to 1922 into PDF files within a few hours~\footnote{\url{http://open.blogs.nytimes.com/2007/11/01/self-service-prorated-super-computing-fun/}}.
This example illustrates two often brought forward benefits of cloud computing:
(a) flexibility and (b) cost savings.
With cloud computing, compute resources can be acquired within minutes and compared to purchasing hardware for a one-off job it is also cheaper.
Other, less tangible benefits, include increased reliability and higher cost efficiency due to consolidation.
Instead of each small enterprise relying on half a person to care for the IT,
the responsibility is outsourced to a big cloud provider,
where a handful of people reliably run thousands of servers.

\begin{marginfigure}[5cm]
\includegraphics{figs/intro/iaas-paas-saas}
\caption[Cloud service delivery models]{The three major cloud service delivery models:
Infrastructure as a Service~(Iaas), Platform as a Service~(PaaS), and Software as a Service~(SaaS).}
\label{figs:intro:iaas-paas-saas}
\end{marginfigure}

\newthought{Under the umbrella of cloud computing} three similar,
but distinct offerings have developed.
They are distinguished by the abstraction level at which resources are rented.
With \emph{Infrastructure as a Service} resources are delivered in the form of virtual machines.
The customer has complete control over the VMs to set up his software infrastructure exactly as he desires.
With \emph{Platform as a Service} the customer gets access to an application framework in which a particular service is developed.
The provider takes care to scale the underlying resources as needed.
This frees the customer from having to configure and deploy virtual machines.
Instead, he can focus on developing the application layer logic.
\emph{Software as a Service} is the highest layer of cloud computing services.
Typical examples are web mailers and customer relationship management~(CRM) software.
The service is delivered over the internet and anyone with a modern web browser can use it.
Fees are then charged per user, typically in a subscription-style contract, i.e.,
monthly payments must be made to be able to continue to use the service.
In this thesis we exclusively focus on the Infrastructure as a Service delivery model,
which greatly benefits from hardware virtualization.

\newthought{An important part} of the infrastructure cloud is its scheduler.
Customers specify exactly what kind of virtual machine they want,
i.e., number of CPUs, amount of memory, and disk space,
before submitting their request.
The cloud provider then must decide on which physical machine to place the virtual machine.
Entire PhD theses have been written on the subject~\cite{beloglazov2013energy} and we revisit the problem in \hyperref[chapter:lease-times]{Chapter 3}.
Our central research question is if and how the scheduling for infrastructure clouds can be optimized knowing the VM's reservation time.
Among the many parameters a cloud customer has to specify for a VM,
the intended lease or reservation time is not among them.

\subsection{Hardware Virtualization}

An important enabling technology that goes hand in hand with cloud computing is \emph{hardware virtualization}.
Although existing in some form or another since the early 1960s,
for example, in IBM mainframe computers,
modern day virtualization technology only became widely available in the early 2000s.
With hardware virtualization, it is possible to create the illusion of a dedicated physical machine,
while in fact the \emph{virtual machine} runs alongside other virtual machines on shared hardware.
The ability to create custom-sized virtual machines,
i.e., vary the processor count and amount of memory freely,
enables the cloud provider to ``chop up'' powerful servers into numerous smaller units,
which are rented out to individual cloud customers.

Because of the ability to run multiple virtual machines securely isolated from one another on the same hardware,
virtualization enables high consolidation ratios.
Previous deployments of one service per server to achieve isolation and minimize interference between different software components,
can these days be realized with much less hardware,
due to virtualization.

Today, there are many different virtualization products on the market:
VMware, Xen, QEMU/KVM, VirtualBox, and HyperV, are probably the most popular ones.
Some of them are commercial products, while others are freely available for download.
Besides the virtualization software itself,
the accompanying tools to manage a fleet of physical and virtual servers is what sets the different solutions apart.
As part of this thesis,
we worked with QEMU/KVM because its source code is available to download and modify.

\newthought{With all the benefits} there are, of course, also downsides.
The degree with which cloud computing has permeated the IT industry becomes especially visible when a large cloud computing provider fails.
As a result of the outage, many of the relying companies with inadequate fail-over strategies will not be reachable anymore.
While outages are less frequent,
because the cloud providers have a vested business interest in making the infrastructure as reliable as possible,
they are, at the same time, more visible.
When a ``cloud'' fails, the outage affects many of its customers simultaneously.
Also, the cloud does not relief the system designer from incorporating fault tolerance into the deployed applications~\cite{ford2012icebergs}.
Large cloud providers now routinely offer multiple \emph{availability zones},
that provide certain degrees of fault independence.
If one data center goes offline, be it due to squirrels, hunters, or lightning~\footnote{\url{http://www.wired.com/2012/07/guns-squirrels-and-steal/}},
only resources within the affected availability zone become unavailable.
Hence, while reliability may generally be higher in the cloud,
the failure probability is still non-zero.

\subsection{Virtual Machine Migration}

The execution of multiple securely isolated operating systems on the same physical hardware is only one benefit of hardware virtualization.
Having another layer between the operating system and the hardware also eases the task of managing the (virtual) infrastructure.
An important selling point of virtualization is the convenient relocation of VMs between different physical machines,
also called \emph{virtual machine migration}.
Virtual machines can be migrated for any number of reasons, for example,
to vacate a host for hardware/software upgrades, to perform load balancing, or resolve resource hot spots.
\citeauthor{vogels2008beyond}~\cite{vogels2008beyond} succinctly summarized these and other use cases of virtualization.

\newthought{The conceptual predecessor} to virtual machine migration is \emph{process level migration}~\cite{douglis1987transparent}.
The main difference, as the terms already suggest,
is the execution context's level of abstraction at which migration occurs.
This naturally reflects on the challenges involved with the migration process.
For process level migration, there is typically a problem with residual dependencies to the source host.
Open resource descriptors, such as file handles,
must remain valid on the migration target.
Access to these resources is then typically forwarded to the original host,
restricting the applicability of process migration to temporary scale-out scenarios.

One of the earliest works in the area of virtual machine migration is by \citeauthor{sapuntzakis2002optimizing}~\cite[-2.5cm]{sapuntzakis2002optimizing}.
Users can switch locations, for example, moving between home and office,
while their personal ``capsule'' moves with them.
Independent of the user's physical location,
their virtual workspace follows them to provide a consistent experience.
To make the migration of capsules practical,
\citeauthor{sapuntzakis2002optimizing} introduced many ideas now taken as granted in VM migration implementations, i.e.,
memory ballooning, compressing memory pages for transfer, copy-on-write disks, and on demand page fetching.

Soon, further virtualization solutions became available~\cite[-3.5cm]{barham2003xen} and showed the viability of migrating a \emph{running} VM instance~\cite[-1cm]{clark2005livemigration}.
While the capsules were suspended during migration,
also called \emph{offline} or \emph{cold migration},
\emph{live migration} moves VMs without shutting them down.
Overall, the VM's unavailability is small,
sometimes as low as a few hundred milliseconds for amenable workloads.

\newthought{Since then, numerous academic works} have taken up the migration topic and enhanced the basic technique in various ways:
for example, to reduce the volume of data transferred during migration,
compression can be used~\cite[-2.2cm]{jin2009adaptive, svard2011evaluation}.
Others have proposed to replace the original \emph{pre-copy} live migration strategy by a \emph{post-copy} approach.
During pre-copy migration,
the virtual machine's state is copied to the destination in rounds.
This is necessary, because the VM continues to run on the source while the migration is in progress.
In the course of its normal operation,
the VM will update memory pages that have already been transferred to the destination,
invalidating the destination's copy of the page.
To synchronize the source with the destination,
the migration algorithm must track the page modifications at the source.
During the next round, all the pages that have been updated during the last round,
must be copied again.
To make sure the migration eventually finishes,
the number of possible rounds is bounded.
During the last round, the VM is paused, the remaining pages transferred,
and execution resumed at the destination.

% \begin{figure}
%   \includegraphics{figs/pre-copy-migration}
%   \caption{Pre-copy migration algorithm}
%   \label{fig:pre-copy-migration}
% \end{figure}

Post-copy migration proposes to switch execution immediately to the destination and transfer data on-demand~\cite{hines2009post, hirofuchi2010enabling}.
Whenever the VM accesses not-yet-copied state,
it is briefly paused while the missing state is fetched from the source.
The benefit is that each page is copied exactly once,
reducing the overall data volume to the theoretical minimum.
However, the frequent short interruptions, due to fetching state from the source,
reduce the VM's performance on the destination host initially.

A third alternative, besides pre- and post-copy, is replay-based migration~\cite{liu2009trace}.
The source creates a consistent checkpoint of the VM and traces modification events.
At the destination, the checkpoint and event trace are combined to re-create the VM's state.
Multiple rounds of tracing and replay may be necessary before the state at the source and destination converge,
as is the case with pre-copy migration.
Trace-based migration reduces the overall transfer volume because traces capture high-level events instead of their low-level manifestation in memory.
The downside is the overhead imposed by tracing relevant system-level events,
which was acceptable for the investigated workloads according to the authors.

%% textwidth in cm: \printinunitsof{in}\prntlen{\textwidth}

%% although the time depends on the \emph{dirty rate}, i.e.,
%% the rate at which processes within the VM cause memory writes.

\newthought{Irrespective of how migration is implemented} in a specific hypervisor,
researchers have used migration as a mechanism for solving various related problems:
for example, to resolve hot spots that may occur when over-committing resources~\cite{wood2007black}.
As one use case of virtualization is to consolidate services that previously were running on individual servers,
it is tempting to push this idea further and run more VMs than is strictly allowed by the hardware resources.
The underlying assumption is that the individual VMs rarely use all their allocated resources simultaneously,
because the allocated resources in sum exceed the physically available resource.
If this should happen, the physical server becomes a \emph{hot spot}.
By migrating VMs to less loaded servers, the hot spot is resolved.

\newthought{VM migration between data centers} is even more challenging.
Because the network topology changes, i.e., the source and target network are separate IP subnets,
another layer of indirection to refer to the VM is required, e.g., a domain name.
Further adaptations are necessary because the migration logic can no longer rely on shared storage:
if the source and destination hosts are located in different data centers,
they likely do not have access to the same persistent storage pool.
Whereas previously only the guest's memory state had to be transferred,
now the persistent storage must be moved too.
The migration overhead can be reduced by eliminating redundancy~\cite{wood2011cloudnet} and exploiting similarity between VMs when migrating entire virtual clusters~\cite{riteau2011shrinker}.
Virtual private networks~(VPNs) can provide seamless network connectivity across geographically distributed locations.

%% how many reservations back to back? r1.end == r2.begin?
%% r1.begin - r2.end < 1 hour? <= 24 hours? > 24 hours (long tail)?

%% \cite{wood2011cloudnet} migrating virtual machines between data centers connected via wide area networks.
%% Besides dealing with the ``state problem'' also proposes a solution regarding a seamless network connectivity.

%% \cite{haselhorst2011efficient} migrating storage and memory state.

%% \cite{riteau2011shrinker} optimizes the migration of a virtual machine cluster over wide area networks.

%% \cite{jin2009adaptive} use adaptive, varying with similarity, memory
%% compression to reduce the migration data volume.

%% \cite{svard2011evaluation} use delta compression, only transmit
%% differences between consecutive transfer rounds instead of whole pages.

%% \cite{voorsluys2009cost} reporting experimental results for live migration of three-tiered web service.
%% Observed multi-second unavailability of the service.

%% \cite{hines2009post} replaced pre-copy live migration with post-copy live migration.
%% Post-copy transfers execution to the destination very early in the migration process.
%% Missing pages are copied from source on-demand and in the background terminate migration eventually.

%% \cite{hirofuchi2010enabling} describes a post-copy live migration implementation on qemu/kvm.

%% \cite{checconi2010real} investigating live migration in the context of soft real time environments.
%% Migration must finish until a certain point in time.
%% Resources must be reserved prior to the migration to meet the deadline.

%% \cite{akoush2010predicting} modeling of live migration performance.
%% Link rate and page dirty rate are the two major factor impacting migration behavior.

%% \cite{breitgand10cost} metrics other than the total migration duration are important too, i.e.,
%% want to minimize SLA violations for the applications running inside the VM.
%% Proposes cost model and algorithms to ensure SLA during migration.

\subsection{Virtualization-aided Energy Savings}

Although not strictly related to energy savings in and around the data center,
similar approaches based on consolidation and VM migration have been pursued for clusters of desktop machines.
As the PC is now a pervasive tool in offices of enterprises, government agencies, and universities,
there is a potential to power off machines when they are not used, i.e., over night.
While leaving the desktop machines powered on over night may, at least partly,
be an issue of habit and workplace culture,
having an unobtrusive and automated power management solution is clearly preferable.

As opposed to the data center, where stringent request latency requirements prevent the use of coarse grained full-system low-power idles modes,
they are very well applicable within a cluster of desktop machines.
Idle intervals last on the order of minutes to hours, for example, during lunch or over night.
One typical approach is to virtualize the desktop and live migrate idle desktop VMs to a centralized server~\cite{bila2012jettison}.

Forgoing virtualization, alternative solutions propose to create a minimal proxy for each desktop~\cite{agarwal2010sleepserver}.
The proxy consumes significantly fewer resources than a fully virtualized desktop,
while its main job is to maintain the network presence of the original desktop.
The proxy participates in basic network protocols such as ARP,
extending the periods during which the desktop is turned off.
Whenever the proxy cannot handle a request,
it wakes up the desktop and forwards the network packets such that the desktop can process them.

Previous proposals included making network cards more intelligent such that they can handle packets without waking up the host~\cite{agarwal2009somniloquy}.
The drawback of this approach is its reliance on custom hardware,
which does not yet exist.
Moving state and computation onto the network card also does not solve the problem of reintegrating the state,
when the host is finally woken up again.

For small to medium sized enterprises with a few hundred desktops,
the savings due to energy costs can easily reach thousands of USD/EUR.
However, the cost/benefit calculation usually never involves additional costs imposed by maintaining the additional hardware, e.g.,
servers to consolidate the desktops on,
nor any labor costs, such as paying the developer to implement such a system.
Those costs may well nullify any energy-related savings.

\subsection{Cloud and Virtualization Summary}

Cloud computing as a new paradigm has changed the way in which computing is done.
The flexibility and ease to acquire and release resources on-demand is truly great for the customers.
Because cloud computing is such a large industry now,
it makes sense to focus our energy efficiency efforts on this particular kind of computing.
The requirements and peculiarities are different enough from request-parallel workloads to allow full-system power modes to be applied.

Further, we highlighted the role hardware virtualization plays within the cloud ecosystem.
Ubiquitous and performant hardware virtualization was an enabling technique for today's clouds.
Along with virtualization came related technologies to manage the ever growing number of virtual machines.
Most prominent is the ability to migrate VMs between servers,
for example, either to vacate the server for maintenance or to dynamically rearrange VMs according to their current use of resources.
VM migration is also used to save energy in office environments,
by consolidating idle virtualized desktop machines on central servers.
Some of the ideas from this space have inspired our work on truly on-demand cloud resources in \hyperref[chapter:dreamserver]{Chapter 5}.
Instead of idle desktop machines,
we target idle virtual machines in cloud data centers.
