\section{Problem}
\label{sec:problem}

%% paragraph about terminology
%% suspend vs resume, save vs restore, hibernate, suspend-to-ram

%% \subsection{Motivating example}

\begin{figure}
  \includegraphics{figs/dream/interarrival-cdf}
  \caption[Request inter-arrival time]{Request inter-arrival time for three exemplary web services.
Idle periods of tens of seconds up to tens of minutes do exist.
}
  \label{fig:dream:interarrival-cdf}
\end{figure}

We start with a motivating example for web services which would benefit from more flexibility with respect to resource deployment in the cloud.
To make our case,
we use publicly available web server traces~\cite{arlitt1996web} as well as traces collected by us.
The Calgary and Dresden trace are from department-level web servers,
while the Saskatchewan trace represents data from a campus-wide web server.
Each trace includes a time stamp indicating when the web server received the request.
Based on the time stamp, we can determine the inter-arrival time distribution,
which forms the basis of our analysis.
% The time stamps have a resolution of one second,
% which is sufficient for our analysis.
Table~\ref{tab:web-server-traces} summarizes,
for three exemplary traces from our collection,
the length of each observation period and the total request volume.
The three traces are interesting because they illustrate different inter-arrival time distributions.
The trace periods range from 214 to 353 days,
with a total request volume of between 0.7 and 4.2 million.


\begin{margintable}
  \begin{center}
    \begin{tabular}{l|r|r}
      Name & Period & Requests \\
           & [days] & [$10^{6}$] \\
      \hline
       Dresden      & 318 & 4.2 \\
       Saskatchewan & 214 & 2.4 \\
       Calgary      & 353 & 0.7 \\
    \end{tabular}
  \end{center}
  \caption[Web server trace attributes]{Trace length and total number of requests for three exemplary web services.}
  \label{tab:web-server-traces}
\end{margintable}

% For our proposed scheme to work,
% there must exist periods of inactivity, i.e., where no request is processed.
Examining the traces, we observe that requests are sometimes spaced minutes apart.
During those times, the service performs no useful work and may as well be disabled,
saving the customer money and potentially allowing the provider to re-allocate resources.
% The idle periods for each trace of our example can be gauged by looking at the inter-arrival time distribution (Figure~\ref{fig:dream:interarrival-cdf})
Figure~\ref{fig:dream:interarrival-cdf} visualizes the inter-arrival time distribution for each of the three traces.
Only looking at the inter-arrival time distribution, we can already approximate the ``idleness'' of each server.
While close to 92\% of the requests for the Dresden trace have an inter-arrival time of 10 seconds or less,
which are too short to exploit,
it also means that 8\% of the requests arrive with a gap of more than 10 seconds.
The Saskatchewan and Calgary traces have 19\% and 35\% of their requests more than 10 seconds apart.
% This is in stark contrast to the other two traces where less than 40\% of the requests arrive within one second of each other.
A small percentage of requests shows even larger inter-arrival times.
For example, 15\%, 5\%, and 2\% of the requests have inter-arrival times of more than 100 seconds for the Calgary, Dresden, and Saskatchewan trace, respectively.
% The Saskatchewan trace is noteworthy because only a small percentage, XX \%, of requests have inter-arrival times of more than 60 seconds.

\begin{figure}
  \includegraphics{figs/dream/timeout-vs-savings}
  \caption[Aggregated idle time]{Accumulated idle time for three web services. Higher is better. More aggressive, i.e., lower, timeouts increase the total idle time.}
  \label{fig:dream:timeout-vs-savings}
\end{figure}

While the inter-arrival time distribution already hints at the potential to suspend idle services between requests,
the savings are more apparent when accumulated over longer intervals.
We define \emph{idle time} as the time between two requests minus a predefined timeout value.
For example, assuming a timeout value of 60 seconds and an inter-arrival time of 100 seconds, the resulting idle time would be 40 seconds.
During the idle time, the service is suspended and does not incur any costs for the cloud customer.
Figure~\ref{fig:dream:timeout-vs-savings} shows the idle time, accumulated over the entire trace period,
for varying timeout values.
% We accumulate the idle time for the entire trace period while varying the timeout values.
% The results are shown in Figure~\ref{fig:dream:timeout-vs-savings}.
Even with a conservative timeout of 60 seconds,
the Calgary trace exhibits 65\% idle time across the duration of the trace.
While the Calgary trace is an example of a service that fits particularly well with our proposed strategy,
even the Dresden trace shows an accumulated idle time of 45\%, i.e., 150 days over its trace period of 318 days.
The 45\% idle time is even more impressive considering that the Dresden trace contains 6x more requests than the Calgary trace during a shorter trace period.
The Saskatchewan trace exhibits only marginal benefits from periodically suspending the service.
Even with an aggressive 60 second timeout,
the Saskatchewan server could only be suspended for roughly 10\% of the time.
That is OK though, because not every service exhibits the request patterns that we are targeting with {\sn}.

%% \begin{table}
%%   \begin{center}
%%     \begin{tabular}{l|r|r}
%%       Name & Period [days] & Requests [$10^{6}$] \\
%%       \hline
%%        Dresden      & 318 & 4.2 \\
%%        Saskatchewan & 214 & 2.4 \\
%%        Calgary      & 353 & 0.7 \\
%%     \end{tabular}
%%   \end{center}
%%   \caption{Trace length and total number of requests for three exemplary web services.}
%%   \label{tab:web-server-traces}
%% \end{table}

%% Billing period
Our strive for more flexible on-demand cloud offerings is in line with the recent move towards finer granular accounting intervals by the major cloud providers.
Ever since Amazon started to rent its infrastructure in 2006,
the standard billing period was one hour.
While low volume services may have hour-long idle periods,
this coarse granular billing reduces achievable savings.
However, we are convinced that the default one-hour billing cycle will soon be replaced by more fine-grained charging intervals.
For example, Google\footnote{\url{http://goo.gl/5zPQrt}}
%% http://googlecloudplatform.blogspot.de/2013/05/ushering-in-next-generation-of.html
and Microsoft\footnote{\url{http://goo.gl/NZEOLN}}
%% http://weblogs.asp.net/scottgu/archive/2013/06/03/windows-azure-announcing-major-improvements-for-dev-test-in-the-cloud.aspx
have already announced that they would charge customers of their respective cloud offerings by the minute.
Other researchers have already speculated whether the current model of
selling fixed-size resource bundles by the hour may soon be replaced
by a market where individual resources are traded freely and acquire/release cycles are even shorter~\cite{ben2012resource, agmon-ben-yehuda2014ginseng}.

\newthought{The techniques to detect idleness} are not limited to virtual machines running in the cloud,
but can equally well be applied to physical machines in home and office networks, as well as, server clusters.
According to a study by \citeauthor{nedevschi2009skilled}~\cite{nedevschi2009skilled} there is a large potential to save energy by powering down idle desktop machines.
In their study, the machines were actively used less than 10\% of the time, on average.
We made similar observations of low overall utilization values for our research group's 50 machine cluster.
A controlled and automated solution to power down idle machines,
and resume them transparently on activity,
would enable significant power savings without manual intervention.
As presented later on, our novel mechanism to determine idleness at the network layer with Software Defined Networking is one possible solution.
It can be used either to conserve resources, for example, by suspending idle virtual machines,
or for automated power management of a server cluster.
We explore the former in the following sections,
leaving the latter for future work.

%% Cost saving example
%% Truly on-demand services help save money and possibly energy too.
%% For example, Amazon being one popular cloud provider charges \$0.06 per compute hour for a small (S) standard instance\footnote{Small Linux instance in US East}.
%% Consider we rented a server at \$0.06 per hour for the duration of 318 days as with the Dresden trace.
%% This would cost 318 days * 24 hours/day * 0.06 \$/hour = \$457.92.
%% Further assuming a strategy that decommissions the instance after 60 seconds of inactivity,
%% for f in *-interarrival ; do echo -n ``$f `` ; sort -n < $f | uniq -c | awk 'BEGIN{s=0;to=60}{if($2>=to){s+=$1*($2-to)}}END{print s, ``sec'', s/60., ``min'', s/60./60., ``hrs''}' ; done
%% gives 3443.59 hours of accumulated idle time
%% we would be able to save \$206.58 or 45\% of the overall cost.

%% Amazon currently bills per hour, but other providers already have 5-minute billing cycles

%% Environment
%% We intend {\sn} to manage a set of virtual machines, each proving one web service.
%% We do not intend to compete with plain web hosting providers that would use different techniques to service a large number of customers with virtually identical needs.
%% Pure hosting providers have no need to run in a virtualized environment.
%% More lightweight techniques, e.g., one web server process or Linux container per customer,
%% can be used to provide isolation.
%% This is possible because for customers of web hosting, the main requirement is to have HTML pages served quickly and reliably.
%% They are not interested in the technologies used to achieve this goal.

%% A more demanding group of customers may want to set up a web application, which can no longer be served by a simple hosting provider.
%% Instead, an application framework, such as Google App Engine or Amazon Beanstalk, may suite the customers' needs.
%% However, although the frameworks offer easy ways to deploy and manage applications,
%% they also force customers to use whatever language and environment the framework uses.

%% By using a cloud infrastructure provider, customers can use every combination of technologies they are most comfortable with.
%% It also allows customers to run their unmodified legacy services in a pay-as-you-go environment.
%% Porting legacy applications to a new web application framework is costly and unnecessary when using infrastructure providers.
%% This is the kind of customers we expect to see on platforms managed by {\sn}.
%% We primarily focus on web services as we expect more and more services to be offered over HTTP.
%% There is an increasing number of services accessible via REST-ful APIs over the Internet.
%% These services would be well supported by {\sn}, which works with HTTP on the application layer.

%% Summary
To summarize, the goal is to suspend virtualized services when they are idle and swiftly resume them on the next incoming request.
In a cloud computing setting this saves the customer money,
as he only pays for the time when the VM is actually up.
The provider's motivation is to be able to host a larger number of services using fewer physical resources:
suspended VMs do not occupy precious resources, such as RAM,
which the provider can use for other, active VMs instead.

To realize this idea, we need a mechanism to reactivate virtual machines within a short time frame, ideally sub-second,
and to monitor the virtual machine activity.
The next section describes our {\sn} architecture and how we achieved those goals.

%% can have a graph showing monetary savings.

%% what exactly is the problem we are solving?
%% optimizing the resume speed of virtual machines?
%% how best to distribute them between machines?

%% must also consider pre-paid instances
%% shared hosting providers
%% ... may have to change the story, if this makes us look bad

% On-demand web services.
% Resurrect virtual machine whenever there is an incoming request

% \cite{arlitt1996web}

