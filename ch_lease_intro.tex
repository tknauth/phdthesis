\section{Introduction}
\label{sec:lease:intro}

Chapter~\ref{chapter:cluster-trace} looked at the usage of physical servers within a research cluster.
The cluster's users expressed their demand for resources by reserving machines in a shared calendar.
This approach works reasonably well at the small scale of a single research group.
In a commercial setting, with potentially thousands of customers,
the infrastructure provider uses more sophisticated means to coordinate access to its compute infrastructure.
Which brings us directly to the topic of this chapter:
cloud computing and scheduling within infrastructure clouds.

\newthought{Cloud providers} are typically categorized into three groups,
based on the abstraction level at which they offer their resources.
\emph{Infrastructure} providers, also called Infrastructure as a Service~(IaaS) providers,
supply resources in the form of virtual machines.
For completeness, the two other groups are called \emph{Platform as a Service~(PaaS) providers}, for example, Amazon Beanstalk,
or \emph{Software as a Service~(SaaS) providers}, for example, Google's e-mail service Gmail.

The distinction in how resources are provided is important because it directly affects how the resources are accounted for.
IaaS provides resources in a generic fashion as network-accessible virtual machines,
where customers pay for the individual resources based on time, for example, CPU,
or actual usage, for example, network bandwidth.
On the other hand, platform providers require their customers to develop applications within a specific framework.
PaaS charges are based on higher-level metrics,
such as the number of processed requests.
Software as a Service providers offer whole applications over the internet and typically operate on a subscription basis.
For example, each user of an SaaS offering pays a fixed monthly usage fee.

%% The onus of configuring and adapting the virtual machine is on the customer.
%% The IaaS provider only supplies the resources and assigns the virtual machine~(VM) instance to a physical machine~(PM).

In this work we focus exclusively on IaaS providers because the virtual machine abstraction and the explicit starting and stopping of virtual machines within an infrastructure cloud make it easy to determine a system's utilization state.
A physical machine is \emph{busy} when it is hosting at least one virtual machine.
Otherwise, the machine is \emph{idle}.
In a sense, the workload serviced by an IaaS cloud,
suits the on/off model of relatively coarse-grained full-system low-power idle states perfectly.
Because the infrastructure exclusively hosts virtual machines,
as soon as all virtual machines running on an individual server have terminated,
the physical machine can transition into a low-power idle mode.

%% More generally, we are concerned with online scheduling of compute tasks.
%% Each task specifies a set of resource demands which the scheduler must map to the available resources.
%% In addition, each task also specifies its intended runtime.
%% Cluster schedulers use the runtime estimate to improve their planning.
%% For our analysis we assume that task never exceed their initial runtime estimate.
%% If a task is not finished after its allotted time passed,
%% the scheduler forcefully terminates the task.

Our study is motivated by the growing popularity and availability of infrastructure management software.
OpenStack, OpenNebula, and Eucalyptus are three popular frameworks that have significantly lowered the bar to deploy in-house private infrastructure clouds.
Multiple departments within an organization can share the previously fragmented IT resources.
The same reasoning that motivates customers to use, for example, Amazon Web Services, also applies for private infrastructure clouds.
However, load balancing or first fit schedulers are the norm within these frameworks,
but they were not developed with energy efficiency in mind.

%% Research from other seemingly related areas of scheduling,
%% such as scheduling operating system threads,
%% I/O scheduling, or job scheduling,
%% is not directly applicable to scheduling of virtual machines within IaaS clouds.
%% The optimization targets and constraints indeed very different.
%% For example, scheduling operating system threads assumes that tasks can be interrupted and resumed at will at the time scale of nanoseconds,
%% whereas migrating a virtual machine between two servers can take \emph{minutes} and involve gigabytes of state transfer.
%% Any service interruption of this magnitude must be clearly avoided.

%% Why is it hard? (E.g., why do naive approaches fail?)  naive
With the existing interface,
the infrastructure provider has no knowledge of the intended virtual machine run time.
Without this information,
there is little the provider can do to optimize the assignment with respect to energy efficiency.
%% Simple scheduling strategies like first fit will lead to situations where long running VMs are scattered over several physical machines.
Popular scheduling strategies, like first fit, will scatter VMs with similar ending times among physical machines.
It is, however, sensible to aggregate those VMs on a single PM.
For example, if each physical machine hosts one long running virtual machine,
it is sensible to ``dedicate'' a single server to host long running VMs.
As a result, the servers only running short-lived VMs can be powered off as soon as those VMs terminate.

\begin{marginfigure}[-1cm]
  \includegraphics{figs/lease/scheduler-example-round-robin}
\caption[Round robin scheduling example]{A round robin scheduler puts the five VMs onto servers A, B, C,
  A, and B, irrespective of the VM's reservation times.
Each VM's reservation time is represented by the circle's filled area.}
\end{marginfigure}

%% Why hasn't it been solved before? (Or, what's wrong with previous
%% proposed solutions? How does mine differ?)
Though giving information about the intended lease time to the provider makes intuitive sense,
it puts an additional burden on the customer.
However, customers may be easily motivated, if their extra effort leads to a price reduction:
VM instances with known run times are cheaper than regular instances with unknown run times.
We also argue that for many use cases lease time information is already available and requires little extra work from the customer.
For example, if the customer needs additional capacity to handle daily load increases for his web site between 10am and 2pm,
the lease time is four hours.
Recurring, periodic tasks (e.g., calculating a day's worth of sales figures) over a quasi-constant data set (sales volume will be constant for most days),
have quasi-fixed run times.
Lease times are thus available using historic data from previous runs.

\begin{marginfigure}[-2.7cm]
  \includegraphics{figs/lease/scheduler-example-first-fit}
\caption[First fit scheduling example]{A first fit scheduler puts VMs 1, 2, and 3 on server A.
VMs 4 and 4 on server B.
Due to the order in which VMs arrived, they are also roughly grouped by their remaining runtime.
However, this is not the case in general, but should actually be enforced by an energy efficient scheduler.}
\end{marginfigure}

%% What are the key components of my approach and results? Also
%% include any specific limitations.
To explore and quantify the potential benefits of \emph{timed instances},
we use simulation.
Because there exists no publicly available data on virtual machine run times and request distributions,
we evaluated a range of possible parameters.
For all simulated scenarios,
the extra run time information decreases the number of physical machines.
Compared against a round robin scheduler, the reduction in cumulative machine uptime varies between 28.4\% and 51.5\% percent.
Using a more competitive first fit scheduler as the baseline,
we still reduce the CMU between 3.3\% and 16.7\% percent.

\subsection{Summary of Contributions}
In this study, we make the following contributions:

\begin{itemize}
\item We propose a new virtual machine type called \emph{timed instance}.
  The user specifies the intended usage time along with all other parameters describing the VM instance (Section~\ref{sec:lease:problem}).
\item We describe our workload generator to compensate the lack of publicly available data sets (Section~\ref{sec:lease:model}).
\item We introduce scheduling algorithms using the virtual machine lease time to increase physical resource utilization (Section~\ref{sec:lease:schedulers})
\item Using simulation we quantify the possible savings for various settings (Section~\ref{sec:lease:eval}).
%% \item Based on the simulation results, we discuss implications for and possible changes to existing IaaS offerings (Section~\ref{sec:fwork})
\end{itemize}
