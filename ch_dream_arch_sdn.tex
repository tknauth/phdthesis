\subsection{A Brief Introduction to Software Defined Networking}
\label{sec:dream:arch:sdn}

Software Defined Networking aims to ease the management of networking equipment through separation of concerns and standardized interfaces.
Conceptually, basic network devices such as switches, are divided into a \emph{control plane} and a \emph{data plane}.
By interacting with the control plane, the behavior of the data plane can be altered.
The data plane, based on how it has been set up through the control plane,
does the actual work.
For each incoming packet it decides if to forward and on which port to output it.

Without SDN, each networking device has its own integrated control plane and each device acts independently of other devices.
The goal of Software Defined Networking is to create a single logical control plane for all networking devices.
By centralizing the previously distributed decision making,
some companies have reported tremendous improvements in overall network utilization~\cite{jain2013b4}.

The \emph{OpenFlow} protocol is sometimes used synonymously with SDN.
However, OpenFlow only standardizes how the control and data plane communicate with each other.
OpenFlow provides a rich set of high-level interfaces and APIs which are supposedly easier to use and are identical between OpenFlow-compliant components.
A standardized interface removes vendor lock-in because migration between different OpenFlow-compliant brands and models becomes easier.

\subsection{Detecting Idleness with SDN}

For our purpose of detecting idleness at the network layer,
we only use a small subset of features offered by SDN.
The {\sn} architecture for our SDN-based prototype, Figure~\ref{fig:sloth:arch},
is similar to the architecture using a reverse proxy.
The components include an OpenFlow switch, an OpenFlow controller,
and a set of servers functioning as virtual machine hosts.
The goal is to \emph{suspend} VMs deemed idle and to \emph{resume} them on incoming network traffic.
Because we target virtualized resources in a data center,
using network activity to determine (in)activity of the VM is reasonable.
Previous work, for example \citeauthor{bila2012jettison}~\cite{bila2012jettison},
used user input, or the absence thereof, to determine idleness.
However, tracking keystrokes or mouse movements is not applicable to virtual machines hosted in a data center.
Any such activity, for example when interacting with a remote desktop,
invariably translates into network traffic.

The network switches, as intermediate elements between the clients and servers,
forward traffic based on installed \emph{flow entries}.
A flow entry is a rule which tells the switch's data plane how to forward traffic.
With OpenFlow, each flow entry has two timeouts associated with it:
an \emph{idle} and a \emph{hard} timeout.
The switch removes a flow entry automatically whenever one of the two timers expires.
As the name suggests,
the idle timeout triggers a flow removal after $n$ seconds of inactivity, i.e.,
the switch has not forwarded any traffic related to this entry.
The hard timeout, on the other hand,
lets a flow expire $n$ seconds after its installation,
irrespective of activity.
The timeouts can be seen as a form of garbage collection.
The switch can only store a limited number of flow entries at a time and this memory is precious.
Upgrading a switch's memory is either very expensive or even impossible.

\begin{figure}[t]
  \centering
  \includegraphics{figs/sloth/arch-single-column}
  \caption[{\sn} architecture based on SDN]{The {\sn} architecture.
Client network traffic arrives at the router (1),
which forwards the traffic (2) to the final destination if it is up and running (5).
Otherwise, the switches contact the controller (3) which resumes the destination VM (4).
Once the VM is up again, the traffic is finally forwarded as usual (5).
}
  \label{fig:sloth:arch}
\end{figure}

\subsection{Suspending VMs}

For our purposes of suspending idle instances we disable the hard timeout by setting it to 0.
We may reconsider this in light of different constraints~\cite{zarek2012timeouts} or future uses cases,
but the hard timeout does not serve any purpose for us.
The idle timeout, on the other hand, is how we detect idleness.
Whenever a flow entry expires, either due to the hard or idle timeout,
the switch sends a message to the controller informing it about the flow removal.
Our SDN controller tracks the active flow entries by monitoring the flow removal messages to determine unused VMs.

\begin{marginfigure}[2cm]
  \includegraphics{figs/dream/suspend-vm-illustration}
\caption[Suspend VM illustration]{The OpenFlow controller resumes VMs on incomming network traffic.
On inbound traffic the switch sends a message to the controller asking how to handle the incomming packet.
The controller recognizes that the packet is for a sleeping VM and resumes the VM before forwarding the packet.}
\end{marginfigure}

 Because a VM can have multiple flows associated with it,
the controller only suspends the VM when the last flow expired.
Most of the time, each instance has at least two flows associated with it:
one for incoming and a second flow for outgoing traffic.
The controller tracks the flow entry count on a per VM basis,
based on the flow removal and missing flow entry messages it receives.
An alternative to reference counting at the controller is to query the switches' flow tables in fixed intervals.
By parsing the switches' flow table statistics,
we can also determine which VMs have not received any traffic for some time.
However, this creates additional and in the end unnecessary network traffic.
Because the information maintained by reference counting at the controller is also available by querying the switches,
this adds some degree of fault tolerance.
If the controller should loose the reference counting information, for example,
due to a server crash,
the information can be reconstructed based on the statistics maintained by the switches.

\pagebreak

\newthought{The idle timeout} directly influences the frequency and delay with which VMs are suspended.
Setting a low idle timeout, e.g., 60 seconds,
translates into an aggressive suspension policy.
If there was no traffic for 60 seconds,
the flow's idle timeout fires.
This allows to exploit even short idle intervals but may increase the frequency of suspend/resume cycles.
A more conservative idle timeout of, say, 5 minutes,
reduces the number of suspend/resume cycles,
but also delays the suspension of idle instances.

For busy switches, idle timeouts measured in minutes are infeasible.
The timeout is chosen to balance table occupancy and performance, i.e.,
a switch can only store a limited number of flow entries but each miss slows down the forwarding of packets.
In this case, a two-stage expiration process is possible.
The flow entries are installed with a short lived idle timeout of a few seconds.
The controller continues to receive messages for each expired flow entry.
After all flow entries, incoming and outgoing, belonging to a VM have expired,
the controller itself starts a timeout.
When the controller's timeout fires, the VM is finally suspended.
Alternatively, the controller's timeout is canceled if a new flow entry for the VM is installed.

Because each flow entry can have a different timeout,
it is possible to assign timeouts on a per-VM basis.
If domain-specific knowledge or historic information for certain VMs exists,
it can be used to adapt the idle timeout to the specific circumstances.
In this way, an administrator can fine tune the number of suspend/resume cycles based on the workload.

\subsection{Resuming VMs}

Analogous to suspending VMs based on flow removal messages,
the controller resumes VMs on \emph{packet-in} messages.
When the switch receives a packet for which no flow entry exists,
the switch sends a \emph{packet-in} message to the OpenFlow controller.
A missing flow entry, in our use case, is synonymous with the corresponding destination VM sleeping.
That is, before pushing a flow entry to the switch and forwarding the packet,
the controller must wake the VM up.
In fact, the controller could not install a flow at this point,
because the virtual network port on the virtual switch belonging to the VM does not even exists at this point.
Instead of pushing a flow entry,
the controller puts the packet into a FIFO queue and sends a command to the VM host to resume the VM.

\begin{marginfigure}
  \includegraphics{figs/dream/resume-vm-illustration}
\caption[Resume VM illustration]{The OpenFlow controller resumes VMs on incomming network traffic.
On inbound traffic the switch sends a message to the controller asking how to handle the incomming packet.
The controller recognizes that the packet is for a sleeping VM and resumes the VM before forwarding the packet.}
\end{marginfigure}

 Bringing the VM up takes anywhere from less than a second,
in the best case, to a few seconds.
Packets, arriving while the VM is resuming, are also queued.
Eventually, the controller realizes that the VM is up again by listening for gratuitous ARP messages originating from the VM.
Gratuitous ARP messages are a common feature of virtual machine monitors to help with re-establishing network connectivity after a VM migration.
Migrating a VM between different physical hosts,
likely also changes the physical switch ports the VM is reachable on.
Gratuitous ARP messages proactively inform the network infrastructure about the topology change.

The gratuitous ARP broadcast is visible to the OpenFlow controller as a \emph{packet-in} message.
Upon receiving the gratuitous ARP message,
the controller installs the appropriate flow entries to allow traffic from and to the VM.
As a last step, the controller forwards all queued packets destined for the instance.

\subsection{Implementation Details}

We implemented our prototype using the well-known OpenFlow controller \emph{Floodlight}~\cite{floodlight}.
Floodlight is written in Java and follows an event-driven and staged execution pattern.
The core functionality can be extended by writing and registering custom \emph{modules}.
A basic module consists of \emph{handlers} for predefined events.
Because, at any point in time, multiple handlers/modules may be listening for the same event,
the handlers are executed sequentially according to a configurable order.
Because our modifications sometimes prevent packets from being forwarded,
our module must run, for example, before the \emph{Forwarding} module.
We achieve this by specifying the execution order as part of the controller's configuration.

%% effect on higher layer protocols

%% higher layer timeouts 

% \subsection{Timeouts}
\subsection{Stale ARP Entries}

During the prototype's development we noticed that the controller would only ever see and queue ARP requests prior to waking up the VM.
This seemed odd, since we were trying to establish a TCP connection and expected TCP SYN packets.
The intention behind queuing packets, instead of simply dropping them,
was to disturb higher level protocols as little as possible.
After all, dropping and queuing are both legitimate options.
If we dropped the packet, a caring sender would retransmit it.
This is, for example, implemented in the ubiquitous Transmission Control Protocol to provide reliable message delivery over unreliable channels.
On the other hand, if the sender used a communication protocol lacking delivery guarantees,
it must expect and cope with lost messages.
For the sake of minimizing interference,
we decided to queue packets instead of dropping them.

The explanation for why the queues only ever contained ARP requests is this:
the VM is suspended because it has received no traffic for some time.
The lack of network traffic let the ARP entry for the VM's IP/MAC pair on the router become \emph{stale}.
To send any packet to the VM, the router must re-validate the IP/MAC mapping by issuing an ARP request~\cite{brown2002guide}.
Until the router receives a reply to the broadcast ARP request,
it cannot send any higher layer packets to the VM.
The ARP requests sent by the router, are the packets being queued at the controller.

The only time we might see packets other than ARP requests queued at the controller is when the ARP entry at the router is still valid while the VM is already suspended.
At least with the default ARP timeout settings,
this scenario seems unlikely.
%% [base_reachable_time_ms/2,  3*base_reachable_time_ms/2] => [30000/2, 3*30000/2] = [15000, 45000]
On Linux, an ARP cache entry is valid for a random time of 15-45 seconds by default.
Because we intend to suspend VMs after \emph{minutes} of inactivity,
the router's ARP entry will always be expired when a packet for a suspended VM arrives.
Conversely, we would only ever see non-ARP packets arriving at the controller for a suspended VM,
if we increase the ARP cache validity timer to several minutes.
% This, on the other hand, may have undesirable consequences when VMs are resumed on a different machine then they were originally suspended on.

\begin{marginfigure}
  \begin{tabular}{ll}
    Operating & Resend\\
    System    & Delay [s]\\
    \hline
    Ubuntu 12.04 & 1, 1, 1, 1, 1, 1\\
    OSX 10.9     & 1, 1, 1, 1, 31, 32\\
  \end{tabular}
  \caption[ARP request resend intervals]{Intervals between resending ARP requests for different operating systems.
After a fixed number of retries the system gives up to resolve the MAC address.}
  \label{tab:dream:arp-resend-interval}
\end{marginfigure}

\subsection{ARP Replies}

A problem related to the timed out ARP cache entries is the strategy to re-validate them.
By default, Linux sends out exactly 3 ARP requests, spaced one second apart,
before declaring the address unreachable.
That is, unless we manage to resurrect the VM within 3 seconds,
we risk breaking the transparency of {\sn}, i.e.,
clients would experience temporarily unreachable services.
As a reference,
we list the resend intervals for Ubuntu 12.04 and OSX 10.9 in Table~\ref{tab:dream:arp-resend-interval}.

While restoring a VM in less than 3 seconds is possible~\cite{knauth2014dreamserver},
instances with a lot of state may take longer than this.
Two solutions come to mind:
first, increase the number of retries before the operating system gives up to resolve the MAC address.
On Linux, which our router runs, the corresponding parameter is configurable separately for each network device.
However, we would prefer a solution that does not rely on changing default system values.
Because ARP is a stateless protocol, i.e.,
each message can be handled without knowledge of previous messages,
it is possible for the controller to answer ARP requests in-lieu of the VM.
This way, we can ensure that ARP requests are answered with minimal delay.

Having the controller answer ARP requests also reduces the achievable lower bound resume latency.
If ARP requests are sent out in one-second intervals,
the achievable end-to-end latency will also only increase/decrease in one-second steps.
For example, before modifying the controller to respond to ARP requests,
the minimum achievable end-to-end latency was slightly above one second.
The router would send out an ARP request, which nobody replied to, because the VM was not running.
One second later, the router would send a second ARP request.
By that time, the VM was up, answered the request, and everything proceeded as normal.
The VM might have been, and was, up after only 500ms.
However, because the packet was only resent after 1 second,
this resend interval puts a lower bound on the achievable resume latency.

The controller can reply to ARP requests because the protocol itself is stateless.
For stateful protocols, e.g., TCP,
it is much more complicated to have the controller act as an end point for the connection.
At least for TCP it is unnecessary to have the controller reply to packets to avoid failed connection attempts.
Higher level protocols, e.g., TCP, are more forgiving when it comes to transmission delays.
For example, with the default settings on a recent Linux distribution,
the client fails to establish a TCP connection only after 63 seconds (cf. Table~\ref{tab:dream:tcp-resend-interval});
much more lenient than the 3 seconds to resolve the MAC address.
During this time the initial SYN packet is retransmitted 6 times with increasing intervals.
One minute should be sufficient to resume VMs even if they have a lot of state.

\begin{marginfigure}
  \begin{tabular}{ll}
    Operating & Resend\\
    System    & Delay [s]\\
    \hline
    Ubuntu 12.04 & 1, 2, 4, 8, 16, 32\\
    OSX 10.9     & 1, 1, 1, 1, 1, 2, 4,\\
                 & 8, 16, 32\\
  \end{tabular}
  \caption[TCP SYN packet resend intervals]{Intervals between resending the initial packet to establish a TCP connection.}
  \label{tab:dream:tcp-resend-interval}
\end{marginfigure}

By having the controller resolve MAC addresses of resuming VMs,
we gain more time to resume a VM,
without causing the client to experience a temporary, but externally visible, error condition.
After having the VM's MAC address resolved,
the router can send the higher level IP packets to their intended destination.
In case the VM is still not up yet, the packets are queued at the controller.

% \begin{table}
%   \begin{center}
%     \begin{tabular}{l||l|l}
%       Platform & Intervals [s] & Total Wait Time [s]\\
%       \hline
%       Ubuntu 12.04 & 1, 2, 4, 8, 16, 32 & 63\\
%       OSX 10.9 & 1,1,1,1 & 75\\
%       Windows 7 & 1,1,1,1 & xx\\
%     \end{tabular}
%   \end{center}
%   \caption{Intervals at which the initial TCP-SYN packet is resent.}
%   \label{tab:tcp-resend-behavior}
% \end{table}

\subsection{Message Flow}

\begin{figure}
\begin{center}
  \includegraphics{figs/sloth/msg-sequence}
  \caption[SDN message flow]{Logical message flow between the client, router, OpenFlow controller, VM host, and VM.
The figure is not drawn to scale, i.e., there is no implied delay only causal ordering.}
  \label{fig:sloth:msg-sequence}
\end{center}
\end{figure}

The logical flow of messages exchanged between the different components is pictured in Figure~\ref{fig:sloth:msg-sequence}.
We illustrate the message flow when a client initiates a new TCP connection with a suspended service.
The initial TCP SYN packet arrives at the router,
which translates the public IP into the private IP associated with the VM~\footnote{
Translating the public IP into a private IP is called \emph{destination network address translation}~(DNAT).
Cloud providers use this to associate public IP addresses with a dynamically changing pool of virtual machines.
VMs only have a single network interface with a private IP address.
Access from the outside world is realized by having the router forward all packets destined to the VM's public IP to the VM's private IP address.}.
Because it has been a while since the router last communicated with the VM,
the ARP entry, mapping the VM's IP to a MAC address, is stale and must be re-validated.
To this end, the router broadcasts an ARP request for the VM's IP.

The OpenFlow controller, recognizes the IP in the ARP request, as belonging to one of the VMs it controls.
As a result, the controller sends a ``wake up'' message to the VM host and replies to the ARP request.
Having the controller answer the ARP request is an optimization which reduces the overall wake up latency.
Once the router receives the ARP reply,
it sends the TCP SYN packet towards its intended destination.
However, the OpenFlow switch is missing the appropriate flow entries to forward the packet to the VM.
Because the switch does not know what to do with the TCP SYN packet,
it forwards it to the OpenFlow controller.
The controller recognizes the packet as being destined for a VM that is currently resuming.
Instead of forwarding, the controller enqueues the packet locally.

At some later point in time, the controller picks up gratuitous ARP replies sent by the resuming VM.
The gratuitous ARP packets signal the end of the resume process, i.e.,
the VM is now able to receive and handle network traffic.
Any previously queued packets, e.g., a TCP SYN packet,
are forwarded to the VM.
This is accomplished by \emph{injecting} the previously queue packet back into the controller.
Injecting a packet causes the controller to execute the logic which handles packets forwarded by the switch.
The difference now is, that the controller sees a packet destined for a \emph{running} VM.
In this case, the standard Floodlight Forwarding module pushes the appropriate flow entries to the switch.
Ultimately, the TCP SYN packet is sent to the VM,
after which the three-way handshake to establish the TCP connection continues as usual.
Future packets do not involve the OpenFlow controller anymore,
because the switch now has all the necessary flow entries.
Client packets are forwarded by the router and passed directly to the VM via the virtual switch.

\subsection{Software Defined Networking Summary}

This concludes the small detour into Software Defined Networking.
We described how {\sn} uses missing flow entries as triggers to resume VM instances.
Conversely, VM instances are suspended when flow entries are removed due to inactivity.
As SDN operates at the lower layers of the network stack, e.g., layer 2 and 3,
the ability to monitor activity at these layers makes {\sn} more widely applicable than the alternative reverse proxy solution.

%% Each handler must make sure that other handlers also interested in the event are called by returning an appropriate status code.
%% This architecture prevents us from simply blocking in the event handler until the VM is resumed.
%% Instead, we put the packet into a FIFO queue.
%% If this is the first packet for a certain flow,
%% we perform two more action before returning from the handler.
%% First, we spawn a new thread which periodically checks if the resume process is finished.
%% Second, we issue a wakup call to the virtual machine.

%% When resuming the VM, qemu announces the VM on the network by generating a gratuitous ARP reply.
%% The OpenFlow controller picks up the ARP packet and matches the source MAC address against a list of currently resuming VMs.
%% This mechanism works well with the event-driven design of Floodlight, i.e.,
%% the controller does not have to actively query the VM's state to determine if it is finished resuming.

%% do we actually have to queue messages? if the ARP goes unanswered,
%% higher level protocols will not be able to send anything anyway.
%% if the ARPs are not answered for too long, however, you might see
%% an error at the higher level though.  the same is true if we just
%% queue the ARPs though. so maybe the controller should answer the
%% ARP requests, such that higher level protocols can start sending
%% packets, while the VM is still waking up.

%% Once the VM is up and running again,
%% we replay the packets queued at the controller.
%% After all packets have been replayed,
%% we also install the appropriate flow entries in the switch.
%% Future packets destined for this VM are directly forwarded by the switch without the involvement of the controller.

%% \subsection{Suspending VMs}

%% We detect idle VMs by tracking network activity.
%% With OpenFlow, it is possible to get a notification from the switch whenever a flow is removed.
%% At the controller, we perform ``reference counting'' with respect to the in and out flows of a VM.
%% Once there are no more flows in either direction,
%% we start a timer.
%% The timer duration is configurable and signifies the aggressiveness with which we suspend idle VMs.
%% Being more aggressive possibly translates into more suspend/resume cycles,
%% as the VM may have to be woken up more frequently.
%% On the other hand, being more aggressive may also translate into higher savings, i.e.,
%% the time the VM is running will be minimized.
%% Less aggressive timeout will have higher costs by potentially leaving the VM running when it is idle,
%% but also reduce the number of suspend/resume cycles.


% \subsection{Extending sleep intervals}

% list techniques to extend the sleep period of VM, e.g.,
% by having the OF controller reply to basic network recovery protocols instead of waking up the VM.

% Some applications, such as ssh, have multiple mechanisms to test if a connection is still live.
% Either through an application layer message, which in the case of ssh is encrypted and, hence, not spoofable.

% TCP connections have a built-in keep-alive mechanism.
% On Linux, the default keep-alive interval is two hours.
% If no TCP packets was exchanged over the channel for longer than two hours,
% one end generates a keep-alive packet to probe the connections status.
% While a keep-alive interval of two hours does interrupt the sleep period,
% at least with the default two hour interval it only minimally decreases the effectiveness of suspending idle VMs.
% Version 1.1 of HTTP also specifies persistent connections by default.
% However, to decrease the server's resource usage,
% open connections are closed after as little as five seconds on some popular web servers~\footnote{\url{http://httpd.apache.org/docs/2.2/mod/core.html#keepalivetimeout}}.

% For any connection-oriented protocol the controller cannot reply in-lieu of the original VM,
% either because the messages are cryptographically secured,
% as in the case of ssh,
% or because the conroller has insufficient knowledge about the connection state, e.g.,
% sequence and acknowledgement numbers in the case of TCP.
% Similar techniques have been proposed to extend the sleep period of idle desktop machines,
% but require application-specific stubs to run on the consolidation host~\cite{agarwal2009somniloquy}.

%% \subsection{Security}

%% Fraudulent resource usage in the cloud: http://dl.acm.org/citation.cfm?id=2046676

%% Can support wake on lan for VMs, although only in same subnet.
