\section{Related Work}
\label{sec:relwork}

%% Live migration over wide area network \cite{bradford2007live}

%% Active and passive replication;
%% examples of active replication schemes;
%% examples of passive replication schemes;
%% rdb;
%% live migration of virtual machines/storage
{\tt lvmsync}~\cite{lvmsync} is a tool with identical intentions as {\dsync},
but less generic than {\dsync}.
While {\dsync} operates on arbitrary block devices,
{\tt lvmsync} only works for partitions managed by the logical volume manager~(LVM).
{\tt lvmsync} extracts the modifications that happened to an LVM partition since the last snapshot.
To provide snapshots, LVM has to keep track of the modified blocks,
which are stored as meta-data.
{\tt lvmsync} uses this meta-data to identify and extract the changed blocks.

File systems, such as ZFS, and only recently btrfs,
also support snapshots and differential backups.
In ZFS, differential backups are performed using 
the ZFS \emph{send} and \emph{receive} operations.
The delta between two snapshots can be extracted and merged again with another snapshot copy,
e.g., at a remote backup machine.
Only users of ZFS, however, can enjoy those features.
For btrfs, there exists a patch to extract differences between two snapshot states~\cite{btrfs-send-receive}.
This feature is, however, still considered experimental.
Besides the file system, support for block tracking can be implemented higher up still in the software stack.
VMware ESX Server, since version 4, implements a feature called \emph{changed block tracking} which allows to track changes to virtual machine disks~\cite{vmware-vsphere-data-protection}.
The purpose is the same as with {\dsync}: knowledge of the changes improves the performance when backing up virtual machine disks.
Implementing support for efficient, differential backups at the block device level, like {\dsync} does,
is more general, because it works regardless of the file system and application running on top.

If updates must be replicated more timely to reduce the inconsistency window,
the distributed replicated block device~(DRBD) synchronously replicates data at the block level~\cite{ellenberg2009drbd}.
All writes to the primary block device are mirrored to a second, standby copy.
If the primary block device becomes unavailable,
the standby copy takes over.
In single primary mode, only the primary receives updates which is required by file systems lacking concurrent access semantics.
Non-concurrent file systems assume exclusive ownership of the underlying device and single primary mode is the only viable DRBD configuration in this case.
However, DRBD also supports dual-primary configurations, where both copies receive updates.
A dual-primary setup requires a concurrency-aware file system, such as GFS~\cite{preslan1997global} or OCFS~\cite{fasheh2006ocfs2}, to maintain consistency.
DRBD has been part of the Linux kernel since version 2.6.33.

There also exists work to improve the efficiency of established synchronization tools.
For example, \citeauthor{rasch2003place}~\cite{rasch2003place} proposed for \texttt{rsync} to perform in-place updates.
While their intention was to improve \texttt{rsync}'s performance on resource-constrained mobile devices,
in-place updates also help with large data sets on regular hardware.
Instead of creating an out-of-place copy and atomically swapping this into place at the end of the transfer,
the patch changes \texttt{rsync}'s behavior to directly update the original file.
An out-of-place copy reduces the transferred data volume if parts of the file's content have shifted from the beginning to the end.
With an in-place update, the content at the start of the file may already have been overwritten once the \texttt{rsync} algorithm reaches the latter parts of the file.
On the other hand, an out-of-place copy temporarily doubles the required disk storage space for the duration of the synchronization.
For gigabyte-sized files, this overhead may be unacceptable.
Since their original patch, in-place updates have been integrated into the regular \texttt{rsync} program and can be activated using the \verb+--inplace+ command line switch.

A more recent proposal tackles the problem of \emph{page cache pollution}~\cite{rsync-fadvise-patch}.
During the backup process many files and related meta-data are read.
To improve system performance, Linux uses a page cache, which keeps recently accessed files in main memory.
By reading large amounts of data,
which will likely \emph{not} be accessed again in the near future,
the pages cached on behalf of other processes are evicted from the cache.
The above mentioned patch reduces cache pollution to some extent.
The operating system is advised, via the \verb+fadvise+ system call,
that pages, accessed as part of the \texttt{rsync} invocation,
can be evicted immediately from the cache.
Flagging pages explicitly for eviction,
helps to keep the working sets of other processes in memory.
%%% Is it worth making note of uncacheable memory as an alternate/supporting approach?

Effective buffer cache management was previously discussed, for example,
by \citeauthor{burnett2002exploiting}~\cite{burnett2002exploiting} and \citeauthor{plonka2007application}~\cite{plonka2007application}.
\citeauthor{burnett2002exploiting} reverse engineered the cache replacement algorithm used in the operating system.
They used knowledge of the replacement algorithm at the application level,
here a web server,
to change the order in which concurrent requests are processed.
As a result, the average response time decreases and throughput increases.
\citeauthor{plonka2007application} adapted their network monitoring application to give the operating system hints about which blocks can be evicted from the buffer cache.
Because the application ultimately knows best about data access and usage patterns,
the performance with application-level hints is superior to the operating system's generic buffer management.
Both works agree with us on the sometimes adverse effects of the default caching strategy.
Though the default strategy certainly improves performance in the average case,
there will always be cases where a custom strategy outperforms the default.


%% unison: another tool for mirroring directory trees
%% http://www.google.de/patents?hl=en&lr=&vid=USPAT6985915&id=N2l4AAAAEBAJ&oi=fnd&dq=synchronization+block+level+backup&printsec=abstract#v=onepage&q=synchronization%20block%20level%20backup&f=false related US patent
%% 
