%% In the previous chapter we presented a solution for synchronizing the state of two block devices efficiently.
%% This enables us, for example, to synchronize copies of virtual machine disks with low overhead among a set of physical servers.
%% We can then switch off select physical servers,
%% knowing that the server's on-disk state, e.g., VM disks, is still accessible at other, live servers.
%% When a particular virtual machine is needed again,
%% a copy of the VM's state on a live server is located,
%% and the VM reinstantiated on this server.

While Chapter~\ref{chapter:lease-times} optimized the assignment of virtual to physical machines,
this chapter looks at the possibility to pause idle VMs with minimal impact on their normal operation.
By explicitly pausing idle VMs, the infrastructure providers needs fewer physical servers to host the same number of active virtual machines. 
Fewer physical servers reduce the provider's hardware as well as energy costs.

One challenge in this context is how to re-activate a paused virtual machine swiftly,
such that the user's experience is only impacted minimally.
To know when to resume the VMs again,
their activity must be monitored.
A novel SDN-based network-activity detector is presented as a more versatile alternative to a proxy-based activity monitor.

\noindent
The material in this chapter originally appeared at CGC'13~\cite[-4.2cm]{knauth2013fast-resume}, SYSTOR'14~\cite[-2.5cm]{knauth2014dreamserver}, and HotSDN'14~\cite[-0.6cm]{knauth2014sloth}.

\section{Introduction}

\begin{marginfigure}
  \includegraphics{figs/dream/vm-startup-latency}
  \caption[VM startup latency]{Comparison of startup latency for two popular cloud/infrastructure providers.
    The delay between requesting a virtual machine instance and being able to log in to that instance is tens of seconds up to minutes.}
  \label{fig:dream:vm-startup-latency}
\end{marginfigure}

%% What is the problem?
Cloud computing, i.e., renting IT resources for indefinite time spans, offers many benefits,
the most touted of which is probably the flexibility to add and release resources based on ones computing needs.
However, we posit that current cloud provider offerings are ill-suited for lower-end customers,
especially considering user-facing, interactive services with frequent idle periods.
Cloud providers should allow resources to be acquired and released even for time frames of a couple of minutes.
While some cloud providers, at the time of writing,
still charge users by the hour, for example, Amazon,
there is no fundamental technical reason preventing the adoption of more fine grained billing cycles.
For example, Google Compute Engine and Microsoft Azure, recently started to charge by the minute.
Even shorter billing intervals are only useful if the startup latency, i.e.,
the time between submitting a resource request and gaining access to the resource,
is reduced at the same time.
Currently, the fastest startup times are still on the order of tens of seconds~\footnote[][-0.8cm]{http://gigaom.com/2013/03/15/by-the-numbers-how-google-compute-engine-stacks-up-to-amazon-ec2/}.

%% \cite{das2010litegreen}

%% Why is it interesting and important?
While there are many factors influencing the total startup time of a ``cloud VM'', for example,
the time to find a suitable host within a cluster of computers~\cite{schwarzkopf2013omega},
we demonstrate that bringing up a stateful virtual machine takes less than one second in the best case.
This allows low-end customers to deploy their services ``in the cloud'' without requiring the services to run continuously.
Temporarily switching off idle services has benefits for the cloud provider and customer:
the cloud provider saves resources while the service is off,
requiring fewer physical machines to host services.
The unused servers can be powered off,
saving energy and thus reducing the provider's operational costs.
On the other side, the customer saves money because he is not billed for periods when his service is suspended.
The customer essentially trades quality of service for reduced cost:
by accepting a small latency increase for some requests,
he saves money compared to a continuously running service.

%% The request distribution of our target services have frequent inter-arrival times of several minutes.
%% Disabling the services during those idle periods would allow the customer to save money by cutting costs.
%% On the provider side, it may enable more flexible scheduling as resources are explicitly transitioned between on and off states.

%% Why hasn't it been solved before? (Or, what's wrong with previous
%% proposed solutions? How does mine differ?)
The key challenge is to reactivate services quickly in response to incoming requests.
As we expect humans to interact with these services,
fast response times are essential.
As already mentioned earlier,
it takes up to several minutes to start a new virtual machine instance with some cloud providers.
It is possible to shorten the startup delay by resuming from a checkpoint instead of booting a new instance.
However, we are unaware of any major cloud provider exposing this functionality via their API.
%% Google does not support it (yet): http://stackoverflow.com/questions/19759991/how-can-i-hibernate-a-google-compute-engine-server
%% Amazon also does not support it: private e-mail conversation.
But, even though resuming from a checkpoint may be faster than booting a fresh instance,
existing resume implementations still leave headroom for further optimization.
Typically, the delay to resume from a checkpoint grows linearly with the checkpoint size,
reaching into tens of seconds for VMs with multiple gigabytes of memory.
%% We build on previous work~\cite{zhang2013esx, zhang2011workingset} to cut resume times to a minimum.
With {\sn} we apply results from previous work~\cite{zhang2013esx, zhang2011workingset, knauth2013fast-resume, zhu2011twinkle} to resurrect VMs with minimal delay.

%% What are the key components of my approach and results? Also
%% include any specific limitations.
With {\sn}, we propose a system architecture that supports the on-demand deployment of virtualized services.
{\sn} suspends idle virtual machines and only resurrects them on the next incoming request.
This is completely transparent to the request issuer,
except for a slight increase in response time.
Applying existing techniques and adapting them to the open-source virtual machine emulator qemu/kvm,
{\sn} resumes stateful VMs in less than one second from local storage,
and in slightly more than one second from remote storage.

We also present two alternative solutions to monitor the service's activity.
To resume VMs on incoming requests, {\sn} must be somehow interposed between the client and the service,
such that it sees the flow of requests.
One solution is to proxy requests and adapt the proxy's behavior according to our needs.
This works well for HTTP-based services and covers many use cases as the hyper text transfer protocol is fundamental to the Internet.
There exists a large variety of proxy software and the overall integration of proxies into the Internet's architecture is established and mature.

\pagebreak
However, not all services run over HTTP and a more generic approach working for \emph{any} application layer protocol would clearly be preferable.
This is why we developed an alternative to the proxy-based solution:
with the recent rise of Software Defined Networking~(SDN) there now exists a mechanism to effortlessly expose and react to network-level events.
We use the event notification framework provided by SDN to monitor the in- and outgoing network traffic of each service.
Based on the perceived network (in)activity,
we determine when to switch a service on and off.
Observing activity at a lower network layer,
the data link layer as opposed to the application layer,
makes the SDN approach more generic by supporting arbitrary application layer protocols.

%% To this end, we modified existing open-source components, Apache and qemu/kvm,
%% to support the on-demand provisioning of virtual machines.
%% Our Apache proxy is aware of the backend server's on/off state.
%% The proxy enables the backend if a request for a suspended backend server arrives.
%% Resuming an entire operating system virtual machine within a few seconds is possible by adapting qemu's default resume implementation.
%% We implemented a \emph{lazy resume} mechanism that drastically cuts the time to reactivate a suspended virtual machine;
%% sometimes as low as 1.1 seconds.

%% \begin{enumerate}
%% \item When to transition service into idle state?
%% Deterministic timeout or prediction based?
%% \item Architecture: load balancer keeps state of which instance is up or down.
%% \item How fast can we resume service from idle state?
%% \item How much money/energy can we save?
%% \end{enumerate}

%% In summary, our contributions are as follows:
%% \begin{itemize}
%% \item We motivate the need and potential of truly on-demand cloud services where services are suspended even for minute-long idle intervals (Section~\ref{sec:problem}).
%% \item We describe our lazy virtual machine resume implementation, VM resume set estimator, and suspend-aware HTTP proxy (Section~\ref{sec:architecture}).
%% \item We evaluate the speed with which we can resurrect virtual machines in a variety of settings,
%% including seven different applications, storage technologies and locations, as well as different VM resume techniques (Section~\ref{sec:eval}).
%% \item We share all code and data with the scientific community at \url{http://bitbucket.org/tknauth/dreamserver}.
%% \end{itemize}
