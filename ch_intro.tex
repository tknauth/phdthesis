\chapter{Introduction}

\section{Motivation}

This thesis presents work done at the confluence of two established topics in the area of systems engineering.
\emph{Hardware virtualization}, the first of the two topics,
became mainstream in the early 2000s when solutions to virtualize the ubiquitous x86 instruction set started to be widely available.
It has since moved on to become an established component of modern day computing.
With a wide array of virtualization products available from different vendors,
virtualization is routinely used by small businesses and warehouse-sized computing installations alike.
In particular cloud computing,
as a new model to deliver IT resources,
relies heavily on fast and efficient virtualization.

The second topic, \emph{energy efficiency},
started to receive attention at the start of the millennium,
but became a really hot topic in the latter part of the first decade.
As the demand for compute and storage capacity continues to grow,
so does the size and density of data centers.
Modern compute facilities routinely consume power on the order of multiple megawatts,
requiring careful planning and management of their energy supply.
Spurred by the general debate on climate change,
people suddenly started to worry about all the megawatts consumed by these outsized IT factories.

\newthought{The research question} overarching all the work presented here is \emph{if and how hardware virtualization and related technologies can be used to improve the energy efficiency within today's IT landscape}.
Hardware virtualization allows to create multiple virtual machines,
all ``emulated'' in software, on a single physical servers.
Each virtual machine provides the illusion of a dedicated physical server.
The ease and low cost of creating new machines has changed the way in which IT administrators manage their infrastructure.
Virtualization allows to co-locate multiple applications on the same hardware while ensuring strong isolation between them.
In particular the cloud computing industry, where IT resources are rented instead of owned,
uses virtualization ubiquitously.

By focusing on energy efficiency aspects,
we will present challenges and propose solutions in the areas of (i)~virtual machine scheduling within cloud data centers,
(ii)~on-demand resource provisioning within cloud data centers, and
(iii)~efficient state synchronization as a solution to the ``local state'' problem~\footnote{When powering off a physical server,
the local state becomes inaccessible. Hence, before powering down the server, the state must be replicated to another location to remain available.}.
The main goal with any of the techniques and tools presented here is to save energy by turning off unused servers.

%% \begin{enumerate}
%% \item Optimize the scheduling of virtual machines within cloud data centers such that physical servers can stay powered off for longer.
%% \item Deploy cloud resources on-demand and only when they are needed, such that physical servers can stay powered off for longer.
%% \item Enable the efficient state transfer between servers.
%% State synchronization is necessary before a server is powered off,
%% because its local state becomes inaccessible.
%% \end{enumerate}

\section{Thesis Outline}

The thesis is divided into 7 chapters, including this introduction.
The following paragraphs give a brief overview of each chapter's content.

\newthought{Chapter~\ref{chapter:background}} presents a brief history of energy efficiency and virtualization within the context of modern day computing.
Energy efficiency has received a lot of interest in the research community and the wider public in the last couple of years.
While energy efficiency has been a concern for embedded and portable computing devices for much longer,
the focus on energy efficiency of the IT industry as a whole is more recent.
With more and more facets of our daily lives depending on and utilizing interconnected devices,
warehouse-sized facilities are required to store and process all our digital assets.
Chapter~\ref{chapter:background} introduces the factors governing energy efficiency of modern day data centers,
as well as highlights the role that virtualization and cloud computing play in this.

\newthought{In Chapter~\ref{chapter:cluster-trace}} we analyze a 50 server cluster owned and operated by our research group.
Based on four and a half years of usage data,
we determine the cluster's overall usage and energy saving potential.
While the research cluster is different from a general purpose internet-scale data center in terms of usage patterns and workloads,
we find the same degree of overall low utilization as in other environments.
Our results regarding the cluster's energy use and overall utilization agree with previously published reports on the huge potential to better utilize the servers and save significant amounts of energy by powering off unused servers.

\newthought{In Chapter~\ref{chapter:lease-times}} we investigate the benefits of specifying the lease time upfront when requesting virtual machines in a cloud environment.
Typically, the cloud customer requests computing resources from the cloud provider in the form of readily-sized CPU, memory, and disk configurations.
What the cloud provider does not know, however,
is the time frame for which the customer intends to use the resources.
We determine if and how much specifying the lease time helps the cloud provider to optimize the mapping of virtual to physical resources.
We are particularly interested in the effect of how co-locating VMs with similar remaining lease times reduces the number of powered up physical machines.
The goal is to power off servers frequently and for as long as possible to save energy.

%% Among the resources rented out in cloud computing,
%% some are easier to share and multiplex between different parties than others.
%% For example, the CPU is very amenable to sharing and processor time-sharing has been developed early on in the computing history.
%% Other resources, such as main memory, are much harder to share.
%% While it is possible to switch the threads executing on a CPU millions of times per second,
%% the same is not true when it comes to memory.
%% Evacuating gigabtyes of memory by, for example, writing them to stable storage,
%% takes on the order of seconds;
%% ages in terms of unused CPU cycles.

\newthought{Chapter~\ref{chapter:dreamserver}} describes a system to automatically suspend and resume virtual machines based on their network activity.
Idle virtual machines, those not having received or sent any data over the network for some time,
are suspended to free resources for active VMs.
Suspending a VM frees up main memory which is often the first resource to become a bottleneck in cloud environments.
The automatic and explicit swapping out of VMs is combined with a fast restore mechanism:
when incoming network traffic is destined for a suspended VM,
the fast VM resume mechanism ensures a swift re-instantiation.
The important parts of the VM's state are read prior to restarting its execution,
while the less important bits are read lazily in the background later on.

In addition to the swift re-activation of VMs,
we also present a novel way to detect (in)activity at the network layer.
Our solution utilizes Software Defined Networking~(SDN) and in particular flow table entries to determine communication activity.
Chapter~\ref{chapter:dreamserver} gives a detailed description of the architecture, optimizations, and evaluation results.
Our conjecture is that explicitly suspending unused VM instances allows for better reuse of memory by multiplexing a larger number of VM instances on a given set of servers.
This, in turn, reduces the overall number of required servers,
reducing their energy consumption and related costs.

\newthought{Chapter~\ref{chapter:dsync}} presents a low-level mechanism to efficiently synchronize block-level data.
Although not directly related to energy efficiency,
it deals with a problem encountered when powering off physical servers:
turning off the server also disables access to all the data stored on locally attached disks.
Hence, before turning off the server,
local updates must be copied to another machine to be continuously accessible.
In a cloud computing context, this could mean moving virtual machine disks from a server scheduled for power down,
to another live server.

% The need to periodically synchronize large amounts of data at the block device level occurs, for example,
% when migrating virtual machines or backing up their on-disk state.
% Migrating a VM between servers must not only copy the VM's in-memory state,
% but also the persistently stored on-disk information.
% If an old version of the VM's disk exists at the migration destination,
% the data transfer volume can be significantly reduced by only transferring updates from the source to the destination.

Established schemes to identify updates within a file or block device incur a high overhead.
For example, to create and compare computationally expensive checksums,
the entire input must be read once.
An alternative is to actively track the changes and record which data blocks have been updated since the last synchronization.
We present an extension to the Linux kernel to do this tracking at the block device layer.
Chapter~\ref{chapter:dsync} presents the design of our kernel extension as well as an extensive evaluation comparing our solution against alternatives.

%% We see our \textit{dsync} tool as a low-level mechanism to efficiently synchronize information at the block device layer.
%% This becomes necessary, for example,
%% when local state must be moved off a server that is scheduled for power down.
%% \textit{dsync} requires no CPU-intensive checksum computations,
%% but still only transfers the updates.
%% Only transmitting the updates reduces the network traffic and overall synchronization time,
%% allowing the server to be powered off sooner,
%% thus saving even more energy.

\newthought{Chapter~\ref{chapter:conclusion}} summarizes the topics discussed in the previous chapters and gives a short outlook on future developments and possible extensions.

\subsection{Clarification of Contributions}

The remainder of the thesis often uses the plural personal pronoun ``we'' when describing certain actions and observations.
To avoid confusion about the exact nature of my contributions,
I will now briefly explain the role I played in crafting the 6 papers on which this thesis is based.
First, I am the lead author on all 6 papers.
In 5 out of 6 papers the only co-author is my supervising professor.
We discussed and shared ideas,
but the ultimate responsibility of designing, building, evaluating, and writing up the results was solely with me.

Only the paper titled ``Sloth: Using SDN for Traffic-driven Virtual Machine Activation''~\cite{knauth2014sloth} has co-authors other than my supervising professor.
Matti Hiltunen, the paper's third author,
sparked the initial idea and participated in discussions as the project evolved.
Pradeep Kiruvale, the paper's second author, contributed the implementation.
I was coordinating our efforts, provided the existing DreamServer infrastructure and implementation, helped with the evaluation, and wrote the paper.

\subsection{List of Relevant Publications}

The following is a list of my publications,
relevant to the contents of this thesis,
in the order in which they were published.

\begin{itemize}
\item \bibentry{knauth2012timed}
\item \bibentry{knauth2012scheduling}
\item \bibentry{knauth2013fast-resume}
\item \bibentry{knauth2013dsync}.
\item \bibentry{knauth2014dreamserver}
\item \bibentry{knauth2014sloth}
\end{itemize}
