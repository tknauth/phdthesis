\section{Related Work}
\label{sec:rwork}

Our work is closely related to \citeauthor{zhang2011workingset}~\cite{zhang2011workingset}.
They were the first to investigate techniques for speeding up the resume of suspended virtual machines.
Experimenting with different resume strategies,
\citeauthor{zhang2011workingset} found that an entirely lazy resume could decreases the VM's performance to the point where customers considered it unusable.
Working set prediction helped to resume the VM faster and with minimal performance impact on the guest's workload.
We applied their findings and presented an implementation for the open-source virtual machine emulator qemu.
The key difference of {\sn} to \citeauthor{zhang2011workingset}'s initial work is our use of a different predictor.
Tracing the execution past the point where the VM is suspended makes little sense in our case.
An idle VM, accessing a minimum of pages,
is a bad predictor for pages accessed upon the next incoming request.
Instead, we use the pages accessed during resume $N$ as the predicted pages for resume $N+1$.

Our modifications to qemu are exclusively in the userspace part,
which makes our system more akin to \citeauthor{zhang2011workingset}'s earlier work based on VMware's Workstation product.
In later work~\cite{zhang2013esx}, \citeauthor{zhang2013esx} required access to core parts of the hypervisor's memory management to implement their ideas,
which was only possible with VMware's ESXi virtualization software.
We feel that our current prototype works well enough without access to privileged kernel-level information.

We also set us apart from \citeauthor{zhang2011workingset} by using a different hard- and software setup.
In addition to spinning disks, we use SSDs,
to see how much a higher IOPS count helps lazy resume.
We also presented data for local and remote storage to further diversify the environment.
Our application mix is also different in that we focus on web services instead of CPU-intensive benchmarks.
We primarily care about a swift initial response,
instead of the resume/suspend cycle's impact on the turn-around time of a standardized benchmark.

In later work, \citet{zhang2013esx} moved away from predicting page accesses.
Instead, they proposed to track the causal relationship between accesses and use this as a heuristic for prefetching.
The implementation is more elaborate and requires modifications to the core parts of the hypervisor.
The evaluation section of their Halite system reports, among other things,
the resume time of a web server with 7.3 seconds.
This is faster then their baseline, but still slower than what we were able to achieve.
For example, our appliances routinely reply to the first request in around 2 seconds (cf. Figure~\ref{fig:dream:resumetime-bar-hdd-direct}) or even faster when stored on SSD.
Although it is difficult to compare the two resume times directly,
as the benchmark setups are different,
it is still illustrative to highlight the achievable resume times.

While virtual machines allow to run existing applications without modifications,
a tailored runtime environment may also reduce the initial startup overhead.
If we forgo the flexibility and deploy on a tailored platform,
impressive application start up times are achievable.
Recently, the Erlang on Xen project~\cite{erlangonxen} demonstrated startup latencies of less than 0.3 seconds by running the application directly on the hypervisor,
without a traditional operating system in between.
Techniques to reduce the start up and resume latency are relevant for {\sn},
because it ensures swift responses whenever there is an incoming request for a suspended service.
Fast resume times also allow to exploit progressively smaller intervals of idle time
as the resume penalty is getting smaller and smaller.
Another noteworthy approach in the same direction is ZeroVM~\cite{zerovm},
which also aims to remove the traditional virtualized operating system from the cloud computing environment.
By repurposing a sandboxed environment originally developed for Google's Chrome web browser,
the hypervisor becomes unnecessary to provide security and isolation between co-located applications.
Less clutter, in the form of duplicate functionality at different levels of the software stack,
is beneficial to speed up application deployment and launch.

%% Then there is a large body of work in the related field of virtual machine migration.
%% From virtualized compute environments~\cite{kozuch2002internet} that provide users an identical experience wherever they go,
%% over the instantaneous creation of hundreds of identical worker VMs~\cite{lagar2009snowflock} in cluster computing scenarios,
%% to consolidating virtualized desktops in order to save energy~\cite{das2010litegreen, bila2012jettison},
%% \high{
%% replication for fault tolerance~\cite{cully2008remus}, and
%% lightweight VM cloning for forensic analysis~\cite{vrable2005scalability}.
%% They all face the same basic challenges of deciding which parts of the virtual machine to migrate and when.
%% Each system has to trade off responsiveness, data volume, and migration overhead, among others.
%% Speeding up the resume process also aims to selectively read data from disk to reduce the overall data volume.
%% to efficiently clone VMs not applicable.
%% }

\citeauthor{zhu2011twinkle}~\cite{zhu2011twinkle} also employ techniques to reduce the time to spin up new virtual machines.
The details are, however, geared towards clusters of virtual machines running identical, stateless software components, e.g.,
the web servers of a three-tiered web service.
As we focus on small services from different customers that explicitly do not span multiple machines,
the techniques to quickly instantiate almost identical instances, have no direct application in our scenario.
Snowflock~\cite{lagar2009snowflock} and Potemkin~\cite{vrable2005scalability} are two additional systems with similar goals.

An interesting direction for future work would be to exploit commonality of instances to reduce the amount data to read from disk when resuming a guest.
Previous work, for example by \citeauthor{gupta2010difference}~\cite{gupta2010difference} on page sharing between VMs,
revealed that up to 50\% of memory can be shared between VMs.
This cannot only be exploited to save memory while the VMs are running,
but also when resuming instances from disk.

%%  used virtualized to provide the user with a consistent compute environment between physically distinct and distant workstations.
%% Advances in operating system virtualization technology made it possible abstract the physical hardware and move the resulting virtual machine between different servers.
%% The intended use case was for people to have a consistent work environment, e.g., file hierarchy and programs,
%% whether at home or in the office.
%% The virtual machine would seamlessly migrate between the different locations the user was working from.

%% Snowflock~\cite{lagar2009snowflock} tackles the problem of how to instantiate hundreds of identical worker VMs in a server cluster.
%% To get the virtual machine in a usable state as quickly as possible,
%% Snowflock transfers missing parts of the virtual machine across the network on demand.
%% Because the VMs are essentially identical,
%% there is significant overlap in the state required by each instance.
%% To reduce the amount of network traffic,
%% Snowflock uses multicast to distribute the worker state.
%% While we are also interested in kick-starting the VM's execution as quickly as possible,
%% we resume non-identical instances instead of starting hundreds of identical instances.

%% \citet{chen2008energy} examined energy efficiency in the context of connection intensive services.
%% Connection intensive services,
%% exemplified by Microsoft's Live Messenger,
%% have characteristics that defeat straightforward power management schemes.
%% Shutting down servers must be weighted against the user-perceived degradation in service quality.
%% Long lived sessions must be either migrated before a server is shut down or re-established after the fact.
%% {\sn} does not target applications with long lived sessions,
%% because it is difficult to establish when such an application is actually idle.
%% Instead, by focusing on transactional, request/response-style services any interval between two requests is idle.
%% By focusing on low volume services,
%% we also do not scale a single service across multiple servers,
%% but each server hosts a multiple low volume services.

%% Researchers have also been busy improving the resume performance of virtualized operating systems~\cite{zhang2013esx, zhang2011workingset, zhu2011twinkle, knauth2013fast-resume}.
%% Resume speed mainly depends on two factors:
%% (a)~the volume of data which must be read from disk before execution can start, and
%% (b)~the disk layout of the necessary data.
%% The data volume can be reduced by only reading in the virtual machine's resume set,
%% while the disk layout should ensure the physical proximity of the on-disk data.

All the work on improving the resume time of virtualized instances will contribute to reduce the notoriously long startup times of today's cloud providers.
A recent study~\cite{mao2012performance} found that it takes on average more than one minute until a newly provisioned instance is ready for service.
Waiting more than a minute for a reply to the initial request is intolerable, i.e.,
today's cloud providers do not yet support the operational model suggested by {\sn}.

\newthought{Another area of related work} is on energy efficient computing infrastructure.
For example, \citeauthor{krioukov2011napsac}~\cite{krioukov2011napsac} deal with the problem of power-proportionality in stateless web clusters,
while \citeauthor{chen2008energy}~\cite{chen2008energy} save energy in the context of stateful connection servers.
With our work we target a different environment in which multiple virtualized services share the same infrastructure.
To benefit from the service's idle times,
the virtualized containers must be quickly reactivated on incoming requests.
The two examples of previous work do not use virtualization and have a single service spanning multiple physical machines.

\subsection{Software Defined Networking}
\label{sec:dream:rwork:sdn}

The novelty with {\sn} is in the use of SDN technology instead of a special purpose application-level proxy.
With SDN we are able to determine (in)activity by monitoring network traffic at the transport level.
Previously, we relied on a customized HTTP proxy to forward requests and initiate the wake up of suspended VMs.
{\sn} is more generic, as it works for any application layer protocol,
but requires SDN-compliant switches, either in hard- or software.

While SDN has been used to implement various networking functions such as firewalls~\cite{wang2013firewall},
load balancers~\cite{wang2011openflow}, and network anomaly detectors~\cite{mehdi2011revisiting},
we are not aware of any prior work where SDN is used to control the life cycle of VMs.
