\section{Related work}
\label{sec:lease:relwork}

% \subsection{Energy-efficient computing}

% Research into energy efficient servers have focused on two aspects:
% low-power idle vs. low-power active states.
% Proponents of low-power idle states argue for rapidly transitioning between active and idle states in order to save energy~\cite{meisner2011power,gandhi2011case}.
% Idle states consume significantly less power than the active states.
% However, depending on the actual workload, idle times may be too few or short to be useful.
% In these cases, only low-power active states can increase the energy efficiency~\cite{meisner2011power}.

There exists a large body of work on the topic of virtual machine scheduling,
where reducing the energy consumption is only one of many possible optimization criteria.
For example, \citeauthor{beloglazov2013energy}'s PhD thesis provides a valuable overview on this topic~\cite{beloglazov2013energy}.
The defining difference is us forgoing live VM migration to dynamically adjust the assignment of VMs to servers.
While there definitely is merit to the ability to freely move VMs between physical machines,
it also comes at a cost:
service interruption, consumed network bandwidth, CPU cycles spent transmitting and receiving data on the network,
and added complexity in terms of algorithms to decide when and how to change the allocation.
While dynamic consolidation based on VM migration is an important selling point and feature in enterprise-grade virtualization solutions, e.g.,
VMware's Distributed Resource Scheduler~(DRS)~\cite[-0.3cm]{gulati2011cloud},
we know of at least one major infrastructure cloud provider that does not use VM migration at all.
Despite us explicitly not considering live migration,
our work actually complements existing work which uses live migration to improve,
for example, energy proportionality~\cite{tolia2008delivering}.
After all, improved initial placements only reduce the total number of migrations,
which is certainly desirable.

%% With live migration~\cite{clark2005livemigration} there exists a mechanism to move virtual machines between physical machines,
%% seemingly obviating improved virtual to physical machine assignments.
%% However, live migration has limitations,
%% and should not be seen as a replacement for improved up front scheduling.
%% Rather, our work complements existing work to use live migration to improve energy proportionality~\cite{tolia2008delivering}.
%% Improved initial placements will reduce the total number of migrations.
%% Despite the tempting simplification to view migrations as ``free'',
%% each migration consumes network bandwidth (state transfer), CPU cycles (TCP processing),
%% and halts the computation for up to 10s (or longer) depending on workload and VM configuration.

Our work is unique in that we are not aware of any public cloud computing provider that allows to specify the intended lease time when requesting a virtual machine.
The idea of specifying intended runtimes has been explored in other contexts though.
For example, the virtual machine resource manager Haizea allows to reserve resources for future use~\cite{sotomayor2008combining},
as do some batch scheduling systems like Condor~\footnote{\url{http://research.cs.wisc.edu/htcondor/}} and LSF~\footnote{\url{http://www.platform.com/Products/platform-lsf}}.
While user-provided runtime estimates are not perfect~\cite{lee2005user},
they at least theoretically give the scheduler more information to better plan the execution of jobs.

Even when job runtimes are taken into account,
the purpose of a traditional batch scheduling system is different from a virtual machine scheduler.
Batch scheduling systems typically maintain one or multiple job queues,
to temporarily hold jobs until they can be executed.
It is the scheduler's responsibility to balance wait times, fairness, and overall job turn-around times.
Often, the scheduler has to weight and prioritize the different objectives,
as they may conflict with each other.

The reason such job queues exist,
is because the total number of resources requested by all jobs, queued and running,
exceeds the physically available resources.
If sufficient resources for running all jobs simultaneously were available,
each job could run as soon as it is submitted.
Job runtime estimates influence the job execution order where the goal is to fill gaps with smaller jobs by executing them in between ``big'' jobs;
a technique also known as \emph{backfilling}~\cite{lawson2002multiple}.

Contrast this to the experience provided by cloud computing:
virtual machines are ready for service in a few minutes at most.
The ``cloud'' is essentially the complete opposite of a batch processing system as its available capacity is larger than the demand.
A cloud scheduler does not need to maintain job queues and create an elaborate schedule to efficiently execute as many jobs as possible on a constrained set of resources.
Instead, the question faced by our virtual machine scheduler is which VMs to co-locate in order to minimize the overall number of powered-on machines.

But even this seemingly simpler problem of co-locating VMs without over-committing major resources, such as CPU and memory,
can lead to contention on shared micro-architectural resources~\cite[-2cm]{ahn2012dynamic}.
Another reason to dynamically reallocate, without prior over-subscription,
is to co-locate heavily communicating VMs.
Network bandwidth, especially across rack-boundaries, is limited,
and optimizations to reduce traffic between racks always welcome~\cite[-1cm]{meng2010improving}.

In the less generic context of data processing frameworks,
researchers also identified the potential of co-locating VMs with similar run times~\cite[-0.3cm]{cardosa2011exploiting}.
While the intentions of their work are similar to ours, i.e.,
reduce the number of active physical servers,
they achieve this goal by aggregating MapReduce jobs with similar runtimes on the same machines.
Applying the same ideas at the virtual machine level is arguably more widely applicable.
Also, our problem definition is more general in that we assume time-varying demand,
whereas \citeauthor{cardosa2011exploiting}'s setup is static, i.e.,
all jobs are known before a placement is calculated.

% Open source infrastructure management frameworks, such as OpenStack~\footnote{\url{https://www.openstack.org/}},
% have spurred the interest of corporations and universities alike.
% Despite the lowered bar of deploying in-house private clouds,
% little work has been published describing modifications to the default round robin and first fit schedulers.

% Batch scheduling systems also commonly assume that jobs can be stopped, moved, and restarted at will.
% Infrastructure clouds do not have this freedom,
% because workloads consist of interactive services.
% Migrating virtual machine instances between physical servers may result in unacceptable service unavailability.
% Though explicitly allowing to preempt certain virtual machines for the sake of energy efficiency may be an option~\cite{salehi2012preemption}.

% \cite{agmon2003incentives}

% Even if we assume knowledge about a job's runtime,
% the optimization objectives are different.
% For example, in the context of semiconductor fabrication facilities the processing times of a certain product at a certain tool is known.
% The task is then to device an assignment of products to tools while respecting dependencies between processing steps.
% The metrics optmized for in this scenario are typically makespan, due dates, or tool utilization.

% Scheduling is an important aspect of many computing systems and at many abstraction levels.
% Operating systems schedule threads onto multi-core CPUs.
% One important underlying assumption in the real-time multi-core scheduling community concerns zero-cost migrations.
% Threads can be migrated between cores for free.
% Also, processes are assumed to be interruptible.
% Both assumptions do not apply to virtual machine scheduling.
% Though migrations are possible,
% they incur non-negligible bandwidth and processing costs, along with multi-second instance unavailability~\cite{hermenier2009entropy}.
% Further, virtual machines are interruptible (e.g., offline migrations use this feature),
% but the interruption is visible to the customer.

% Third, data processing frameworks, such as MapReduce, also have a scheduling component.
% The MapReduce scheduler assigns jobs to compute nodes in a fashion that optimizes for locality.
% Because cross-rack bandwidth is limited in a data center,
% the focus is on minimizing data transfer.
% In infrastructure clouds, data access patterns are unknown at the time of scheduling,
% precluding locality-aware placement.
% Depending on how stable storage is attached (e.g., directly vs. networked),
% optimizing for locality~\cite{park2012locality} may be superfluous.

% Haizea as a resource management and scheduling framework~\cite{sotomayor2008combining}.

%% \cite{beloglazov2013energy} dynamic virtual machine consolidation,
%% but without considering the intended run time.

%% \cite{gulati2011cloud} scheduler becomes difficult to scale as more and more parameters must be considered.

%% Thematically closest from the scheduling community is gang scheduling.
%% Threads belonging to the same process are scheduled together to achieve better cache hit rates.

%% VM scheduling?
%% Energy efficiency?

% Alternative approaches for conserving energy in the data center propose hardware changes.
% Instead of building hardware that achieves energy proportionality at all utilization levels,
% it may suffice to frequently switch between a single low-power idle and a full-power active state~\cite{meisner2009powernap}.
% Utilizing hardware heterogeneity is another viable option to achieve power efficiency~\citep{heath2005energy}.
