\section{Problem}
\label{sec:lease:problem}

%% Cloud computing and the related movement to outsource IT infrastructure is enjoying a huge success.
%% Consolidating IT infrastructures is cost effective,
%% because it exploits the economies of scale.
%% However, energy is a constantly increasing cost factor for IT infrastructures.
%% Data center operators and the research community are investigating novel ways for reducing the data center's hunger for kilowatts.

%% Energy can be conserved in various ways inside a data center.
%% However, the single, highest payoff point of optimization is the server itself.
%% According to \citet{hamilton2009cooperative}, servers consume around 60\% of the total energy in a data center.
%% Approaches to save energy in servers fall into two categories~\cite{gandhi2011case, barroso2007case}:
%% (a) making individual components energy-proportional, and
%% (b) utilizing (deep-)sleep states with near-zero energy consumption.
%% Energy-proportional server hardware is an active research area,
%% but it will still take some time before products are available.
%% Also, this does not help with already deployed servers.
%% As an immediate and effective mean of saving energy,
%% we advocate to power down unused servers.

We study the problem of scheduling virtual machines in an Infrastructure as a Service cloud.
Our main focus is on the potential energy savings gained by assuming that the run time of each VM is known.
By co-locating VMs which terminate at approximately the same time,
we hope to reduce the overall number of powered up physical machines.
In fact, powering down servers, seems to suit the IaaS paradigm particularly well.
In other usage scenarios, where server utilization is seemingly low,
secondary purposes may prevent shutdown,
e.g., distributed, in-memory caches or holding redundant copies of persistent storage items~\cite[-3cm]{ghemawat2003google}.
Even without secondary purposes,
the relatively long transition times of multiple seconds between on and off states,
is a show stopper if idle periods are on the order of milliseconds~\cite[-2.3cm]{meisner2009powernap}.
For IaaS offerings, however, servers only host virtual machines and can be turned off as soon as no VM is running anymore.

Once we decided that turning off servers is the best short-term solution,
the question becomes, how to maximize the ``time off''?
\citeauthor{tolia2008delivering} used virtual machine migration for the same purpose~\cite[-3.3cm]{tolia2008delivering}.
However, while virtual machine migration allows for a more dynamic resource allocation strategy,
there also exist good reasons to avoid it:
VM migration involves transferring (potentially) gigabytes of in-memory and on-disk state~\cite[-2.4cm]{mashtizadeh2014xvmotion},
reducing the network and disk I/O capacity available to the actual applications.
Further, even though live virtual machine migration is less disruptive than offline migration,
the execution must still be paused from anywhere between 0.01 to 10 seconds; and possibly longer.
The exact time depends on the VM's page dirtying rate and configuration~\cite{clark2005livemigration}.
For those reasons, some people see migration as a valuable tool for managing VM populations,
while others, for example, Amazon EC2 and Microsoft Azure, do not use migration at all.
%% ~\footnote{
%% Mentioned by Chris Schl\"ager,
%% Managing Director of the Amazon Development Center (Germany), during a presentation at TU Dresden on 22 May, 2014.}
In any case,
we do not aim to replace virtual machine migration as a tool for vacating servers.
Instead, placing virtual machines more carefully in the beginning,
reduces the need to migrate them later on.

In order to make a more informed initial placement,
we propose to introduce a new purchasing option (in Amazon EC2 parlance) called \emph{timed instance}.
Timed instances are a middle ground between on-demand and spot instances.
Timed instances are guaranteed to exist for the whole time of the specified allocation,
just like on-demand instances.
In contrast to on-demand instances,
timed instances have a fixed lease time specified by the requesting party.

We envision the price of a timed instance to be between on-demand and (typical) spot prices.
The customer pays a premium for guaranteed, unbounded compute time with on-demand instances.
Timed instances are cheaper than on-demand instances, because timed instances allow the provider to manage resources more efficiently.
Spot instances are cheaper still than timed instances:
there is no guarantee of availability at a fixed price and spot instances face the risk of early termination.
Exactly how much cheaper timed instances can be priced at,
depends on the savings the provider manages to achieve based on the lease time information.
The purpose of this study is to find out just how large those savings are.

%% http://spot.scem.uws.edu.au/ec2si/Home.jsp?Region=4&AvailabilityZone=eu-west-1a&InstanceType=m3.xlarge&Product=Linux%2FUNIX&TimeStartDay=01&TimeStartMonth=08&TimeStartYear=2014&TimeFinishDay=31&TimeFinishMonth=08&TimeFinishYear=2014&Submit=Submit&MultiDataCount=1&AvailabilityZone_1=eu-west-1a&InstanceType_1=m1.large&Product_1=Linux%2FUNIX&GraphColour_1=%23FF0000

\begin{marginfigure}
  \includegraphics{figs/lease/spot-prices}
  \caption[Amazon EC2 spot prices]{Historical spot price for EC2 m3.xlarge Linux instances in Amazon's EU data center during the month of August 2014.
For comparison, the listed price for on-demand instances of the same type was 30.8 Cents,
more than 7 times more expensive than the baseline spot instance price.}
  \label{fig:lease:spot-prices}
\end{marginfigure}

%% Without lease time information scheduling strategies are rather simplistic:
%% virtual machines can be spread evenly across all server using a round robin scheduler.
%% Alternatively, a first fit scheduler can pack each server with the maximum number of virtual machines,
%% before moving on to the next server.

%% possible strategies for allocating VMs to PMs: round robin, first fit.
%% both have no knowledge (or only statistical knowledge) about runtime of virtual machine.
%% let customer specify runtime of VM when requesting resources.
%% assign vms to pms based on this additional information.
%% The expectation is that the additional information leads to better scheduling decisions.
%% As a result, more machines are completely idle for longer time spans.
%% This translates into reduced energy costs for the provider.
%% Part of this cost saving is handed to the customer,
%% by pricing the timed instances between established on-demand and spot instances.

%% Three main assumptions:
%% \begin{enumerate}
%% \item There exist periodically recurring tasks, for which the processing time is fixed and known.
%% \item The user wants to save money, which is why he eschews on-demand instance as they are expensive.
%% The user also hates the unreliability of spot-instances, where his computation may be terminated any time.
%% The user would gladly take up the opportunity to have a no-early-termination guarantee bundled with a price that is lower than on-demand instances.
%% In return, he would make the effort of giving the provider the needed lease time information.
%% \item 
%% \end{enumerate}

To summarize,
our goal is to reduce energy usage in IaaS clouds by increasing the time servers are powered down.
We do not use virtual machine migration to re-shuffle VMs between servers because of the limitations it entails.
Rather, we assign VMs to servers based on the VM's requested run time,
co-locating VMs with similar expiration times.
To this end,
we propose to include the intended lease time with the resource request sent to the provider.

\section{Model}
\label{sec:lease:model}

In this section,
we present the system model,
introduce notation,
and articulate assumptions underlying our evaluation.

\subsection{Terminology}

A \emph{virtual machine (VM)} is an entity that must be scheduled.
Virtual machines enter the system randomly following according to some probability distribution.
Entering the system is synonymous with ``a customer requests a new virtual machine instance''.
Virtual machines are hosted by \emph{physical machines (PM)}.
Each PM can host a certain number of VMs simultaneously.
The exact number of VMs depends on the PM's available resources and the resources requested by the VMs.
A valid scheduler must honor this maximum, i.e., we do not oversubscribe resources.
Each VM has a \emph{run time},
alternatively called \emph{lease time} or \emph{reservation time}.
The run time defines how long the VM stays in the system.
Once a VM has been assigned to a PM,
it occupies a certain amount of resources on this PM for the duration of its run time.
After a VM's run time has elapsed,
the VM's slot on the PM becomes available again.
The sequence of VMs entering the system together with their run times defines a \emph{workload}.

A \emph{configuration} is an assignment of VMs to PMs.
Two events cause the configuration to change:
(i) a VM entering and (ii) a VM leaving the system.
For the first case,
the scheduler defines how the configuration changes, i.e.,
it assigns the VM to an available PM.
When a VM expires, the configuration changes independently of the scheduler.
The resources previously used by the expired VM become immediately available again.
More formally, a \emph{scheduler} can be viewed as a mathematical function that,
given a configuration and a VM, produces a new configuration.

A single simulation \emph{run} consists of individual \emph{rounds}.
The first step during each round is to generate,
according to a workload model explained in the next section,
a list of VM requests.
The scheduler takes this list of requested VMs and assigns them to PMs.

Note, that the scheduler has access to the entire list of VMs to be scheduled during a single round.
This allows for optimizations which require knowledge about other, ``concurrent'' requests.
For example, if multiple VMs with identical run times are requested,
the scheduler can put all of them together on a dedicated server.
Identifying sets of VMs to schedule as a group rather than individually is impossible when scheduling each VM in isolation.
%% Tufte book does not allow for forward references as
%% chapters/sections are not numbered ...

Because we are ultimately interested in energy savings,
a scheduler's performance is measured by how many PMs are occupied and how many are vacant during each round.
A PM is occupied or up if it hosts at least one VM, otherwise the PM is \emph{down}.
The \emph{uptime} of a single PM is defined as the number of rounds for which it is up.
For example, if we assume each simulation step represents one minute of real time and a PM hosts a single VM for 30 steps,
the PM's uptime is 30 minutes.
Analogously, the uptime over all PMs, called \emph{cumulative machine uptime~(CMU)},
is simply the sum of each individual PM's uptime.
Clearly, the goal, i.e., \emph{objective function},
is to reduce the number of physical machines and minimize their cumulative machine uptime.

We make the simplifying assumption of ignoring fractional energy costs for servers running at less than 100\% utilization.
We focus on the machine uptime instead, because energy consumption between 1\% and 100\% utilization increases linearly,
and we only want to eliminate the excessive energy wastage at 0\% utilization.
Further, even if a server is populated with a large number of VMs,
the server's actual utilization is still close to zero, if all the VMs are idle too.
Figure~\ref{fig:lease:nr-vms-vs-cpu-utilization} illustrates this:
we first measured the server's idle CPU utilization which was, unsurprisingly, zero.
Then we started 24 virtual machines, each with one virtual CPU and $512\,\textrm{MiB}$ RAM.
Once all the VMs had booted, the server's overall CPU utilization was still zero.
Hence, a system's CPU utilization cannot be inferred only based on the VM count.

\begin{marginfigure}
  \includegraphics{figs/lease/nr-vms-vs-cpu-utilization}
  \caption[CPU utilization vs. number of VMs]{The actual physical CPU utilization shows no apparent correlation to the number of virtual machines running on the host.}
  \label{fig:lease:nr-vms-vs-cpu-utilization}
\end{marginfigure}

As we already mentioned in the thesis' background chapter,
the CPU is only one among a set of essential compute resources.
While the CPU may be idle, other resources, for example,
memory, can have a totally different utilization statistic:
24 idle VM instances still occupy $12\,\textrm{GiB}$ of main memory.

In the context of our simulation-based study,
we define \emph{load} as the total number of virtual machines running across all servers.
This is in line with our observation that while some resource are virtually unused,
the VM instances still utilize memory.
If resources are guaranteed, i.e., they are not overcommitted,
then memory typically tightly constraints the number of virtual machines on a server and the total cluster.

%% In Section~\ref{sec:schedulers} we present \nAlg algorithms that minimize the objective function.

\subsection{Workload}

A time-varying load curve is a key assumption for our approach to yield savings.
Coping with time-varying load can be as simple as always provisioning for peak load.
This, however, is wasteful, because peak load only lasts for a small fraction of the time.
A better strategy is to activate (e.g., power on machines) or lease resources according to the current demand.
If, on the other hand, load does not vary over time, provisioning is trivial.

Diurnal load patterns are typical for ``web workloads'', i.e.,
web services receive the highest number of requests around noon;
with a trough of requests at night.
Online Data-Intensive services~(OLDI)~\cite{meisner2011power} is a second, web-related workload class.
As described by \citeauthor{meisner2011power}, OLDI services show a diurnal load pattern where the peak load is three times higher than the trough load.
We use this as the basis for our simulated workloads, which also have a factor of three difference between peak and trough.
For more examples on time-varying workloads see, for example, \citet{feitelson2014workload}~\cite{feitelson2014workload}.
%% \note{One sentence why peak/trough ratio is important. The higher the ratio, the higher the potential savings, right?}

\begin{figure}
  \includegraphics{figs/lease/cgc2012_optimal}
  \caption[Number of occupied physical servers]{Number of occupied physical machines over time for different schedulers.}
  \label{fig:optimal}
\end{figure}

An important observation here is that savings can only be realized during periods of \emph{decreasing} demand.
As long as demand grows, more physical resources must be activated to meet it.
Only when demand decreases again, can resources be freed.
The goal is to release resources, i.e., physical servers, earlier such that they can be powered off to save energy.
In our IaaS scenario, decreasing demand translates into a decreasing total number of VMs in the system.
As a result of fewer virtual machines,
the total number of switched on PMs should decrease accordingly.

Using clever scheduling, based on the intended lease time,
PMs should become vacant sooner and can be transitioned into low-power modes earlier.
Figure~\ref{fig:optimal} illustrates the time-varying demand for physical machines based on our simulations.
The graph plots the number of used physical servers over time for three different schedulers.
Even without exact knowledge of the schedulers,
it is apparent that some schedulers are worse than others, i.e., they use more physical machines.

\subsection{Workload Parameters}

To the best of our knowledge,
there exists no publicly available data set describing VM properties,
such as runtime distribution, typical configurations, or inter-arrival time,
in an infrastructure cloud.~\footnote{Since then, \citeauthor{birke2012datacenters} published a study based on data collected from IBM cloud data centers.}.
To compensate for this, we identified a set of parameters to describe infrastructure cloud workloads and use a range of possible values for each to evaluate the impact of timed instances.
As we will see in greater detail later,
timed instances decrease the cumulative machine uptime in every case.
In particular, we focus on the following six parameters:

\begin{itemize}
\item number of physical machines/servers/hosts
\item server heterogeneity
\item run time distribution
\item instance sizes
\item purchase type
\item batch instances
\end{itemize}

\noindent
We will describe each parameter in the following paragraphs.

%% We model a data center as a set of physical machines
%% $$DC = \{PM_0, PM_1, \ldots, PM_n\}$$.
%% Each $PM_i$ has $s$ slots available for VM hosting,
%% where $s$ determines the maximum number of concurrently running VMs.
%% Each VM occupies one or more slots.
%% The sum of all slots occupied by VMs on a PM must be less or equal to the slots supplied by this PM.

%% All PMs and VMs have identical configurations, i.e.,
%% we do not consider heterogeneous virtual or physical machines here and leave it for future work.
%% Given the number of PMs $|DC|$ and the number of slots $s$,
%% the maximum number of parallel running VMs is $|DC|*s$.
%% This figure is important when generating the artificial workloads,
%% because peak load should be close to this maximum.

\paragraph{Number of hosts}

To simulate variable size data centers we varied the number of hosts.
Exploring different data center sizes allows us to see whether savings are correlated to the number of machines.
The data center size affects the total number of handled requests as well as their inter-arrival time,
because the workload generator must consider the available physical resources to not ``overwhelm'' the data center.
Recall, that a key assumption is that there are always enough resources to satisfy all requests, i.e.,
requests are never rejected nor does the scheduler maintain a request backlog.

Considering variable-sized data centers is interesting,
because, according to the EPA~\cite{epa-datacenter-report},
the majority of energy is consumed by small to medium sized data centers.
Consequently, even if small data centers yield relatively small savings the absolute savings may still be considerable given their sheer numbers.

\paragraph{Server heterogeneity}

A homogeneous server population eases management and service of the data center.
However, over time, due to machine failures or novel features in next generation servers,
the server population almost certainly becomes heterogeneous.
We model heterogeneity by varying the number of \emph{resources} per server.
We use resources as an abstract measure to quantify a server's hardware capabilities.
Using an abstract measure simplifies resource allocation because the scheduler must not match individual resources, e.g., CPUs, RAM, and storage.
Virtual servers, as rented out to customers,
then have fractional sizes of the original server hardware, e.g., 1/8, 1/4, 1/2, or 1/1.
Individual server resources, such as CPU, RAM, and local disk, double between virtual machine ``sizes''.
For example, a small instance may have one CPU, 1~GiB memory, and 100~GB storage.
The next instance size has 2~CPUs, 2~GiB memory, and 200~GB storage.
Popular cloud providers, such as Amazon and Google, also follow this scheme of predetermined VM sizes.
Hence, we consider this an appropriate choice for our simulation.

%% Two things we exclude in our simulation is oversubscribing resources and virtual machine migration.
%% Oversubscribing is problematic when instances simultaneously utilize the requested resources fully.
%% The provider can hand down the cost of not oversubscribing to the consumer.
%% The consumer can then choose what best fits theirs needs.
%% Live migration has different limitations,
%% but customers should also be able to chose their provider based on whether live migrations of instances are to be expected or not.

We assume that less powerful servers have a worse performance per watt than more powerful machines.
In our context, a more powerful machine can host more VM instances simultaneously than a less powerful machine. 
The increased performance per watt is due to more recent and more energy-efficient hardware.
New server generations may even consume \emph{less} peak power than previous generation servers.
Energy-wise, it makes sense to prefer more recent, powerful machines,
reserving the use of old machines for peak load.

\paragraph{Instance size}

As with homogeneous servers,
homogeneous instance sizes make scheduling easier because a fixed number of instances will fit on each server.
However, customers have diverse resource needs.
As such, they want to mix and match the resource sizes to their computing demands.
Low-frequency web servers will run on ``small'' instances,
whereas compute intensive scientific applications will run on ``extra large'' instances.

A problem related to variable instance sizes is \emph{fragmentation}~\cite[-0.6cm]{gulati2012drs}.
Fragmentation is the phenomenon where the global resource aggregate indicates sufficient capacity yet there is no single host with sufficient resources to host the VM.
Take, for example, two servers with two free resource units each.
In aggregate, hosting another VM requesting 4 resource units seems possible,
yet there is no single host with a remaining capacity of 4 resource units.

%% We consider 4 different instance sizes: small, normal, large, and extra large.
%% The instance sizes consume 1, 2, 4, and 8 resources, respectively.

\paragraph{Purchase type}

While we are primarily interested in timed instances,
it seems unlikely that an infrastructure provider would host only this kind of VM.
The infrastructure provider Amazon, for example,
offers two different purchase types: on-demand and spot instances.
On-demand instances have a fixed price, typically higher than spot instances,
and only the user determines when the instance terminates.
Spot instances are, in the common case, cheaper than on-demand instances.
However, because the spot instance price varies and customers specify a maximum price they are willing to pay,
the provider terminates the instance when the current spot price increases above the user-specified limit.

Timed instances, which we investigate in this work,
essentially represent a third purchase type.
Timed instances have an a priori specified reservation time of fixed length.
The scheduling algorithm uses the reservation time to co-locate instances with similar expiration times.
We based our energy-aware scheduler on this purchase type to reduce the overall energy consumption.
For easy reference,
Table~\ref{tab:lease:purchase-types} lists the defining characteristics of each purchase types.

\begin{table}
\begin{tabular}{l|l|l}
  purchase type & reservation length & price\\
  \hline
  on-demand & indefinite    & high\\
  timed     & fixed         & medium\\
  spot      & unpredictable & low\\
\end{tabular}
  \caption[Amazon EC2 purchase type comparison]{Comparison of different purchase types according to reservation length guarantees and price.}
  \label{tab:lease:purchase-types}
\end{table}

\paragraph{Run time distribution}

Again, because we are missing any empirical data on the runtime distribution of virtual machines in infrastructure clouds,
we investigate two possible distributions,
normal and log-normal,
while also varying their respective parameters.

The normal distribution is defined by its \emph{mean}~$\mu$ and \emph{scale}~$\sigma$ parameter.
We set the mean VM runtime to either 2 or 4 hours,
while the shape parameter $\sigma$ is either 0.5, 0.75, or 1.0.
Resulting instance run times are rounded to full hours,
because this is the accounting granularity used by large cloud providers, for example, Amazon.
While hourly billing cycles may seem wasteful if the computation finished 5 minutes after the full hour,
it allows the leasing party to incorporate some slack when specifying the lease time.
Restricting runtimes to multiples of one hour also makes sense because of the human preference for round numbers~\citep[sec. 9.1.6]{feitelson2014workload}~\citep{reiss2012trace}.

Because the probability distributions we use to generate VM runtimes produce continuous values,
we mathematically round the random value to the closest full hour.
In case of the normal distribution, which may generate negative values,
all runtimes of less than 30 minutes are ``rounded'' to one hour.

\pagebreak
The lease times are fixed, i.e.,
it is not possible to extend or shorten the lease time after issuing the request.
Although interesting, we leave scheduling under probabilistic deadlines for future work.

As an alternative, we also investigate VM runtimes following a \emph{log-normal} distribution.
The log-normal distribution is an interesting alternative because its random values are asymmetrically distributed around the mean.
Our expectation is to have more VMs with longer runtimes compared to a normal distribution,
because of the ``right-tailed'' nature of the log-normal distribution.

After the initial publication of our results,
\citet[Sec. IV]{birke2013state}~\cite{birke2013state} published an empirical study about various metrics within a private cloud environment.
Among the many interesting workload characteristic, the study also contained information about the observed runtime of virtual machines.
The study states:

\begin{quote}
In general, we see that for almost 90\% of the VMs the on/off times are less than a portion of the day,
while a small percentage corresponds to very long on/off durations.
\end{quote}

\noindent
The number of instances which run for less than 30 minutes (0.01 days) is also surprisingly high at 50\%.
As to the actual distribution of runtimes,
the study does not provide sufficient data to make an informed statement.
The difference between our assumed workload characteristics and the recently published empirical data should be kept in mind when interpreting the results.

\begin{table}
  \begin{tabular}{l|l|l|lll}
                &      & Standard  & \multicolumn{3}{c}{Percentiles}\\
                & Mean & deviation & 5\%  & 50\% & 95\%\\
\hline
Time On [days]  & 0.39 & 1.91      & 0.01 & 0.01 & 1.19\\
Time Off [days] & 0.25 & 1.13      & 0.01 & 0.04 & 1.00\\
  \end{tabular}
  \caption[VM on/off intervals]{Virtual machine runtimes for multiple private clouds as reported by \citet{birke2013state}.}
  \label{tab:lease:on-off-vm-intervals}
\end{table}

\paragraph{Batch Instances}

While instantiating of a single VMs is common use case,
some workloads, e.g., MapReduce computations,
require the concerted deployment of multiple instances.
When requesting multiple instances simultaneously, we call them \emph{batch instances},
it is a reasonable assumption that they all have similar runtimes.
Even without explicitly stating the lease time upfront,
it makes sense to co-locate instances belonging to the same batch.
All instances within a batch have identical resource sizes.
For timed batch instances the runtime is known,
while for other purchase types the runtime is unknown,
but still the same for each instance.
Hence, even without knowing the exact runtime,
schedulers can tacitly rely on the runtime being identical for all instance of a single batch.

%% Two factors govern the number of VMs in the system:
%% \begin{enumerate}
%% \item the inter-arrival time of VM instance requests, and
%% \item the instance run time
%% \end{enumerate}

%% We use an exponential distribution $exp(\lambda)$ to model the inter-arrival time (Poisson process).
%% The instance run time follows either a normal $norm(\mu, \sigma)$ or log-normal $lognorm(\mu, \sigma)$ distribution.
%% We varied the parameters $\lambda$, $\mu$, and $\sigma$, to determine how sensitive the results are to changes in those parameters.

\subsection{Workload Generator}
\label{sec:lease:workload-generator}

The previous section outlined the workload parameters and characteristics which we want to model.
We now explain the principles underlying our workload generator.
Figure~\ref{fig:requests-resources-hosts} illustrates, based on a synthetic sample workload,
relationship between (i)~virtual machine instances, (ii)~occupied resources, and (iii)~occupied servers.
The three plots show how each metric evolves over the course of a single simulated day (1440 minutes).

\begin{figure*}[t]
  \includegraphics{figs/lease/cloudcom2012_requests-resources-hosts}
  \caption[Relationship between instances, resources, and hosts]{Relationship between (a)~instances, (b)~resources, and (c)~hosts.}
  \label{fig:requests-resources-hosts}
\end{figure*}

The left-most plot shows the number of virtual machine instances over time.
As is typical for a diurnal workload pattern,
the instance number is highest around noon with a trough at night~\cite{meisner2011power,barroso2007case,reiss2012trace}.
An important factor is the by how much the peak and trough workload differ,
expressed as the \emph{peak-to-trough ratio}.
As stated earlier, we target a peak-to-trough ratio of 3:1.
Whereas the peak-to-trough ratio of VM instance is closer to 2.5:1,
the peak-to-trough ratio for the raw resources (center figure) is much more ``on target''.

The number of occupied resources depends on the sheer number of instance but also on their size.
In the simplest case,
there is only a single instance size, i.e.,
the number of used resources is a multiple of the instance count.
However, if the instance population is heterogeneous,
a more realistic assumption,
the total resource consumption is the sum of each instance size times the instance count of this size.
The example workload of Figure~\ref{fig:requests-resources-hosts} shows a mix of instance sizes,
evidenced by the similar but not identical shape of the left and center curve.

The number of utilized host machines is again related to but different from the instance count and occupied resources.
The curve's shape, rightmost plot of Figure~\ref{fig:requests-resources-hosts},
resembles the other two plots,
but it is smoother and less jagged.
The reason is that even if, for example, the number of instances changes,
the host count may well remain the same.
For example, if a running host has sufficient resources to host the new VM,
there is no need to power up a new host.
Similarly, if an instance expires, i.e., the VM count decreases,
the host may remain powered, because other instances are still using it.
Only when the last instance terminates,
does the host count decrease.

We further observe that the instance and resource count only depend on the workload generator and are,
in particular, independent of the VM scheduler,
the number of occupied hosts depends exclusively on the scheduler.

\pagebreak
\newthought{Workload generation is related to} time series generation but not identical.
We can consider each plot of Figure~\ref{fig:requests-resources-hosts} as an individual time series and generate it according to some model.
However, some ways of arriving at a workload are easier than others.
For example, generating a time series for the resource count is only the first step.
The resource count must be translated into a concrete number of instances, instance sizes, and runtimes.
Instead, we pick parameters for the inter-arrival and runtime distribution of VMs such that we arrive at the desired load curve.

We define the base resource usage $b$ to be $1/3$ of the peak resource usage

$$b = \frac{1}{3} (\textrm{number of hosts}) \times (\textrm{host size})$$

\noindent
For a mixed host population, the equation becomes

$$b = \frac{1}{3} \sum_{t \in \textrm{host type}} (\textrm{number of hosts}_{t}) \times (\textrm{host size}_{t})$$

Using the base resource usage $b$ as our target resource usage,
we have to find suitable parameters $\lambda$, for the exponentially distributed inter-arrival time,
as well as $\mu$ and $\sigma$ for the (log-)normal runtime distribution.
The exponential distribution is defined by its singular $\lambda$ parameter,
which signifies the mean arrival frequency.
Conversely, the reciprocal of $\lambda$, $\frac{1}{\lambda}$,
is the mean inter-arrival time.
For the symmetric normal distribution,
$\mu$ determines the random variable's mean, i.e., the mean VM run time in our case.
Based on this information,
the following formula captures the relationship between the parameters and the system's load:

\begin{equation*}
\begin{split}
\textrm{resource usage} & = (\textrm{mean VM runtime}) \times (\textrm{mean inter-arrival time})\\
                          & = \mu \times \lambda
\end{split}
\end{equation*}

Because we know the target resource usage, i.e., our base load,
and the mean VM run time,
we can calculate the corresponding mean inter-arrival time.

$$\textrm{mean inter-arrival time} = \frac{\textrm{resource usage}}{\textrm{mean VM runtime}}$$

Picking the parameters in this way,
leads to an actual resource usage that oscillates around the targeted resource load.
% Not sure where the following insight came from: does not seem to be true.
% The degree of oscillation depends on the run time distribution's shape parameter $\sigma$.
For simplicity, we set the shape parameter $\sigma$ to one-third of the average runtime, i.e.,
$\sigma = \mu/3$.
Note that the shape parameter does not influence the system's load,
as the shape only determines the ``diversity'' of VM runtimes.
A small $\sigma$, leads to runtimes tightly clustered around the mean,
whereas a large $\sigma$ increases diversity albeit without changing the average.
Figure~\ref{fig:lease:base-load-generator-example} shows two example workloads with different target loads created according to the scheme above.

\begin{marginfigure}[-4.2cm]
  \includegraphics{figs/lease/base-load-generator-example-margin}
  \caption[Random workload curves]{Random workload curves for two target loads of 500 and 1000.}
  \label{fig:lease:base-load-generator-example}
\end{marginfigure}

\pagebreak

\newthought{We determined the parameters} for the log-normal distribution in a similar fashion.
The log-normal distribution's $\mu$ parameter is defined in terms of the distribution's mean and its variance

$$\mu = log(\textrm{mean}) - 0.5*(\sigma^2)$$

In our case, $\textrm{mean}$ is the average instance run time in minutes.
Defining the workload in terms of VM inter-arrival times and randomly distributed runtimes results in a stationary workload.
Once the system passed the warm up phase,
the workload oscillates around the base load.
The constant arrival of new instances and the expiration of old instances yield a steady state.

\newthought{What is needed}, in fact, is to make the distribution parameters time dependent, i.e.,
turning them into time-dependent functions $\lambda(t)$, $\mu(t)$, and $\sigma(t)$.
This would enable the workload generator to vary the inter-arrival time and runtime distribution with the simulation time.
To simulate the diurnal load curve, the inter-arrival time would be low at noon,
meaning faster arrivals, i.e, more work and higher load.
Similarly, the mean runtime could be increased during peak hours to let instances run for longer.

We decided to keep the ``static'' random process to generate the base load and have a second, time-varying random process to generate peak load.
Choosing the appropriate probability distribution parameters for the second random process requires care.
Ideally, the load curve should look bell-shaped.
We make two observations related to the load curve's shape:
first, the rising flank of the curve is primarily defined by the VM inter-arrival time.
The more VMs enter the system per time unit,
the steeper the load curve's rising slope.
The trailing slope's shape, on the other hand,
is predominantly determined by the run time distribution.
Using a normal distribution for the run time naturally yields a bell-shaped trailing slope.

Second, with a static setup of $\lambda$ and $\mu$,
it takes $\mu$ time units to reach the target load level.
For example, if the target load is 400 VMs and the mean run time is two hours,
choosing $\lambda$ as outlined above,
it will take two hours to reach the target load.
This is important, because the peak load (our target load) has to be reached within a bounded amount of time.
Using a large mean run time, e.g., 12 hours, it takes 12 hours to reach the target load.
Figure~\ref{fig:lease:workload-rampup-time-margin} illustrates relationship between average VM runtime and delay to reach a certain target load.

\begin{marginfigure}
  \includegraphics{figs/lease/workload-rampup-time-margin}
  \caption[Workload rampup time]{Workload rampup time for three different average VM runtimes.
As the inter-arrival time depends on the target load and average VM runtime,
increasing the average runtime, decreases the inter-arrival time, i.e.,
a fixed target load takes longer to reach as average VM runtimes increase.}
  \label{fig:lease:workload-rampup-time-margin}
\end{marginfigure}

The observations above, lead to the following setup:
a second load generating process is active from 8\,am until 1\,pm.
The second generator uses a mean run time of four hours.
Because the generator is active for five hours, i.e.,
one hour longer than is strictly necessary to reach peak load,
the resulting workload has a small ``plateau'' around noon.
Figure~\ref{fig:simulated-load-curve} shows an example workload generated this way.

%% \begin{figure*}
%%   \begin{tabular}{cc}
%%     \begin{minipage}[t]{0.5\textwidth}
%%       \includegraphics[width=1.0\textwidth]{figs/load-curve}
%%       \caption{Diurnal load curve with peak load occurring around noon.}
%%       \label{fig:load-curve}
%%     \end{minipage}
%%     &
%%     \begin{minipage}[t]{0.5\textwidth}
%%       \includegraphics{figs/simulated-load-curve}
%%       \caption{Three days of simulated load for 20 physical machines (PM) and a maximum of 16 VMs per machine.}
%%       %% exp__phy_machines20__runtime_genNormalGenerator__runtime_mean120__vms_per_pm16
%%       \label{fig:simulated-load-curve}
%%     \end{minipage}
%%   \end{tabular}
%% \end{figure*}

\pagebreak

\begin{figure}
  \includegraphics{figs/lease/cgc2012_simulated-load-curve}
  \caption[Simulated load example]{Three days of simulated load for 20 physical machines (PM) and a maximum of 16 VMs per machine.}
  %% exp__phy_machines20__runtime_genNormalGenerator__runtime_mean120__vms_per_pm16
  \label{fig:simulated-load-curve}
\end{figure}

\subsection{Simulator}

We wrote a discrete event simulator in Python~\footnote{Code available at \url{https://bitbucket.org/tknauth/cgc2012/}.}.
Each simulation step represents a progress in time by one minute.
Before any VMs are created, expired VMs are removed from their respective host machines.
After removing expired VMs, a random number of VMs are created according to the above outlined principles.
Each VM is assigned a random run time also according to the outlined principles.
The scheduler is responsible for assigning newly created VMs to available host machines.
These steps are repeated for a given number of iterations.
For example, two simulated days require 2*24*60 iterations.
The simulator tracks metrics, such as the number of occupied physical machines,
which are used to compare the different schedulers.
