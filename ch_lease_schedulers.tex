\section{Schedulers}
\label{sec:lease:schedulers}

This section describes two scheduling algorithms we developed.
The algorithms optimize the assignment of virtual to physical machines based on the VMs run time.
Figure~\ref{fig:example} depicts an example that we use to explain how each algorithm works.
The figure shows two physical machines, PM\,1 and PM\,2.
In our example, each PM\,1 and PM\,2 can each host four virtual machines simultaneously.
PM\,1 runs three virtual machines,
each of which will terminate at $t=3$.
PM\,2 only has one virtual machine,
terminating at $t=2$.

\begin{figure}
  \centering
  \includegraphics{figs/lease/cgc2012_example}
  \caption[VM scheduling example]{Example with two physical machines, running virtual machines (blue),
and requested virtual machine (orange).}
  \label{fig:example}
\end{figure}

\subsection{Reference Schedulers}

We use a round robin and first fit scheduler as reference schedulers.
Round robin assigns the first VM to the first PM,
the second VM to the second PM, and so on.
In many infrastructure cloud setups round robin is the default scheduler.
The benefits of round robin scheduling are its easy implementation and its load balancing properties.
For example, the Essex version of the popular OpenStack framework uses a variant of round robin scheduling to load balance according to each host's available RAM~\footnote{\url{http://docs.openstack.org/essex/openstack-compute/admin/content/ch_scheduling.html}}.
However, while distributing requests among all machines is good for load balancing,
an energy-conscious scheduler would do the direct opposite, i.e.,
concentrate the requests on a small (sub)set of all available machines.

The second reference scheduler is first fit.
In the absence of any run time information, first fit is a good strategy for resource conservation.
First fit assigns all VMs to a single PM until the PM's resources are depleted.
At this point, first fit picks another PM and continues to pack as many VMs as possible on the new host.
We chose first fit as our second reference scheduler,
because it represents a more challenging baseline.
Our achieved savings should be smaller when compared to a first fit scheduler as opposed to a round robin scheduler.

\subsection{Scheduler 1 (S1)}

The first algorithm is based on the simple idea to have each virtual machine finish at about the same time.
Mathematically, we want to minimize the difference in termination times.
For each server, a score is calculated according to the following formula:

\begin{equation}
  \label{eq:alg1.1}
  score_i = \sum_{vm \in PM_i} |vm_{new}.finish - vm.finish|
\end{equation}

The sum is computed only for PMs that are neither empty nor full.
By only considering machines that already have at least one VM,
this algorithm can be seen as a variant of first fit.
New physical machines are only switched on,
if all powered on machines already hold the maximum possible number of VMs.
%% This is in line with the previously made observation that savings can only be gained at times when demand is decreasing,
%% i.e., at the falling slope of the load curve.

The VM is placed on the PM with the lowest score.
If all PMs host either zero or the maximum number of VMs,
the VM is scheduled on some currently unoccupied PM.

Turning to our example:
PM\,1 has a score of $3*1$ because the expiration time difference between the \emph{three} existing instances and the new instance is \emph{one} time unit.
PM\,2 gets a score of $1*2$ because it only has a single instance running with a delta of 2 time units relative to the new instance.
The scheduler S1 would assign the instance to PM\,2 because its score is lower.

However, this is actually a bad choice.
Scheduling the VM on PM\,1, extends the time PM\,1 needs to be up by one time unit.
If the VM would have been scheduled on PM\,2, PM\,1 could be turned of at $t=3$.
In fact, this was our first version of the scheduler.
However, an initial evaluation showed that this scheduler performed consistently worse than first fit.
Instead of consolidating the virtual machines, the scheduler spreads the VMs.
The reason is, that PMs with a high number of VMs automatically have higher scores.
To remedy this, the initial score (Equation~\ref{eq:alg1.1}) is divided by the number of virtual machines:

\begin{equation}
  \label{eq:alg1.2}
  score_i = \frac{\sum_{vm \in PM_i} |vm_{new}.finish - vm.finish|}{|PM_i|}
\end{equation}

\noindent
As a result,
the second version of this scheduler consistently outperforms first fit.

\subsection{Scheduler 2 (S2)}

Our second algorithm is a variation on the idea of the first algorithm.
Instead of minimizing the difference in ending time for all VM instances on a PM,
only the difference to the VM with the longest remaining run time is considered.
The reasoning is that as long as the new VM does not increase the PMs uptime, this is good.
The score for this scheduler is simply the difference between the new VM's ending time and the VM with the longest remaining run time.
If $vm_{max}$ signifies the VM with the longest remaining run time,
the scoring formula becomes

%% \[score = \frac{\sum_{vm \in PM_i} vm.finish - vm_{new}.finish}{|\{vm | vm \in PM_i : vm.finish > vm_{new}.finish\}|}\]
%% \[vm_{max} \in PM_i, \forall vm \in PM_i : vm_{max}.finish \ge vm.finish\]
\[score_i = vm_{max} - vm_{new} \]

\noindent and VM is put on the host with the lowest score.
Note that the score may be less than zero,
if $vm_{new}$ has a termination time exceeding that of all running VMs on a host.
As long as there is a single PM with a positive score,
the VM is scheduled on the PM with the lowest positive score.
If the score is negative for all PMs,
meaning that the new VM runs longer than any of the existing VMs,
the scheduler picks the PM with the highest score.
The reasoning is that when all scores are negative,
choosing the PM with the highest score is synonymous with putting the instance on the PM with the longest remaining runtime.

In our example (see Figure~\ref{fig:example}), PM\,1 scores $-1$ and PM\,2 scores $-2$.
The VM is scheduled on PM\,1, because the scores are all negative.
Because the new VM runs longer than any existing VM,
the new VM is assigned to the highest scoring PM, i.e., PM\,1.
Again, the idea is that by putting the instance on PM\,1,
its runtime is extended by only a single time unit,
while PM\,2's runtime would have been extended by 2 time units.
PM\,2 can now potentially be shut down one time unit earlier.

\subsection{Scheduler 3 (S3)}

This scheduler is a variation of Scheduler 2 and only used for the evaluation's second part.
Scheduler 3 incorporates features required for more realistic workloads consisting of multiple instance sizes,
heterogeneous hosts, mixed purchase types, and batch requests.

Scheduler 3 uses the same scoring function as S2 to assign a score to each server.
The lowest scoring server receives the instance.
This basic scheduling algorithm is adapted as follows:
(1)~Timed instances and on-demand instances occupy non-intersecting sets of hosts, i.e.,
each host only runs either timed or on-demand instances.
The motivation is that we do not want cancel the benefit of knowing the runtime for some instance by co-locating them with instances of unknown runtime.
Hence, when scheduling timed instances,
only servers already hosting timed instance are considered.

(2)~If the server population is heterogeneous, 
the scheduler fills up powerful machines first.
Only if an instance does not fit into within a certain server class,
is the next, less powerful server class considered.

\subsection{Batch-awareness}

For scenarios with batch instances,
each scheduler can operate in a batch-aware mode.
If this is enabled, the scheduler tries to pack as many of the instances together on a single host.
If the remaining instances do not completely fill an empty host anymore,
they are scheduled according to the normal scheme, i.e.,
compute per host scores and assign the instance to the lowest/highest scoring host.

Co-locating instances stemming from the same request is sensible because it reduces the instances' communication distance.
Requesting multiple instances in a single operation is an indicator that those instances will cooperatively solve a problem or provide a service.
A small communication distance reduces message latency and cross-rack network traffic~\cite{meng2010improving}.
It also makes sense to co-locate batch instances from an energy-perspective.
Even if their actual runtimes are unknown,
as is typically the case with classic on-demand instances,
the probability of them expiring at approximately the same time is higher than for two random individually requested instances. 
