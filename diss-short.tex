\documentclass[a4paper]{scrartcl}

%% \bibpunct{[}{]}{,}{a}{}{;}

% force pdflatex to use A4 paper
\setlength{\pdfpagewidth}{210mm}
\setlength{\pdfpageheight}{297mm}

%\usepackage[pdftex]{graphicx}
%\usepackage{amsmath}
%\usepackage{color}
\usepackage{enumerate}
\usepackage{listings}
\usepackage{longtable}
\usepackage{multicol}
\usepackage{pgf}
\usepackage{tikz}
\usepackage{url}
\usepackage{hyperref}
\usepackage{layouts}
\usepackage{lipsum}
\usepackage{comment}
\usepackage[numbers]{natbib}
\usepackage[format=plain]{caption}
\usepackage{sidecap}

\usetikzlibrary{calc}
\usetikzlibrary{positioning}
\usetikzlibrary{decorations.pathreplacing,%
  decorations.pathmorphing}
\usetikzlibrary{shadows,backgrounds}
\usetikzlibrary{shapes.callouts}
\usetikzlibrary{shapes.geometric}
\usetikzlibrary{patterns}

\title{Energy Efficient Cloud Computing:\\%
Techniques and Tools}
\date{October 2014}
\author{Thomas Knauth}

\begin{document}

\newcommand{\high}[1]{{\color{red}#1}}
\newcommand{\todo}[1]{\footnote{\color{red}TODO: #1}}

\maketitle

%% \tableofcontents

\section{Introduction}

In times of worldwide declining fossil energy reserves and growing concerns about the environment,
the seemingly sleek and efficient IT industry has come under intensified scrutiny.
Modern data centers, forming the backbone of our digital life,
routinely consume as much energy as a small city~\cite[pg. 12]{how-dirty-is-your-data} even though the servers within are mostly idle.
Combined with growing energy prices,
it makes economic sense for the data center's owner to investigate potential energy saving options.
According to one analysis~\cite{barroso2009datacenter},
energy-related costs account for up to 22\% of a data center's total cost of ownership.

One technology that has transformed the IT industry significantly in the past decade was hardware virtualization,
especially for the ubiquitous x86 instruction set architecture.
The ability to create multiple, independent \emph{virtual machines} on a single physical machine enabled novel avenues for researchers and businesses to employ this technology~\cite{vogels2008beyond}.
Among them, and most relevant in terms of energy efficiency,
is the possibility to co-locate applications which previously ran on distinct physical machines.
Co-location reduces the \emph{server sprawl} where,
for reasons of isolation and fear of unintended interaction,
each service was provisioned on a separate server.

The major underlying research question of this work is \emph{if and how hardware virtualization helps to improve the energy efficiency in cloud data centers.}
We answer this question by presenting novel techniques and tools which utilize hardware virtualization and related technologies as their main building blocks.
In particular, our four central contributions are:

\begin{enumerate}

\item We examine the usage data of our 50-server research cluster.
We find that over the past 4 years only 15\% of the available compute time was allocated,
while the cluster was powered on non-stop.
Even though the cluster is not a cloud data center,
the techniques presented in latter parts of this thesis can be adapted to save energy within our research cluster.

\item We investigate if and how knowledge of the resource lease time in infrastructure clouds decreases the overall number of required physical servers.
We propose a new class of virtual machines called \emph{timed instances}.
In contrast to existing VM classes,
the cloud customer specifies the VM's reservation time when requesting the resources.
The infrastructure scheduler then co-locates virtual machines with similar termination times,
thus increasing the probability of a physical server to become completely empty.
Machines without any VMs can be powered off to save energy.

\item DreamServer combines activity-based VM life-cycle management with optimizations to quickly resume VMs~\cite{knauth2013fast-resume}.
In this sense, DreamServer realizes truly on-demand VM provisioning.
Idle VM instances are suspended to free resources for other tasks.
Further, suspending inactive VMs is a first step to power down the physical server,
which can only happen if there are no more active VMs.
On renewed activity, VMs are automatically and transparently resumed.
Clients only notice a slight increase in latency when a suspended VM must be resumed on an incoming request.

\item Our last contribution is in the area of periodic data synchronization at the block device layer.
When powering off servers to save energy,
data stored on those servers becomes inaccessible.
To keep data accessible, and for other purposes, such as periodic backups,
copies of the data must be refreshed at certain intervals.
Our ``dsync'' system enables an efficient synchronization by recording which blocks have been modified since the last synchronization.
Hence, ``dsync'' presents an alternative to established data synchronization methods and tools.
Its focus is on periodic block-level synchronization, for example, of virtual machine disks in a cloud environment.

\end{enumerate}

The dissertation's short version contains one section per contribution.
Each section explains the concepts and insights at a very high level,
along with the most important experimental results.
We conclude with a brief summary and possible future work.

\pagebreak

\section{Research Cluster Usage}

As an introduction to the topic of energy efficiency and to provide additional proof for the pervasively low server utilization~\cite{cloud-factories},
we analyzed more than 4 years of usage data from our in-house research cluster.
The cluster comprises 50 machines which, as is typical industry practice, are only powered off for maintenance.
During normal operation, even if nobody actually uses the machines,
they stay powered on.

\begin{figure}[]
  \centering
  \includegraphics{figs/intro/usage-per-week}
  \caption{Weekly utilization for the entire trace period.
  Long stretches of low utilization are interspersed with brief periods of heightened utilization.
  The overall utilization is at 15.09\%.}
  \label{fig:intro:usage-per-week}
\end{figure}

The results, shown in Figure~\ref{fig:intro:usage-per-week}, are revealing:
brief periods with peak usage of 40\% and more typically coincide with EU project or paper deadlines.
During those peak periods, which sometimes only last a single week,
users run lots of last minute experiments or prepare project demonstrators.
For the majority of the time, however, usage is low with an average usage of 15.09\% for the entire trace period.

Currently, we do not have any power management enabled for the cluster.
However, the workloads running on the cluster have frequent idle periods of multiple hours between jobs.
It is thus possible to employ full-system low-power modes,
for example, transitioning the servers into a ``suspend-to-ram'' state when they are unused.

The time to transition a server into and out of this low-power state is on the order of a few seconds.
Hence, when people access a suspended machine, they may have to wait a few seconds before the system responds again.
We deem the tradeoff between responsiveness and possible energy savings acceptable.
Typical job runtimes are on the order of a few hours and an added delay of a few seconds at the beginning is a minimal interruption.

\section{Online Scheduling with Known Resource Lease Times}

Cloud computing allows those in need of IT resources to acquire and release them swiftly.
The resources are typically delivered in the form of pre-packaged virtual machines, accessed over the internet.
This flexibility is good for the cloud user,
but creates tension at the provider side.
It is unknown to the provider for how long each resource is acquired for.
However, this knowledge is actually helpful when deciding which physical server to allocate the VM on.
Mixing VMs running for days with VMs only running for a couple of hours prevents shutting off the physical server,
because it is never completely unused.
The research question is if and to what extent a priori knowledge of the resource lease time helps the cloud provider to optimize the scheduling of resources.

Mixing long running and short lived virtual machines can be resolved by virtual machine migration.
If a single VM remains and prevents the physical server from being shut down,
the VM can be migrated to another physical server.
While VM migration allows for a lot of flexibility,
it also incurs costs.
The state associated with a single VM instance is measured in multiple gigabytes for the in-memory state,
along with potentially tens to hundreds of gigabytes for the on-disk state.
Ideally, migrations should happen rarely or not at all.
After all, a good initial placement decision reduces the frequency of necessary migrations.

Our goal is to reduce the number of required physical machines to service a given workload.
To this end, the scheduler incorporates the lease time into its scheduling algorithm,
which is only possible if the lease time is known a priori.
The general idea is to co-locate virtual machines with coinciding termination times.
A physical server can only be turned off once all virtual machines have expired.
Hence, aligning the VM's termination times potentially increases the time physical servers are eligible for shutdown.

Our initial work~\cite{knauth2012timed} used a simple model capturing data center size, server capacity, lease time distribution, and the distribution's parameters.
In our follow up work~\cite{knauth2012scheduling},
we extended the model to make it more realistic.
The extended model also considers requesting multiple VM instances simultaneously~\footnote{One request for $n$ instances instead of $n$ independent single instance requests.},
a mix of instances with known and unknown reservation lengths,
as well as heterogeneous host and VM sizes.
To evaluate our ideas, we built a discrete event simulator to compare different virtual machine scheduling algorithms.
Besides our own algorithms, we compared against established algorithms,
such as, first fit and round robin.

\pagebreak

\subsection*{Results}

\begin{figure*}[t]
  \includegraphics[width=\textwidth]{figs/lease/cgc2012_savings}
  \caption{Average savings of S2 scheduler compared with first fit scheduler. VM/PM ratio differs between sub-plots. Results for log-normal runtime distribution are similar and omitted for brevity.}
  \label{fig:lease:savings}
\end{figure*}

Through simulations across a diverse parameter space,
we established that reductions in physical machine uptime of up to 51.5\% are possible.
Figure~\ref{fig:lease:savings} shows the average savings while varying three key workload parameters:
(1)~mean reservation time, represented by three different colors,
(2)~server capacity increases from left to right between the sub-plots as stated in each sub-plot's title,
(3)~data center size, as indicated by the x-axis labels.
The y-axis shows the savings achieved by our reservation length aware scheduler over the baseline first fit scheduler,
where each bar represents the average savings over 1000 simulation runs.

Savings here indicate the reduction in \emph{cumulative machine uptime}~(CMU) which represent the total machine uptime for a set of servers.
If one server is powered on for 24 hours,
the CMU is 24; if two servers are active for 24 hours the CMU is 48.
Further, if our custom scheduler achieves savings of 10\% compared to first fit,
it means the CMU was reduced by 10\%.

As Figure~\ref{fig:lease:savings} shows,
the savings vary with the simulation parameters.
At the lower end, savings barely accumulate 5\%,
while in the best case scenarios, savings double to slightly more than 10\%.
A general trend exists with savings growing as server capacity grows, i.e.,
savings increase across the board moving from left to right between sub-plots.
The data center size affects the savings differently depending on the average reservation length.
For average reservation lengths of 120 minutes,
savings increase with data center size;
for average reservation lengths of 240 minutes the data center size seems to have little effect.
For reservation lengths of 480 minutes,
the savings actually decrease with increasing data center size.
We conclude that whatever the distribution of reservation lengths turns out to be in real infrastructure clouds,
it will significantly impact the achievable savings.

As no data on the parameters used in our simulations is publicly available,
we can only speculate.
When such data does become accessible in the future,
it would be interesting to re-run the simulations.


\section{On-Demand Resource Provisioning}

\begin{figure}
\begin{center}
  \includegraphics{figs/dream/arch-single-column}
  \caption{DreamServer architecture.
Requests pass through a proxy (1) which is aware of the on/off state of backend servers.
If the backend server is up, the proxy forwards the request immediately (2).
Otherwise, the proxy resumes the backend (3) and waits until the server is up again before forwarding the request (2).
The entire process is transparent to the client.}
  \label{fig:dream:arch}
\end{center}
\end{figure}

Virtualization in cloud data centers is used to isolate customers from each other and to provide a self-contained execution environment.
Virtual machines can be suspended,
halting their execution and storing all their associated state on persistent storage.
Suspending an idle VM frees up resources to be used by other, active instances.
Further, it is as a precursor to powering off the physical server,
which is only possible if no VMs are actively running on the server.

Irrespective of the reason to suspend the VM,
the goal is to maintain its network presence and have it ready for processing when needed.
To this end, we proposed an overall architecture to monitor incoming requests,
as well as technology, to speed up resuming the virtual machine.
We presented two alternative architectures to detect idleness and activity at the network layer:
our first solution is based on an application layer HTTP proxy~\cite{knauth2014dreamserver},
while the second, more generic solution uses Software Defined Networking to monitor network traffic~\cite{knauth2014sloth}.

Figure~\ref{fig:dream:arch} shows a schematic of the proposed architecture.
In this case, a reverse proxy is used to gather information about the activity of backend VMs.
In a typical data center, multiple proxies can be used for scalability.
Each proxy is responsible for a fixed number of backend services.
The VMs run web services, i.e., communicate over HTTP with their clients.
Every interaction between clients and the backend VMs passes through the proxy.
We hook into the reverse proxy's functionality to forward requests based on the information contained within the HTTP request.
This allows the proxy to track for each VM when it has last seen a request.
The proxy periodically checks if the time since the last request is larger than some threshold and suspends idle VMs.

Conversely, if the proxy sees a request for a currently suspended VM,
it resumes the VM first, before forwarding the request.
To resume a VM, its associated state must be read from disk into memory.
Depending on the state size, it can take several seconds before the VM is ready to serve requests again.

\subsection*{Results}

\begin{figure*}
  \includegraphics[width=0.5\textwidth]{figs/dream/20140404T103906-resumetime-bar-ssd-direct}
  \includegraphics[width=0.5\textwidth]{figs/dream/20140404T103906-resumetime-bar-ssd-nbd}
  \caption{Resume times for seven applications from local (left) and remote (right) SSD.
Lower is better. Error bars show the standard deviation (one $\sigma$).}
  \label{fig:dream:resumetime-bar-ssd}
\end{figure*}

Besides verifying the basic functionality,
an important metric is the overall time it takes to answer the initial request.
We performed measurements with different hardware setups and various server applications.
For the hardware setups, we varied the storage medium to compare resume times from HDD and SSD.
Further, we also experimented with local and network storage solutions.

In our measurements, we compare three different resume implementations:
\emph{eager}, \emph{lazy}, and \emph{hybrid}.
The hybrid resume implementation is, as the name suggests, a combination of eager and lazy resume.
During an eager resume, the VM continuous execution only after \emph{all} of its state has been read from disk back into memory.
The resume time for an eager resume, hence, depends on the VM's state size.
On the other hand, a lazy resume execution continuous immediately.
If the VM accesses state not yet read from disk,
it is done so on-demand, potentially incurring many small interruptions, akin to page faults,
until a sufficient amount of state is present in memory again.
Hybrid resume combines both ideas:
reading a small portion of the overall state before resuming the execution again minimizes the number of interruptions.
The remainder of the state is read lazily and/or in the background while the VM is already executing again.

The numbers for a few of those experiments are plotted in Figure~\ref{fig:dream:resumetime-bar-ssd}.
While the left graph shows resume times from local SSD,
the right graph used SSDs in network storage configuration accessed over gigabit Ethernet.
For brevity,
we only discuss the results of the best performing implementation, namely \emph{hybrid}, here.
For a full discussion, consult the thesis' full version.
From local SSD, it is possible to answer the initial HTTP request with a delay of less than one second for some of the applications,
e.g., Django, Jenkins, and Rubis.
More complex applications, most notably the Mediawiki application,
require more than 2 seconds.
Resuming over the network slows down the initial response.
Best case response latencies are around the one second mark (Django),
with worst case values of slightly below 3 seconds (Mediawiki).

\section{Efficient Block-Level Synchronization of Binary Data}

When turning off physical servers to save energy,
access to the locally stored on-disk data is no longer possible.
For reasons of fault tolerance, important data is likely backed up periodically.
Hence, an older version of the same data already exists at a second location.
To synchronize the two copies efficiently,
the question arises how to identify the differences to minimize the overall volume of data to transfer.
Schemes that rely on checksum computations to identify matching blocks at the source and destination impose an overhead that is equal to the total data size.
If possible, we would like to avoid computing checksums by tracking modifications as they happen.
When it is time to synchronize,
the synchronization process is guided by the recorded modifications.

With our \texttt{dsync} system, we implemented this tracking functionality at the block device layer~\cite{knauth2013dsync}.
While some recent file systems, for example, ZFS,
posses similar functionality by means of differential snapshots,
implementing it at the block device layer makes this technique independent of the file system.
Regardless of whether a particular file system supports it out-of-the box,
\texttt{dsync} enables delta snapshots for a larger number of users.
Figure~\ref{fig:dsync:arch} shows where dsync\texttt{dsync} fits into the software stack when tracking a virtual machine disk.
The \emph{device mapper}, i.e., \texttt{dsync}), can be put directly on top of the block device, e.g., a physical disk partition.
Alternatively, a loopback device can function as a ``shim'' to enable modification tracking for any file.
A loopback configuration is, however, not advisable for production systems as it noticeably reduces the performance.

\begin{figure}
  \centering
  \includegraphics{figs/dsync/arch}
  \caption{Two configurations where the tracked block device is used by a virtual machine (VM). If the VM used a file of the host system as its backing store, the loopback device turns this file into a block device (right).}
  \label{fig:dsync:arch}
\end{figure}

We implemented the modification tracking functionality as a Linux kernel module.
The additional code is small, a few hundred lines of code,
and localized.
For comparison, we looked at four alternative methods to synchronize block-level data:
(1)~\texttt{rsync}, (2)~\texttt{zfs}, (3)~\texttt{copy}, and (4)~\texttt{blockmd5sync}.
The tools and methods present a mix of standard and widely available tools, e.g.,
\texttt{rsync} and \texttt{copy},
as well as more advanced technologies, e.g., \texttt{zfs},
which are not uniformly available, but offer much of the same functionality at a different layer of abstraction.
\texttt{blockmd5sync} is a computationally lighter alternative to \texttt{rsync}.
\texttt{blockmd5sync} computes block-wise checksums, hence, must read the entire input once,
but does not try to identify shifted content.
\texttt{rsync} is computationally complex because it computes and compares block-wise checksums for every single byte offset.
\texttt{blockmd5sync} simply calculates checksums for non-overlapping, adjacent blocks.
This may miss some opportunities to reduce the data volume,
but significantly reduces the CPU overhead.% and overall completion time.

\subsection*{Results}

Our empirical measurement with different hardware setups showed that \texttt{dsync} performs at least as good as its competitors in terms of wall-clock completion time.
We discuss the total synchronization time for different data sizes (x-axis) and different storage media here:
traditional spinning hard disks (Figure~\ref{fig:dsync:synctime-vs-size} left) and solid state disks (Figure~\ref{fig:dsync:synctime-vs-size} right).
We did not plot the values for \texttt{rsync},
because the measured synchronization times are several times higher than for the shown methods,
which would make the graph less readable.
The measurements are included in the thesis' full version though.

For data stored on HDD, \texttt{dsync} has a similar runtime compared to \texttt{copy} and \texttt{zfs}.
The third method, \texttt{blockmd5sync}, takes longer to complete and, at the same time,
must read through the entire data set to compute block-wise checksums.
Recall, that \texttt{dsync} tracks modifications and does \emph{not} require to read the entire input;
\texttt{dsync} already knows the differences.
While \texttt{copy} has completion times comparable with \texttt{dsync},
it must also read and transmit the entire data set,
which wastes local disk and network I/O resources.

With data stored on SSD, the difference in completion times between the methods is more pronounced.
\texttt{copy}, which undiscerningly reads and transmits all the data,
takes longest to complete.
Next is \texttt{zfs} with its built-in delta snapshot functionality.
Last but not least, \texttt{blockmd5sync} is slightly faster than \texttt{dsync},
but has a higher disk I/O overhead, which is not displayed in this graph.

In summary, \texttt{dsync} enables efficient delta synchronization at the block device layer by tracking modified blocks.
Compared to its alternatives, \texttt{dsync} is at least as fast in terms of raw synchronization time,
while using less disk and network I/O than the other methods except for \texttt{zfs}.
\texttt{dsync}'s advantage over \texttt{zfs} is its wider availability,
as \texttt{dsync} operates at the block device layer.

\begin{figure*}[t]
  \includegraphics[width=0.5\textwidth]{figs/dsync/rsync-1373558022-1375372816-synctime-vs-size-hdd-without-rsync}
  \includegraphics[width=0.5\textwidth]{figs/dsync/rsync-1373558022-1375372816-synctime-vs-size-ssd-without-rsync}
  \caption{Synchronization time for four different synchronization techniques.
    Lower is better. Data on the source and target was stored on HDD (left) and SSD (right).}
  \label{fig:dsync:synctime-vs-size}
\end{figure*}

\section{Conclusion}

The research question we set out to answer was \emph{if and how hardware virtualization can help to increase the energy efficiency in cloud data centers}.
We began by looking into the usage of our research group's own compute infrastructure.
Four years of usage data revealed an average usage of only 15.09\%,
while the machines were always on.
The usage data from our own cluster confirms other observations regarding the low average utilization of IT resources.

Transforming our cluster into a ``cloud environment'' might make it easier to better manage the server's power,
which brings us to our second contribution:
scheduling of virtual machines within infrastructure clouds.
We presented theoretical considerations backed up by simulations regarding the utility of known reservation times in infrastructure clouds.
We evaluated the degree to which the number of powered up servers is reduced due to a priori known reservation times.
Compared to a first fit scheduler,
the cumulative machine uptime can be reduced by as much as 21.4\% in favorable settings,
although the average reduction is much lower at 4.9\%.

Third, we proposed to explicitly suspend idle virtual machines in an infrastructure cloud.
Quickly suspending and resuming of entire (virtual) servers is made possible by hardware virtualization.
We argue that by suspending idle VMs,
fewer physical servers are needed in total to host the same number of active VMs.
Fewer physical servers means less energy spent to power those servers,
hence improving the infrastructure cloud's energy efficiency.
We implemented a prototype, complete with a fast VM resume and a network-based idleness detector.
Through empirical measurements we demonstrated that possibility to resume a VM from local storage in less than one second (best case).

Our last contribution does not directly target energy efficiency,
but touches a related problem:
when powering off servers the data stored on those servers becomes unavailable.
To allow for the efficient state synchronization at the block-level,
we designed a novel tool for fast, efficient, and periodic data synchronization.
One use case is the synchronization of virtual machine disks before a server within an infrastructure cloud is powered down.

For the future, we have many ideas how to extend our work.
For example, within the context of scheduling in infrastructure clouds,
we would like to explore the relationship between online scheduling and offline planning.
Further, we would like to extend our DreamServer system to mange physical servers in addition to virtual machines.
This can then be applied to our 50-node research cluster,
to power down physical machines when they are not used.
Last but not least, investigating alternative data structures for \texttt{dsync} may reduce the memory overhead of the tracking information.

As for the future of energy efficient computing,
we are convinced that giving up on small-scale on-premise data centers and moving them ``into the cloud'' is the way forward.
Economies of scale enjoyed by large data centers at many levels, measurably improve the overall energy efficiency.
In addition, energy efficiency gains achieved due to consolidation can be complimented by software-based approaches such as, for example,
those presented this thesis.

%% We see three main avenues for future work in the areas touched in this thesis:
%% First, exploring the relationship and tradeoff between scheduling and planning for infrastructure clouds seems like an interesting extension.
%% While scheduling implies instantaneous decision making, i.e.,
%% deciding on the spot where to schedule a virtual machine,
%% planning is more focused on the long term.
%% Instead of handling each request instantaneously as it arrives,
%% the system collects requests for a certain time period, say one minute.
%% The scheduler then runs once per minute,
%% handling all the accumulated requests together,
%% exploiting knowledge about other, simultaneously arriving requests.
%% The system would tradeoff the time period over which to collect requests,
%% with the responsiveness of the system.
%% Collecting requests over longer time periods potentially yields better placements,
%% but at the same time delays when users can access their resources.

%% Second, in the area of on-demand resource provisioning we want to expand from only managing virtual resources to manage the power of physical machines.
%% In the beginning we presented a usage analysis of a 50 node resource cluster.
%% As a next step, we want to perform a more detailed network traffic analysis.
%% A deep understanding of the applications using the network is essential to estimate the potential for powering down servers.
%% Even a seemingly idle machine receives multiple packets per second.
%% Discerning between essential and non-essential packets is important to power off servers for reasonable time periods.

%% Third, for \texttt{dsync} it would be interesting so see if and how Bloom filters affect the memory overhead.
%% Currently, \texttt{dsync} uses one bit per $4\,\textrm{KiB}$ block arranged in a simple bit vector.
%% For large storage arrays, the memory overhead may become a problem,
%% which warrants a look at alternative data structures.
%% On the practical side, getting the source code accepted into the mainline Linux kernel is also on our agenda.

\pagebreak

\bibliographystyle{plainnat}
\bibliography{refs}

\end{document}
