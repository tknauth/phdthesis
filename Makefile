FIGS=$(wildcard figs/*.pdf)
TEXS=$(wildcard *.tex)

MAIN=thesis

paper.pdf : $(TEXS) $(FIGS) refs.bib
	pdflatex $(MAIN).tex
	bibtex $(MAIN)
	pdflatex $(MAIN).tex
	pdflatex $(MAIN).tex

clean :
	rubber -f -d --clean $(MAIN).tex
