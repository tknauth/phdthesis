\chapter{Efficient Block-Level Synchronization of Binary Data}
\label{chapter:dsync}

The material in this chapter was originally published at LISA'13~\cite{knauth2013dsync}.

\section{Introduction}

\newcommand{\dsync}{dsync}

\newthought{The efficient periodic transfer} of large binary data sets is the topic of this chapter.
In the context of virtualized data centers, large binary data predominantly arise in two forms:
virtual machine checkpoints and virtual machine disks.
Checkpoints are the result of suspending VMs, i.e., writing their in-memory state to disk,
while VM disks are just like normal physical hard disks.
They allow the VM to store information persistently.

From time to time the data must be copied between two locations, for example,
backups for disaster recovery or migration of virtual machines.
While in the former case the source and destination will sit in different data centers,
the latter may only involve servers in the same rack or data center.
Indiscriminately copying all the data is one option to synchronize the two replicas.
However, because we perform the synchronization periodically, e.g., once per day,
only a fraction of the data will have changed since the last synchronization.

A plain copy might be acceptable if the data sets are small,
but today even a single consumer hard disk stores up to $4\,\textrm{TB}$ of data.
Just sequentially reading through the content of a single disk,
takes more than six hours~\footnote{Assuming an ideal sequential read throughput of $170\,\textrm{MB/s}$,
which is only possible at the outer tracks of a disk due to the constant angular velocity. $(4*10^{12}\,\textrm{bytes})/(170\,\textrm{MiB/s}) = 6.23\,\textrm{hours}$}.
The trend of ever more digitized information, whether in personal or business related contexts,
is expected to continue if not accelerate even more.
Hence the importance of reducing the overall volume of data which must be moved during synchronization.

Because only a subset of the data changed between to subsequent synchronizations,
the copied data volume can be reduced by only transferring the differences.
The question then becomes, how to identify the differences.
Common tools, like the command line program \emph{rsync},
compute checksums to determine the changed parts.
Checksums are used to detect changes \emph{after the fact} and the whole data set must be read to compute the checksums.
If, instead, it were possible to track modifications as they happen,
checksums would no longer be required.

Unfortunately, modern Linux systems have no support to record modifications at the block device layer.
The remainder of this chapter presents a system called \emph{dsync}.
It combines a Linux kernel extension with complementary userspace tools for the efficient, periodic synchronization of binary block-level data.
The following sections will expand and clarify the problem definition,
present the design and implementation of our solution,
provide experimental validation as to the benefits of \emph{dsync},
before concluding with related work.

\section{Problem and Motivation} \label{sec:dsync:problem}

The task at hand is to periodically synchronize two data sets and to do so efficiently.
The qualifier ``periodically'' is important because there is little to optimize for a one-off synchronization,
except, possibly, compressing the data.
With periodic synchronizations, on the other hand,
we can exploit the typical case where the majority of the data is unchanged between two successive synchronizations.

In the personal computing sphere there exist many solutions to perform backups of personal data.
For example, Dropbox~\footnote{\url{http://www.dropbox.com}}, a recent, popular service to replicate directory structures between devices,
is one such available solution.
Dropbox hooks into the operating system to receive notifications of file-system related events.
Using the OS-provided hooks, any application can get notified of files being created, read, updated or deleted.
On Linux, the subsystem responsible for registering changes and notifying applications is called \emph{inotify}.
Similar functionality exists for other operating systems too~\footnote{On OSX the FSEvents API allows to watch directories for changes.}.
However, even using the file-system level notification services provided by modern operating systems,
the application must still determine exactly which parts of modified file have changed.

Change detection typically involves the computation of checksums for a given input.
For example, the modified file is split into non-overlapping $4\,\textrm{KiB}$ blocks and a checksum computed for each block.
By comparing the checksums of each block between the file's old and new version,
we can determine the modified blocks.
Merkle trees are a generalization of the same concept,
which combines the per-block checksums into a hierarchical \emph{hash tree}.

Even if modifications of individual files can be detected,
computing checksums for multi-gigabyte files is costly and takes time.
To compute the checksums, the entire file must be read from disk.
This potentially destroys the system's file cache, consumes disk I/O bandwidth, and CPU cycles.
While daemon-like applications, such as Dropbox, can rely on storing checksums locally between sync operations,
on-demand tools, such as \emph{rsync},
must compute the checksums at the source and destination on each invocation.

The task of monitoring individual files for changes can be generalized to monitoring directories, directory trees or even entire file systems.
Because a file system is an abstraction built on top of block devices,
we decided to monitor changes at this most generic of interfaces -- the block device level.
By modifying a subsystem of the Linux kernel,
we are able to track modifications for arbitrary block devices.
The tracked modifications, essentially a list of block numbers, is exported to userspace.
Two complementary tools, called \emph{dmextract} and \emph{dmmerge},
use the tracking information to extract and merge modified blocks from the source and destination block devices.

%% file system alternatives

\section{Design and Implementation}

{\dsync} relies on the ability to monitor changes at the block device level.
A block device provides an interface to read and write data in fixed-size \emph{sectors},
where each sector is identified by a logical block address~(LBA).
Typical sectors sizes are 512 bytes,
although recently hard disk manufacturers are moving to $4\,\textrm{KiB}$ sectors for drives larger than $2\,\textrm{TB}$.
This is because at 512 byte, a single 32 bit integer can only address sectors up to $2.2\,\textrm{TiB}$.
It is up to the drive's internals how a logical block address is mapped to a physical location on the disk.
For example, sectors can become faulty and maybe remapped.
Also, solid state drives~(SSDs) have a much more sophisticated logic to cope with the characteristics of the underlying flash memory~\cite{cornwell2012anatomy, leventhal2013file}.

While sectors are the smallest addressable units with respect to the underlying hardware,
the Linux kernel's smallest unit when dealing with block devices is a \emph{block}.
The size of a block is typically identical to that of a \emph{page}, for example, $4\,\textrm{KiB}$.
Consequently, a single block consists of multiple sectors.
For the purpose of persistently storing information,
block devices referring to hard disks are partitioned and formatted with a file system.

\subsection{Device Mapper}

The Linux kernel comes with a subsystem to create arbitrary new virtual block devices from existing physical block devices.
The \emph{device mapper}, allows for example, to combine multiple physical devices into a single logical device.
Besides aggregation, the device mapper also supports various RAID configurations.
Another feature, which only partially solves our problem, are snapshots.
All writes to a snapshotted device are redirected to a special copy-on-write device.
This leaves the original data unmodified and can be used, for example, to create ephemeral virtual machine disks.
A snapshot block device is created based on a VM disk template and used as a disk by the VM.
The VM can modify the disk, i.e., to write log files, which are redirected to the copy-on-write device.
After the VM terminated, the copy-on-write device, containing all the temporary modifications, is discarded, while the original disk image is unmodified.

However, device mapper snapshots are inadequate to track changes for three reasons:
first, they require temporary storage to hold the modified blocks.
The required storage grows linearly with the size of modified data.
Second, the current implementation uses a static space allocation for redirected writes.
If the capacity limit of the copy-on-write device is reached,
future writes will fail.
Third, while the snapshot device will contain the modified blocks,
they must be merged into the underlying, tracked device eventually,
in addition to merging the changes with the remote copy.

In its current form, the device mapper does not support change tracking for block devices,
but it is a convenient place to start implementing the sought functionality.

\subsection{Device Mapper Internals}

The device mapper's functionality is split into separate \emph{targets}.
For example, there exists one target for each supported RAID level 0, 1, and 5.
Each target implements a predefined interface laid out in the \verb+target_type+~\footnote{\url{http://lxr.linux.no/linux+v3.6.2/include/linux/device-mapper.h\#L130}} structure.
The structure contains a set of 17 function pointers for operations related to the specific target.
The target-independent part of the device mapper calls the target-dependent code via the function pointers stored in this structure.

The most important functions for our purposes are: (1)~the constructor (\verb+ctr+),
(2)~the destructor (\verb+dtr+), and (3)~the mapping (\verb+map+) function.
The constructor is called whenever a device of a particular target type is created.
Conversely, the destructor cleans up when a device is dismantled.
The mapping function translates the accessed location of the mapped device to a location for one of the underlying (physical) devices.
We will see shortly how to hook into the map function to track modifications.

Use of the device mapper, for example, by a system administrator,
is facilitated by a userspace program called \verb+dmsetup+.
Given the right parameters, \verb+dmsetup+ will, through a series of \verb+ioctl()+ system calls,
instruct the kernel to setup, tear down, or configure a mapped device.
For example, 

\begin{verbatim}
# echo 0 1048576 linear /dev/original 0 | \
  dmsetup create mydev
\end{verbatim}

\noindent%
creates a new device called mydev.
Access to the sectors 0 through 1048576 of the mydev device are mapped to the same sectors of the underlying device \verb+/dev/original+.
The previously mentioned function, \verb+map+,
is invoked for every access to the linearly mapped device.
It applies the specified offset, 0 in our example,
to arrive at the access location of the underlying device.
Because the offset is zero,
the mapping function is effectively an identity function.
The example restricts accesses to the underlying device to the first $2^{20}$ sectors, i.e.,
creating a device smaller than the original.

The device mapper has convenient access to all the information we need to track block modifications and it interfaces with the block device layer; exactly what we are interested in.
Every access to a mapped device passes through the \verb+map+ function.
Out of the available device mapper targets, we modified the behavior of the \emph{linear} target.

\subsection{Use Case: Track Modifications of Virtual Machine Disk}

We continue by highlighting one possible use case of the tracking functionality.
The goal is to monitor a virtual hard disk for modifications,
such that after the VM finished executing,
it is possible to extract, transfer, and merge the delta with a second copy of the VM's disk.
Figure~\ref{fig:dsync:arch} shows a conceptual view of the layered architecture.
The lowest layer is a physical block device, for example, a hard disk.
Using the device mapper, a tracking device is created on top of the physical block device,
which is, in turn, used by the virtual machine as its backing store.

\begin{figure}
  \includegraphics{figs/dsync/arch}
  \caption[Block device configurations]{Two configurations where the tracked block device is used by a virtual machine (VM). If the VM used a file of the host system as its backing store, the loopback device turns this file into a block device (right).}
  \label{fig:dsync:arch}
\end{figure}

It is also possible for the VM to use a file in the host's file system as its virtual disk.
In these cases, a loopback device first ``converts'' the file into a block device.
Instead of installing the tracking device on top of a physical device,
we track modifications of the loopback device;
pictured on the right in Figure~\ref{fig:dsync:arch}.
The VM uses the tracked loopback device as its backing store.

To summarize, the tracking device enables to track arbitrary block devices,
from hard disks to loopbacked files.
In particular, by combining the loopback and tracking device it is possible to monitor arbitrary files for changes.
The tracking functionality itself is implemented entirely in the host system's kernel.
In our example, the guests VMs are unaware of the tracking and do not require any modifications.

\subsection{Implementation}

Storing the modification status for a block requires exactly one bit:
%if the bit is set, the block is modified. Otherwise, not.
%if the bit is set, the block has been modified, otherwise the bit remains. Otherwise, not.
modified blocks have their bit set, unmodified blocks not.
The status bits of all blocks form a bit vector indexed by the block number.
%However, 
Given the size of today's hard disks and the option to attach multiple disks to a single machine,
the bit vector may occupy multiple megabytes of memory.
With $4\,\textrm{KiB}$ blocks, for example,
a bit vector of $128\,\textrm{MiB}$ is required to track the per-block modifications of a $4\,\textrm{TiB}$ disk.
An overview of the relationship between disk and bit vector size is provided in Table~\ref{tab:dsync:bit-vector-size}.

The total size of the data structure is not the only concern when
allocating memory inside the kernel;
the size of a single allocation is also constrained.
The kernel offers three different mechanisms to allocate memory:
(1)~\verb+kmalloc()+, (2)~\verb+__get_free_pages()+, and (3)~\verb+vmalloc()+.
However, only \verb+vmalloc()+ allows us to reliably allocate multiple megabytes of memory with a single invocation.
The various ways of allocating Linux kernel memory are detailed in ``Linux Device Drivers''~\cite{ldd3rd}.

\begin{table*}[t]
  \begin{center}
    \begin{tabular}{r@{ }|l||l|l|l|r@{ }}
      Disk size & Disk size  & Bit vector size & Bit vector size & Bit vector size & Bit vector size \\
                & (bytes) & (bits)        & (bytes)      & (pages)      & \\
      \hline
  4 KiB & $2^{12}$ & $2^{0}$ & $2^0$ & $2^{0}$        &    1 bit\\
128 MiB & $2^{27}$ & $2^{15}$ & $2^{12}$ & $2^{0}$    &    4 KiB\\
  1 GiB   & $2^{30}$ & $2^{18}$ & $2^{15}$ & $2^{3}$  &   64 KiB\\
%  16 GiB & $2^{34}$ & $2^{22}$ & $2^{19}$ & $2^{7}$    &  512 KiB\\
% 128 GiB & $2^{37}$ & $2^{25}$ & $2^{22}$ & $2^{10}$   &    4 MiB\\
% 256 GiB & $2^{38}$ & $2^{26}$ & $2^{23}$ & $2^{11}$   &    8 MiB\\
512 GiB & $2^{39}$ & $2^{27}$ & $2^{24}$ & $2^{12}$   &   16 MiB\\
  1 TiB & $2^{40}$ & $2^{28}$ & $2^{25}$ & $2^{13}$   &   32 MiB\\
  4 TiB & $2^{42}$ & $2^{30}$ & $2^{27}$ & $2^{15}$   &  128 MiB\\
    \end{tabular}
  \end{center}
  \caption[Memory overhead vs. storage capacity][0.5cm]{Relationship between data size and bit vector size. The accounting granularity is $4\,\textrm{KiB}$, i.e., a single block or page.}
  \label{tab:dsync:bit-vector-size}
\end{table*}

%% The methods differ in three important properties:
%% \begin{itemize}
%% \item Is memory physically contiguous?
%% \item What is the maximum possible size of a single allocation?
%% \item What is the allocation cost? How is the allocation implemented internally?
%% \end{itemize}

%% \begin{table}
%%   \begin{center}
%%     \begin{tabular}{l|*{5}{l}}
%%       pages & $2^{0}$ & $2^{1}$ & $2^{2}$ & \ldots & $2^{10}$ \\
%%       \hline
%%             & 379     & 0 & 0 & \ldots & 0\\
%%     \end{tabular}
%%   \end{center}
%%   \caption{Availability of different allocation sizes.}
%%   \label{tab:buddyinfo}
%% \end{table}

%% \verb+kmalloc()+ returns physically contiguous memory but typically only supports allocations of up to 128~KB.
%% Larger allocations may be obtained using \verb+__get_free_pages()+.
%% However, availability of allocations larger than 128~KB is not guaranteed.
%% On a running Linux system, the availability of differently sized allocation units can be inspected by reading from \verb+/proc/buddyinfo+.
%% In theory, \verb+__get_free_pages()+ can allocate up to $2^{11}$ pages with a single invocation.
%% $2^{11}$ pages with 4~KB per page equates to 8~MB ($2^{12} * 2^{11} = 2^{23}$).
%% This would be sufficient to track a single 256~GB disk.
%% However, on systems running a production workload the limits will be much lower.
%% For example, the availability of different allocation sizes on the author's desktop is summarized in Table~\ref{tab:buddyinfo}.
%% The maximum size of a single allocation is, in fact, at the exact opposite of the spectrum:
%% \emph{a single page}.
%% The last candidate is \verb+vmalloc()+.
%% Although \verb+vmalloc()+ is more expensive in terms of work necessary for an allocation,
%% the main benefit is its ability to work around the lack of contiguous physical memory.
%% \verb+vmalloc()+ internally uses \verb+kmalloc()+ in combination with page table manipulations to construct a mapping that is \emph{contiguous in virtual memory}.
%% Allocations of multiple megabytes are thus possible with \verb+vmalloc()+.
%% The book Linux Device Drivers~\cite{ldd3rd} provides further details about the Linux kernel memory management.

Total memory consumption of the tracking data structures may still be a concern:
even commodity (consumer) machines commonly provide up to 5 SATA ports for attaching disks.
Hard disk sizes of $4\,\textrm{TB}$ are standard these days too.
%The block-wise dirty status for a 10~TB setup like this requires 320~MB of memory.
To put this in context, 
the block-wise dirty status for a $10\,\textrm{TiB}$ setup requires $320\,\textrm{MiB}$ of memory.
We see two immediate ways to reduce the memory overhead:
\begin{enumerate}
\item Increase the minimum unit size from a single block to 2, 4, or even more blocks.
%% Do this dynamically?  Make it a tuneable?
\item Replace the bit vector by a different data structure, e.g., a bloom filter.
\end{enumerate}
\noindent A bloom filter could be configured to work with a fraction of the bit vector's size.
The trade-off is potential false positives and a higher (though constant) computational overhead when querying/changing the dirty status.
We leave the evaluation of tradeoffs introduced by bloom filters for future work.

Our prototype currently does not persist the modification status across reboots.
Also, the in-memory state is lost, if the server suddenly loses power.
One possible solution is to persist the state as part of the server's regular shutdown routine.
During startup, the system initializes the tracking bit vector with the state written at shutdown.
%% MIKEC:  This is somewhat similar to what happens with battery-backed write cache on hardware RAID controllers
If the initialization state is corrupt or not existing,
each block is marked ``dirty'' to force a full synchronization.
%% MIKEC:  Observation: In theory, at least, you could always layer a per-file dirty flag and fall back to that i.e. a hybrid system

%% If total emory usage is of concern, a different technique for tracking the status of each block can be used.
%% The bit vector offers constant time set and query operations, while the bit vector's size grows linearly with the size of the disk.
%% Other data structures, e.g., linked lists or b-trees, offer different trade-offs.

\subsection{User-space Interface}

The kernel extensions export the relevant information to user space.
For each device registered with our customized device mapper,
there is a corresponding file in \verb+/proc+, e.g.,
\verb+/proc/mydev+.
Reading the file gives a human-readable list of block numbers which have been written.
Writing to the file resets the information, i.e.,
it clears the underlying bit vector.
%%% Ummm..... isn't this going to be dangerous?  i.e. testing use only for prototype :-)
The \verb+/proc+ file system integration uses the \verb+seq_file+ interface~\cite{salzman2001linux}.

Extracting the modified blocks from a block device is aided by a command line tool called \verb+dmextract+.
The \verb+dmextract+ tool takes as its only parameter the name of the device on which to operate, e.g.,
\verb+# dmextract mydevice+.
By convention, the block numbers for \verb+mydevice+ are read from \verb+/proc/mydevice+ and the block device is found at \verb+/dev/mapper/mydevice+.
The tool outputs, via standard out,
a sequence of $(block number, data)$ pairs.
Output can be redirected to a file, for later access,
or directly streamed over the network to the backup location.
The complementing tool for block integration, \verb+dmmerge+,
%It reads, from standard input,
reads 
a stream of information as produced by \verb+dmextract+
from standard input.
A single parameter points to the block device into which the changed blocks shall be integrated.

Following the Unix philosophy of chaining together multiple programs that each serve a single purpose well,
a command line to perform a remote backup may look like the following:
\begin{verbatim}
# dmextract mydev | \
  ssh remotehost dmmerge /dev/mapper/mydev
\end{verbatim}
%% If this is breaking over columns/pages, look at the fancyvrb package 
%% 	For preamble:
%%		\usepacakge{fancyvrb}
%% then
%% 	\begin{Verbatim}[samepage=true]
%% 	# dmextract mydev | \
%%	   ssh remotehost dmmerge /dev/mapper/mydev
%% 	\end{Verbatim}

This extracts the modifications from \verb+mydev+ on the local host,
sends the information over a secure channel to a remote host,
and merges the modifications into an identically named device on the remote host.

\input{ch_dsync_eval.tex}
\input{ch_dsync_rwork.tex}

\section{Conclusion}

Recall that the overall goal is to power off unused servers to conserve energy.
This chapter dealt with the ``local state problem'', i.e.,
data residing on locally attached storage becomes inaccessible when the server goes offline.
One solution is to keep multiple copies of the data,
but this required periodically synchronizing the copies.
With {\dsync} we developed a low-level tool to efficiently synchronize data at the block device layer.
{\dsync} reduces the overall synchronization time, does not require expensive checksum calculations,
and minimizes disk and network I/O.
Hence, {\dsync} is a novel addition with a unique set of properties to the existing set of bulk data synchronization tools.
