\section{Discussion}

The evaluation determined how many machine hours are saved when the scheduler knows the instance runtimes.
An open question still is how the reduction in cumulative machine uptime translates into actual power savings.
We will give an analytical answer to this here.
The power consumption of a server is typically modeled as a linear function in relationship to the CPU utilization.
When the system is idle, i.e., at 0\% utilization,
it has a baseline power consumption of $P_{base}\,\textrm{Watts}$.
At the other end of the spectrum, at 100\%, the system consumes maximum or peak power, denoted as $P_{peak}$.
The difference between $P_{base}$ and $P_{peak}$ is the system's dynamic range.
Depending on the machine's concrete configuration, i.e.,
number of hard disks, network cards, CPUs, and amount of memory,
the baseline power consumption can be up to 60\% of peak for older servers~\cite{barroso2009datacenter}.
However, we expect modern servers to have a larger dynamic range where $P_{base}$ is only 20-30\% of $P_{peak}$.
To keep the analysis generic, we define a peak-to-idle ratio $r_{p2i}$,
such that

$$P_{peak} = r_{p2i} * P_{base}$$

\noindent
Given a cumulative machine uptime~(CMU) without and with our optimizing scheduler, named $CMU_{before}$ and $CMU_{after}$,
we can now define an upper and lower bound on the power savings.
The power consumed before optimizing the assignment is defined as

$$P_{before} = CMU_{after} * P_{peak} + (CMU_{before}-CMU_{after}) * P_{base}$$

\noindent
while the power consumption after optimizing the assignment is

$$P_{after} = CMU_{after} * P_{peak}$$

Conceptually, the lower bound is based on the assumption that during the reduced uptime, i.e.,
the difference between $CMU_{before}$ and $CMU_{after}$,
the machines only consumed minimal baseline power,
while they consumed peak power during the remaining time.
This is the most pessimistic view possible.

\newpage

To calculate the power savings, we relate the power consumptions before and after optimization

$$savings_{lower\ bound} = \frac{P_{before}-P_{after}}{P_{before}} = \frac{\Delta CMU}{r_{p2i} * CMU_{after} + \Delta CMU}$$

For the upper bound savings,
we assume that the machines only consume baseline power without optimizing their assignment

$$P_{before} = CMU_{before} * P_{base}$$

After simplifying the equation,
the resulting expression for the upper bound on relative power savings is

$$savings_{upper\ bound}=\frac{P_{before}-P_{after}}{P_{before}} = \frac{\Delta CMU}{CMU_{before}}$$

We can now apply the formulas to determine an upper and lower bound on the power savings based on the CMU reduction achieved through clever scheduling.
We distinguish two scenarios,
acknowledging advances in reducing the baseline power consumption of modern servers.
For old servers, with a high baseline power consumption,
we assume a peak-to-idle ratio of 2,
while we assume a peak-to-idle ratio of 5 for more modern servers.
That is, baseline power consumption is only $1/5$ of the peak power for modern servers.
The only other value required, according to the formulas we just derived, is the CMU reduction.
As an example, we assume a CMU reduction of 20\%.
Based on this, $CMU_{before}$, $CMU_{after}$, and $\Delta CMU$ can be derived:
$CMU_{before} = 1$, $CMU_{after} = 0.8$, $\Delta CMU = 0.2$.
Plugging those values into the formulas for lower and upper bound power savings gives

$$savings_{lower\ bound} = \frac{\Delta CMU}{r_{p2i} * CMU_{after} + \Delta CMU} = \frac{0.2}{2 * 0.8 + 0.2} = 0.11$$

$$savings_{upper\ bound} = \frac{\Delta CMU}{CMU_{before}} = \frac{0.2}{1} = 0.2$$

\noindent
That is, a 20\% CMU reduction reduces the server power consumption by 11-20\%.
For modern servers, with a larger peak-to-idle ratio,
reducing the CMU by 20\% only yields power savings of 5\%:

$$savings_{lower\ bound} = \frac{\Delta CMU}{r_{p2i} * CMU_{after} + \Delta CMU} = \frac{0.2}{5 * 0.8 + 0.2} = 0.05$$

\pagebreak

Based on the formulas, we can plot how a CMU reduction translates into power savings across the entire spectrum.
Figure~\ref{fig:lease:cmu-savings-vs-power-savings} shows the upper and lower bound for two server generations.
We observe that the power savings' upper bound is identical to the CMU savings,
while the lower bounds are convex, non-linear functions.
Based on the data from recently published studies on cloud data centers~\cite[-2.8cm]{birke2012datacenters, birke2012data} which confirmed the low average server utilization, 
we expect actual savings due to our proposed optimized scheduling technique to be closer to the upper than the lower bound.

\section{Conclusion}
\label{sec:lease:conclusion}

We presented our idea of \emph{timed instances},
a new class of virtual machine instances.
Besides established VM configuration parameters, for example, operating system, memory and CPU size,
the user also specifies the VM's runtime when requesting an instance.
The infrastructure provider incorporates the VM's runtime into the scheduling algorithm.

As our primary concern is energy consumption,
we evaluated three different schedulers with respect to their ability to reduce the overall number of used servers for a given workload.
The schedulers reduce the cumulative machine uptime by up to 60.1\% and 16.7\% when compared against a round robin and first fit scheduler, respectively.
Apart from these best-case scenarios,
typical savings are only about $1/3$ of the maximum.

\begin{figure}[t]
  \includegraphics{figs/lease/cmu-savings-vs-power-savings}
  \caption[Upper and lower bound on power savings]{Upper and lower bound on power savings based on CMU reductions.}
  \label{fig:lease:cmu-savings-vs-power-savings}
\end{figure}

As we lacked real-world input data to drive our simulations,
we developed a workload model and generator.
The parameters covered by the workload model are data center size and server mixes,
various VM sizes and mixes,
different runtime distributions,
batch requests,
and mixed purchase types.
%% We can conclude that only an increase in the run time distribution's shape parameter lead to an increase in savings.
%% Savings decreased due to host and instance heterogeneity.
%% Batch requests and mixed purchase types also had a negative impact.
%% We conclude that an optimal environment yielding maximal savings consists of homogeneous instance sizes and hosts,
%% has no batch instances, and consists solely of timed instances.
Even though CMU savings decrease as the variability of our model parameters increases,
the optimizations still yield consistent improvements.
How this translates into monetary savings depends on factors such as energy price,
data center size, and power usage effectiveness (PUE).
The monetary savings, in turn, determine the price difference between on-demand and timed instances.
%% Timed instances are an attractive alternative to on-demand and spot instances.
%% Cloud providers should consider adding timed instances as an attractive alternative to their offerings.

A back-of-the-envelope calculation shows that even seemingly small savings of a few percent can translate into cost reductions of multiple hundred thousand dollars:
according to the U.S. Environmental Protection Agency~\cite{epa-datacenter-report},
servers in mid-sized data centers in the US alone consumed a total of 3.7 billion ($10^{12}$) kWh.
Reducing this energy consumption by 5\% already saves thousands of millions of dollars~\footnote{At \$0.08/kWh 5\% of $3.7*10^{12}*0.08$ is \$14,800,000,000} let alone carbon emissions.
Of course, not every data center is an infrastructure cloud,
but even for a single site, the potential savings are on the order of \$100,000 per year.
%% Assuming a 20 MW data center, where servers consume 60\% of the energy,
%% reducing the server up time and thus energy consumption by 5\% saves \$420,000 annually
%% ~\footnote{\$420,480 = 24 * 365 * 20 MW * 0.6 * 0.05 * \$0.08/kWh}.

%% >>> 3.7*(10**12)*0.05*0.08
%% 14800000000.0
%% >>> 24*365*20*1000*0.6*0.05*0.08
%% 420480.0

%% \section{Conclusion -- CGC'12}
%% \label{sec:conclusion}

%% Using simulation, we showed that virtual machine lease time information helps to optimize the virtual-to-physical machine mapping.
%% The cumulative machine up time was reduced by 28.4\% to 51.5\% (w.r.t. round robin) and 3.3\% to 16.7\% (w.r.t. first fit).
%% The real-world Google cluster trace revealed possible reductions of 36.7\% and 9.9\%.
%% How this translates into monetary savings depends on factors such as energy price, data center size, and power usage effectiveness (PUE).
%% The monetary savings, in turn, determine the price difference between on-demand and timed instances.
%% A mere 3.3\% reduction in price is unlikely to persuade many customers to start using timed instances;
%% a reduction of 16.7\% may be already worthwhile.
