import math
import matplotlib
import matplotlib.mlab as mlab
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import os.path

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("font", weight="medium")
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 1.9
height = width/1.3

pylab.rc("figure.subplot", left=(30/72.27)/width)
pylab.rc("figure.subplot", right=(width-2/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-5/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

fig = plt.figure(figsize=(width, height))
ax1 = fig.add_subplot(111)

plt.bar([0, 1], [96.9, 44.2], 0.5, align='center', color=['#377eb8'], linewidth=0.5)

plt.annotate('96.9', (-0.12, 100))
plt.annotate('44.2', (0.88,  48))

# turn off ticks where there is no spine
ax1.xaxis.set_ticks_position('bottom')
ax1.yaxis.set_ticks_position('left')
ax1.set_xticks([0,1])
locs, labels = plt.xticks()
plt.xticks(locs, ['EC2', 'Rackspace'])
# ax1.set_yticks([-100,0,100,200,300])
ax1.set_ylim(0, 110)
ax1.set_xlim(-0.5, 1.5)
# ax1.set_yscale('log')

plt.xlabel('Provider')
plt.ylabel('Startup Time [sec]')

# http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
for loc, spine in ax1.spines.iteritems():
    if loc in ['left','bottom']:
        pass
    elif loc in ['right','top']:
        spine.set_color('none') # don't draw spine
    else:
        raise ValueError('unknown spine location: %s'%loc)

# plt.legend(loc=1, numpoints=1, ncol=1)
# leg = plt.gca().get_legend()
# leg.draw_frame(False)
# ltext = leg.get_texts()
# plt.setp(ltext, fontsize=fontsize)
# plt.setp(leg.get_frame(), linewidth=0.5)

plt.savefig('vm-startup-latency')
