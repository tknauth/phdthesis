import random as rnd
import glob
import collections
import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import re
import fileinput
import numpy as np
from pylab import *
import colorscheme
import os.path

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("font", weight="medium")
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 3.2
height = width/1.3

pylab.rc("figure.subplot", left=(30/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

def consecutive_blocks(blocknrs) :
    cur = prev = None
    result = []
    consecutive = 1
    for block in blocknrs :
        cur = block
        if not prev :
            prev = cur
            continue
        else :
            if cur - prev == 1 :
                consecutive += 1
            else :
                result.append(consecutive)
                consecutive = 1
            prev = cur
    return result

def consecutive_vs_totalblocks(consecutives) :
    xs = list(set(consecutives))
    xs.sort()
    freq = [consecutives.count(x) for x in xs]

    ys = []
    running_total = 0
    for i in range(len(xs)) :
        running_total += xs[i] * freq[i]
        ys.append(running_total)

    return (xs, ys)

consecutives = []
for fn in glob.glob(os.path.expanduser('~/phd/devicemapper/exp/client-192.168.2.132/rubis-2013-01-17_21:*-??.csv')) :
    consecutives.extend(consecutive_blocks(map(int, open(fn).readlines())))

fig = plt.figure(figsize=(width, height))
ax1 = fig.add_subplot(111)
ax1.set_xscale('log')

(xs, ys) = consecutive_vs_totalblocks(consecutives)
plt.plot(xs, ys, '.-', label='realistic', clip_on=False, color=colorscheme.method2color['rsync'])

# Create as many randomly distributed blocks and measure their
# proximity.
blocks_synthetic = rnd.sample(xrange(10*ys[-1]), ys[-1])
blocks_synthetic.sort()
(xs, ys) = consecutive_vs_totalblocks(consecutive_blocks(blocks_synthetic))
plt.plot(xs, ys, 'x-', label='synthetic', clip_on=False, color=colorscheme.method2color['dsync'])

# http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
for loc, spine in ax1.spines.iteritems():
    if loc in ['left','bottom']:
        pass
    elif loc in ['right','top']:
        spine.set_color('none') # don't draw spine
    else:
        raise ValueError('unknown spine location: %s'%loc)

# turn off ticks where there is no spine
ax1.xaxis.set_ticks_position('bottom')
ax1.yaxis.set_ticks_position('left')

ax1.set_xscale('log')
#ax1.set_ylim(0, 46000)
ax1.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x, pos : str(int(x/1000))+'k'))

plt.xlabel('Consecutivly modified blocks')
plt.ylabel('Modified blocks total')
plt.legend(['realistic', 'synthetic'] , loc=4, numpoints=1)
leg = plt.gca().get_legend()
leg.draw_frame(False)

plt.savefig('consecutive-blocks-frequency-vs-totalblocks')
