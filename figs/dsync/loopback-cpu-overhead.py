#!/usr/bin/python2.7

import subprocess as sp
import random
from subprocess import check_call
import itertools as it
from shlex import split
import time
import os

parameter_order = ['device', 'access', 'run']
timestamp = time.strftime("%Y%m%dT%H%M%S")

loopback_file_path='/tmp/bigfile'
size_mb=8*1024
update_percentage = 3
block_size_byte=4096
blocks = size_mb*1024*1024/block_size_byte

def global_setup() :
    # check_call(split('dd if=/dev/zero of=%s bs=1M count=%d'%(loopback_file_path, size_mb)))
    check_call(split('sudo sync'))

def global_teardown() :
    # check_call(split('rm %s'%(loopback_file_path)))
    pass

def setup_loop() :
    check_call(split('sudo losetup /dev/loop0 %s'%(loopback_file_path)))
    time.sleep(1)
    check_call(split('sudo chmod o+rw /dev/loop0'))

def tear_loop() :
    check_call(split('sudo chmod o-rw /dev/loop0'))
    check_call(split('sudo losetup -d /dev/loop0'))

def run(cfg) :

    # use inflection to call the appropriate setup_* function
    for k in cfg.keys() :
        if 'setup_'+str(cfg[k]) in globals() :
            globals()['setup_'+str(cfg[k])]()

    rand_4k = ''.join([chr(random.randint(0,255)) for _ in xrange(block_size_byte)])

    cfgstr = '-'.join([k+'='+str(cfg[k]) for k in cfg.keys()])
    p = sp.Popen(split('sar -u 1'),
                 stdout=open('%s-loopback-cpu-overhead-sar-%s'%(timestamp, cfgstr), 'w'))

    dev2file = {'phy' : loopback_file_path, 'loop' : '/dev/loop0'}
    fd_flags = os.O_WRONLY
    if cfg['access'] == 'read' :
        fd_flags = os.O_RDONLY

    fd = os.open(dev2file[cfg['device']], fd_flags)

    update_blocks = random.sample(xrange(blocks), (blocks*update_percentage)/100)
    update_blocks = sorted(update_blocks)
    for block in update_blocks :
        offset_byte = block*block_size_byte
        os.lseek(fd, offset_byte, os.SEEK_SET)
        if cfg['access'] == 'read' :
            os.read(fd, block_size_byte)
        elif cfg['access'] == 'write' :
            os.write(fd, rand_4k)

    os.close(fd)
    check_call(split('sudo sync'))

    p.terminate()
    p.wait()

    # use inflection to call the appropriate teardown_* function
    for k in cfg.keys() :
        if 'tear_'+str(cfg[k]) in globals() :
            globals()['tear_'+str(cfg[k])]()

if __name__ == '__main__' :
    
    paramspace = {'device' : ['loop'],
                  'access' : ['read', 'write'],
                  'run' : range(1)}

    global_setup()

    cfgs = it.product(*[paramspace[k] for k in parameter_order])
    for cfg in cfgs :
        # turn list into dict again
        d = {}
        for i in range(len(paramspace.keys())) :
            d[parameter_order[i]] = cfg[i]
        run(d)

    global_teardown()
