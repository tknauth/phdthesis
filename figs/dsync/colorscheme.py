# http://colorbrewer2.org/

# sequential; 5-class BuPu
# method2color={'rsync' : '#bfd3e6',
#               'dsync' : '#b3cde3',
#               'copy' : '#8c96c6',
#               'blockmd5sync' : '#8856a7',
#               'zfs' : '#810f7c'}

# qualtivative; 5-class set1

method2color={'rsync' : '#e41a1c',
              'dsync' : '#377eb8',
              'copy' : '#4daf4a',
              'blockmd5sync' : '#984ea3',
              'zfs' : '#ff7f00'}

c=[
method2color['rsync'],
method2color['dsync'],
method2color['copy'],
method2color['blockmd5sync'],
method2color['zfs']]
