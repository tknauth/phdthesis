import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import subprocess as sp
import colorscheme as cs

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

# width = 6.5
# height = 2.0
width = 1.9
height = width/1.3

pylab.rc("figure.subplot", left=(32/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-22/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

# xs may be a list of list. This creates a multi-plot figure.
def plot_single_figure(bars, disk, xlabel='', ylabel='') :
    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    ind = np.array([0, 2/4., 4/4.])
    bar_width = 0.3
    plt.bar(ind+bar_width, bars, bar_width, color=cs.c[0],
            linewidth=0.5)

    # print exact values above the bars
    ymax = 80
    for (x, y) in zip(ind+bar_width, bars) :
        ax1.text(x+0.06, y+0.025*ymax, '%d'%(y), rotation=0, verticalalignment='bottom')

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('none')
    ax1.yaxis.set_ticks_position('none')
    ax1.set_xticks(ind+1.5*bar_width)
    ax1.set_xticklabels(disk)
    ax1.set_yticks(np.arange(0,121,20))
    # ax1.set_ylim(0, 24)
    ax1.set_xlim(0, 2)
    # ax1.set_yscale('log')

    # for i, label in enumerate(ax1.xaxis.get_ticklabels()) :
    #     if (i % 2) == 0 : continue
    #     # print dir(label)
    #     print label.get_transform()
    ax1.xaxis.majorTicks[1].set_pad(12)
        

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    # plt.legend(bbox_to_anchor=(0.5, 1.22), loc="upper center", numpoints=1, ncol=3)
    # leg = plt.gca().get_legend()
    # print dir(leg)
    # leg.draw_frame(False)
    # ltext = leg.get_texts()
    # plt.setp(ltext, fontsize=fontsize)
    # plt.setp(leg.get_frame(), linewidth=0.5)

    plt.savefig('ssh-cipher-throughput-fig')

# average runtime in seconds from 20140304T162211-ssh-cipher-throughput.csv
ys = [33.388, 18.958, 116.56]
# convert to megabyte per second; total data size was 2 GiB
ys = map(lambda x : 2*1024/x, ys)

plot_single_figure(ys, ['aes128-ctr', 'arcfour128', '3des-cbc'],
                   '', 'Throughput [MB/s]')
