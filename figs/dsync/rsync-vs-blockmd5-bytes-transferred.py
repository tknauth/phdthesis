import math
import matplotlib
import matplotlib.mlab as mlab
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import os.path

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("font", weight="medium")
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 1.9
height = width/1.3

pylab.rc("figure.subplot", left=(3/72.27)/width)
pylab.rc("figure.subplot", right=(width-2/72.27)/width)
pylab.rc("figure.subplot", bottom=(20/72.27)/height)
pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

fig = plt.figure(figsize=(width, height))
ax1 = fig.add_subplot(111)

# See personal notes file 2014-37.txt
rsync_cpu_sec=1720.52+147.09
md5sync_cpu_sec=358.21
rsync_tx_bytes=90073435880-86393929933
md5sync_tx_bytes=82667981914-79029687480

plt.bar([-0.25, 0.75], [rsync_cpu_sec, md5sync_cpu_sec], 0.25, align='edge', color=['#377eb8'], linewidth=0.5, label='CPU [min]')
plt.bar([0.0, 1.0], [rsync_tx_bytes/(1024*1024), md5sync_tx_bytes/(1024*1024)], 0.25, align='edge', color=['#e41a1c'], linewidth=0.5, label='Tx traffic [MB]')

# CPU time
plt.annotate('%.1f'%(rsync_cpu_sec/60.), (-0.23, 1970))
plt.annotate('%.1f'%(md5sync_cpu_sec/60.), (0.8,  450))
# Network tx traffic
plt.annotate('%d'%(rsync_tx_bytes/float(1024**2)), (0.01, 3600))
plt.annotate('%d'%(md5sync_tx_bytes/float(1024**2)), (1.01,  3550))


# turn off ticks where there is no spine
ax1.xaxis.set_ticks_position('bottom')
ax1.yaxis.set_ticks_position('none')
ax1.set_xticks([0,1])

locs, labels = plt.xticks()
plt.xticks(locs, ['rsync', 'block-\nmd5sync'])
ax1.set_yticks([])
ax1.set_ylim(0, 3800)
ax1.set_xlim(-0.5, 1.5)
# ax1.set_yscale('log')

# plt.xlabel('Sync Method')
# plt.ylabel('CPU Time [sec]')

# http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
for loc, spine in ax1.spines.iteritems():
    if loc in ['bottom']:
        pass
    elif loc in ['left', 'right','top']:
        spine.set_color('none') # don't draw spine
    else:
        raise ValueError('unknown spine location: %s'%loc)

plt.legend(bbox_to_anchor=(1.02, 1.05),
           bbox_transform=plt.gcf().transFigure,
           loc=1, numpoints=1, ncol=2,
           handlelength=1.0)
leg = plt.gca().get_legend()
leg.draw_frame(False)
ltext = leg.get_texts()
plt.setp(ltext, fontsize=fontsize)
plt.setp(leg.get_frame(), linewidth=0.5)

plt.savefig('rsync-vs-blockmd5-bytes-transferred')
