import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import subprocess as sp
import colorscheme as cs

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

# width = 6.5
# height = 2.0
width = 3.2
height = width/1.3

pylab.rc("figure.subplot", left=(32/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-22/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

def parse_input(filename) :
    vals = []
    for line in open(filename) :
        line = line.strip()
        fields = line.split()
        kv = {}
        for field in fields :
            (k, v) = field.split('=')
            kv[k] = v
        vals.append(kv)
    return vals

def get_block_count(block_file) :
    p = sp.Popen('wc -l %s | cut -f1 -d" "'%(block_file), shell=True, stdout=sp.PIPE)
    (out, err) = p.communicate()
    return int(out.strip())

plot_order = ['rsync', 'dsync', 'copy', 'blockmd5sync', 'zfs']
hatch=['', '/', '\\', 'x', '']
# xs may be a list of list. This creates a multi-plot figure.
def plot_single_figure(bars, disk, xlabel='', ylabel='') :
    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    ind = np.arange(len(bars.values()[0]))
    bar_width = 0.15
    rects = []
    for (i, k) in enumerate(plot_order) :
        plt.bar(ind+bar_width*i, bars[k], bar_width, color=cs.method2color[k], label=k,
                hatch=None, linewidth=0.5)
        

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('none')
    ax1.yaxis.set_ticks_position('none')
    ax1.set_xticks(ind+2.5*bar_width)
    ax1.set_xticklabels(range(len(ind)))
    # ax1.set_yticks([6,12,18,24])
    # ax1.set_ylim(0, 24)
    # ax1.set_xlim(0, 33)
    # ax1.set_yscale('log')

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    plt.legend(bbox_to_anchor=(0.5, 1.22), loc="upper center", numpoints=1, ncol=3)
    leg = plt.gca().get_legend()
    print dir(leg)
    leg.draw_frame(False)
    ltext = leg.get_texts()
    plt.setp(ltext, fontsize=fontsize)
    plt.setp(leg.get_frame(), linewidth=0.5)

    plt.savefig('synctime-bar-'+disk)

# rsync-1374153582
vals = parse_input('rsync-1374077837')

for disk in ['hdd', 'ssd'] :
    vals_filtered = filter(lambda kv : kv['disk'] == disk, vals)
    bars = {}
    for method in set([kv['method'] for kv in vals_filtered]) :
        bars[method] = filter(lambda kv : kv['method'] == method,
                              vals_filtered)
        synctimes_per_blockfile = []
        for block_file in sorted(set([kv['block_file'] for kv in bars[method]]))[0:3] :
            synctime_sec = filter(lambda kv : kv['block_file'] == block_file,
                                  bars[method])
            synctime_sec = [float(kv['duration_sec']) for kv in synctime_sec]
            synctime_sec = np.mean(synctime_sec)
            synctimes_per_blockfile.append(synctime_sec)
            print disk, method, block_file, synctime_sec
        bars[method] = synctimes_per_blockfile

    plot_single_figure(bars, disk, 'Day of block-level trace', 'Time [s]')
