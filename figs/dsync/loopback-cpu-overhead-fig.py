import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import subprocess as sp
import colorscheme as cs

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

# width = 6.5
# height = 2.0
width = 1.9
height = width/1.3

pylab.rc("figure.subplot", left=(32/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-22/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

plot_order = ['w/o lo', 'w/ lo']
hatch=['', '']
# xs may be a list of list. This creates a multi-plot figure.
def plot_single_figure(bars, disk, xlabel='', ylabel='') :
    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    ind = np.arange(len(bars))
    bar_width = 0.3
    plt.bar(ind+bar_width, bars, bar_width, color=cs.c[0],
            linewidth=0.5)

    # print exact values above the bars
    ymax = 80
    for (x, y) in zip(ind+bar_width, bars) :
        ax1.text(x+0.06, y+0.025*ymax, '%d'%(y), rotation=0, verticalalignment='bottom')

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('none')
    ax1.yaxis.set_ticks_position('none')
    ax1.set_xticks(ind+1.5*bar_width)
    ax1.set_xticklabels(disk)
    ax1.set_yticks(np.arange(0,100,20))
    # ax1.set_ylim(0, 24)
    ax1.set_xlim(0, len(bars))
    # ax1.set_yscale('log')

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    # plt.legend(bbox_to_anchor=(0.5, 1.22), loc="upper center", numpoints=1, ncol=3)
    # leg = plt.gca().get_legend()
    # print dir(leg)
    # leg.draw_frame(False)
    # ltext = leg.get_texts()
    # plt.setp(ltext, fontsize=fontsize)
    # plt.setp(leg.get_frame(), linewidth=0.5)

    plt.savefig('loopback-cpu-overhead-fig')

plot_single_figure([100-77.2, 100-53.9], ['w/o lo', 'w/ lo'], 'configuration', 'CPU util [\%]')
