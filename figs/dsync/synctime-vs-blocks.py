import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import subprocess as sp
import colorscheme as cs

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 3.2
height = width/1.3

pylab.rc("figure.subplot", left=(30/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

def parse_input(filename, disk='hdd') :
    # data points
    vals = {}
    for line in open(filename) :
        if not line.startswith('method=') : continue
        kv_pairs = line.strip().split()
        kv = {}
        for kv_pair in kv_pairs :
            (k, v) = kv_pair.split('=')
            kv[k] = v

        if kv['disk'] != disk : continue

        xy_map = vals.setdefault(kv['method'], {})
        block_count = get_block_count(kv['block_file'])
        xs = xy_map.setdefault(block_count, [])
        xs.append(float(kv['duration_sec']))
    return vals

def get_block_count(block_file) :
    p = sp.Popen('wc -l %s | cut -f1 -d" "'%(block_file), shell=True, stdout=sp.PIPE)
    (out, err) = p.communicate()
    return int(out.strip())

plot_order = ['rsync', 'dsync', 'copy']
method2linestyle = {'rsync' : 'o-', 'dsync' : 's-', 'copy' : '^-', 'blockmd5sync' : 'x-', 'zfs' : 'v-'}

# xs may be a list of list. This creates a multi-plot figure.
def plot_single_figure(xs, ys, filename, xlabel='', ylabel='',
                       labels='') :
    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    # do not connect data points with line, i.e., remove - (minus
    # sign) from linestyle
    linestyle = map(lambda ls : ls[0], [method2linestyle[k] for k in plot_order])
    color = [cs.method2color[k] for k in plot_order]
    for (i, y) in enumerate(ys) :
        plt.plot(xs, y, linestyle[i], color=color[i], label=labels[i])

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_ticks_position('left')
    # ax1.set_xticks([1,2,4,8,16,32])
    # ax1.set_yticks([6,12,18,24])
    # ax1.set_ylim(0, 24)
    # ax1.set_xlim(0, 33)
    ax1.set_yscale('log')

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    plt.legend(bbox_to_anchor=(0.05,0.95,1,.102), loc=2, numpoints=1, ncol=3)
    leg = plt.gca().get_legend()
    leg.draw_frame(False)
    ltext = leg.get_texts()
    plt.setp(ltext, fontsize=fontsize)
    plt.setp(leg.get_frame(), linewidth=0.5)

    plt.savefig(filename)

vals = parse_input('synctime-vs-blocks-rubis.csv')

xs = []
multi_plot_ys = []
for (i, method) in enumerate(plot_order) :
    ys = []
    # There may be multiple data points for each x. We do not want to
    # average them, but include them all in our plot.
    for x in sorted(vals[method].keys()) :
        if i == 0 : xs.extend([x]*len(vals[method][x]))
        ys.extend(map(float, vals[method][x]))

    multi_plot_ys.append(ys)

plot_single_figure(xs, multi_plot_ys,
                   'synctime-vs-blocks-rubis',
                   xlabel='Number of modified blocks',
                   ylabel='Synchronization time [s]',
                   labels=plot_order)

throughput_multi_ys = []
for ys in multi_plot_ys :
    tput_ys = []
    for (i, y) in enumerate(ys) :
        tput_ys.append(xs[i]*1024.0/y)
    throughput_multi_ys.append(tput_ys)

plot_single_figure(xs, throughput_multi_ys,
                   'tput-vs-blocks-rubis', xlabel='Number of modified blocks',
                   ylabel='Throughput [MB/s]', labels=plot_order)

xs = []
ys = []
for x in sorted(vals['dsync'].keys()) :
    xs.extend([x]*len(vals['dsync'][x]))
    ys.extend(vals['dsync'][x])

print map(type, xs), len(xs)
print map(type, ys), len(ys)

plot_single_figure(xs, [ys],
                   'synctime-vs-blocks-rubis-hdd-dsync',
                   xlabel='Number of modified blocks',
                   ylabel='Synchronization time [s]',
                   labels=['dsync'])
print np.corrcoef(xs, ys)

# for x in [t, ax1.xaxis.get_major_ticks()[0].label] :
#     print x.get_weight()
#     print x.get_fontweight()
#     print x.get_fontsize()
#     print x.get_fontproperties()
#     print x.get_fontvariant()
#     print x.get_fontname()
#     print x.get_fontstyle()
#     print x.get_fontstretch()

# ax2 = ax1.twinx()
# ax2.plot(xs, ys2, 'r.')
# ax2.set_ylabel('transitions')
