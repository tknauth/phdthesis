import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 3.2
height = width/1.3

pylab.rc("figure.subplot", left=(30/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt
import colorscheme as cs

linestyle=['-', '-', '-']
plot_order = ['rsync', 'copy', 'dsync']

# xs may be a list of list. This creates a multi-plot figure.
def plot_single_figure(xs, ys, filename, xlabel='', ylabel='',
                       labels=None,
                       customize=None) :
    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)
    
    for (i, y) in enumerate(ys) :
        method = plot_order[i]
        plt.plot(xs[i], y, linestyle[i], color=cs.method2color[method], label=labels[i])

    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_ticks_position('left')
    # ax1.set_xticks([1,2,4,8,16,32])
    # ax1.set_yticks([0,20,40,60,80,100,120])
    # ax1.set_ylim(0, 120)
    # ax1.set_xlim(0, 33)

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    plt.legend(loc=1, numpoints=1)
    leg = plt.gca().get_legend()
    if leg :
        leg.draw_frame(False)
        ltext = leg.get_texts()
        plt.setp(ltext, fontsize=fontsize)
        plt.setp(leg.get_frame(), linewidth=0.5)

    if customize :
        customize(plt, ax1)

    plt.savefig(filename)

def movingaverage(interval, window_size) :
    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(interval, window, 'same')

def extract_cpu_util(fn) :
    ys = []
    f = open(fn, 'r')
    for line in f.readlines()[7:] :
        fields = line.strip().split(',')
        usr_sys_wait = sum(map(float, [fields[2], fields[3], fields[5]]))
        ys.append(usr_sys_wait)
    return ys

def extract_net_tx(fn) :
    ys = []
    f = open(fn, 'r')
    for line in f.readlines()[7:] :
        fields = line.strip().split(',')
        tx_bytes = float(fields[9])
        ys.append(tx_bytes)
    return ys

from os.path import expanduser
home = expanduser("~")
method2filename = {'dsync' : home+'/phd/devicemapper/exp/1375372816-dsync-sshnone-32GB-ssd-random-010-00-192.168.2.3-dstat',
                   'rsync' : home+'/phd/devicemapper/exp/1373558022-rsync-sshnone-32GB-ssd-random-00-192.168.2.3-dstat',
                   'copy'  : home+'/phd/devicemapper/exp/1373558022-copy-sshnone-32GB-ssd-random-00-192.168.2.3-dstat'}
multi_plot_ys = []
for method in plot_order :
    ys = extract_cpu_util(method2filename[method])
    # ys = movingaverage(ys, 5)
    multi_plot_ys.append(ys)

def customize_cpu_plot(plt, ax) :

    arrowprops=dict(arrowstyle="->",
                    connectionstyle="arc3",
                    linewidth=0.5)

    xy = {'dsync' : (230, 35),
          'rsync' : (1000, 20),
          'copy'  : (270, 16)}

    for m in xy.keys() :
        (x, y) = xy[m]
        ax.annotate(m, xy=(x, y), xycoords='data',
                    xytext=(x+100, y+5), textcoords='data',
                    arrowprops=arrowprops)

def customize_net_plot(plt, ax) :

    arrowprops=dict(arrowstyle="->",
                    connectionstyle="arc3",
                    linewidth=0.5)

    xy = {'dsync' : (220, 17),
          'rsync' : (1000, 4),
          'copy'  : (350, 80)}

    for m in xy.keys() :
        (x, y) = xy[m]
        ax.annotate(m, xy=(x, y), xycoords='data',
                    xytext=(x+100, y+10), textcoords='data',
                    arrowprops=arrowprops)

xs = map(range, map(len, multi_plot_ys))
plot_single_figure(xs, multi_plot_ys,
                   'cpu-utilization',
                   xlabel='Time [s]',
                   ylabel='CPU utilization [\%]',
                   labels=[""]*3,
                   customize=customize_cpu_plot)

multi_plot_ys = []
for method in plot_order :
    ys = extract_net_tx(method2filename[method])
    # Convert from bytes to megabytes
    # ys = movingaverage(ys, 5)
    ys = map(lambda x : x/1024./1024, ys)
    multi_plot_ys.append(ys)

print multi_plot_ys[0][0:10], multi_plot_ys[1][0:10]
plot_single_figure(xs, multi_plot_ys,
                   'net-tx',
                   xlabel='Time [s]',
                   ylabel='Network transmit traffic [MB/s]',
                   labels=[""]*3,
                   customize=customize_net_plot)
