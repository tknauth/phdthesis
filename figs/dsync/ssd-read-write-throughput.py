import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import subprocess as sp
import colorscheme as cs
from collections import defaultdict
from matplotlib.ticker import NullLocator

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

# width = 6.5
# height = 2.0
width = 1.9
height = width/1.3

pylab.rc("figure.subplot", left=(32/72.27)/width)
pylab.rc("figure.subplot", right=(width-3/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-8/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

# xs may be a list of list. This creates a multi-plot figure.
def plot_single_figure(xs, ys) :

    fig = plt.figure(figsize=(width, height))
    ax1 = fig.add_subplot(111)

    for key in ys.keys() :
        print type(ys[key]), ys[key]
        ys_avg = map(np.average, ys[key])
        plt.plot(xs, ys_avg, label=key, linewidth=0.5, marker='x', markersize=3)

    # turn off ticks where there is no spine
    ax1.set_xscale('log')
    ax1.tick_params(size=2)
    ax1.xaxis.set_minor_locator(NullLocator())
    ax1.xaxis.set_ticks_position('bottom')
    ax1.xaxis.set_ticks([4*1024, 32*1024, 1024*1024])
    ax1.yaxis.set_ticks_position('left')
    # ax1.set_xticks([4*1024, 32*1024, 1024*1024])
    ax1.set_xticklabels(['4 KiB', '32 KiB', '1 MiB'], fontsize=fontsize)
    ax1.set_yticks(np.arange(0, 301, 100))
    # ax1.set_ylim(0, 24)
    ax1.set_xlim(1024, 2*1024*1024)

    # for i, label in enumerate(ax1.xaxis.get_ticklabels()) :
    #     if (i % 2) == 0 : continue
    #     # print dir(label)
    #     print label.get_transform()
    # ax1.xaxis.majorTicks[1].set_pad(12)
        
    plt.xlabel('Block size')
    plt.ylabel('Throughput [MB/s]')

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

    # plt.legend(bbox_to_anchor=(0.5, 1.22), loc="upper center", numpoints=1, ncol=3)
    # leg = plt.gca().get_legend()
    # print dir(leg)
    # leg.draw_frame(False)
    # ltext = leg.get_texts()
    # plt.setp(ltext, fontsize=fontsize)
    # plt.setp(leg.get_frame(), linewidth=0.5)

    ax1.text(2*1024, 40, 'random')
    ax1.text(2*1024, 290, 'sequential')

    plt.savefig('20140410T115244-ssd-read-write-throughput')

blocksizes = None
with open('20140410T115244-blockdev.csv') as f :
    records = f.readlines()
    blocksizes = sorted(list(set([r.split()[2].split('=')[1] for r in records])))
    orders = ['random', 'sequential']

    ys = defaultdict(list)
    for order in orders :
        for blocksize in blocksizes :
            subset = records
            subset = filter(lambda r : 'order=%s'%(order) in r, subset)
            subset = filter(lambda r : 'access=read' in r, subset)
            subset = filter(lambda r : 'blocksize=%s'%(blocksize) in r, subset)
            subset = [r.split()[4].split('=')[1] for r in subset]
            subset = map(float, subset)
            subset = map(lambda time_sec : 4*1024/time_sec, subset)
            ys[order].append(subset)

plot_single_figure(blocksizes, ys) 
