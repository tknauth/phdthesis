#!/usr/bin/python2.7

import subprocess as sp
import random
from subprocess import check_call
import itertools as it
from shlex import split
import time
import os

parameter_order = ['cipher', 'run']
timestamp = time.strftime("%Y%m%dT%H%M%S")

file_results = open('%s-ssh-cipher-throughput.csv'%(timestamp), 'w')

def global_setup() :
    pass

def global_teardown() :
    pass

def run(cfg) :

    # use inflection to call the appropriate setup_* functions
    for k in cfg.keys() :
        if 'setup_'+str(cfg[k]) in globals() :
            globals()['setup_'+str(cfg[k])]()

    start = time.time()
    check_call('dd if=/dev/zero bs=1M count=2k | ssh 141.76.44.147 -c %s dd of=/dev/zero'%(cfg['cipher']), shell=True)
    elapsed = time.time() - start

    print >> file_results, 'cipher=%s run=%d time_s=%.2f seconds'%(cfg['cipher'], cfg['run'], elapsed)

    # use inflection to call the appropriate teardown_* function
    for k in cfg.keys() :
        if 'tear_'+str(cfg[k]) in globals() :
            globals()['tear_'+str(cfg[k])]()

if __name__ == '__main__' :
    
    paramspace = {'cipher' : ['aes128-ctr', 'arcfour128', '3des-cbc'],
                  'run' : range(5)}

    global_setup()

    cfgs = it.product(*[paramspace[k] for k in parameter_order])
    for cfg in cfgs :
        # turn list into dict again
        d = {}
        for i in range(len(paramspace.keys())) :
            d[parameter_order[i]] = cfg[i]
        run(d)

    global_teardown()
