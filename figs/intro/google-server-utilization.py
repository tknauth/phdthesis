import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import os.path
import datetime

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("font", weight="medium")
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 3.2
height = 0.8*width/1.3

pylab.rc("figure.subplot", left=(35/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-5/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

def parse_input(filename) :

    xs = []
    ys = []
    with open(filename) as f :
        for line in f.readlines() :
            line = line.strip()
            fields = line.split(',')
            xs.append(float(fields[0]))
            ys.append(float(fields[1]))
                
    return xs, ys

def turn_off_spines(ax1) :
    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

def turn_off_ticks(ax1) :
    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_ticks_position('left')

    # ax1.set_xticks([1,2,4,8,16,32])
    # ax1.set_yticks([0,1])
    # ax1.set_ylim(0, 3.5)
# ax1.set_yscale('log')

fig = plt.figure(figsize=(width, height))
ax1 = fig.add_subplot(111)
turn_off_spines(ax1)
turn_off_ticks(ax1)
ax1.tick_params('both', length=2)
xs, ys = parse_input('google-server-utilization.csv')
bar_width=1.0/float(len(xs))
plt.bar(np.linspace(0,1,len(ys), endpoint=False), ys, bar_width, color='#FFFFFF', edgecolor='#377eb8', linewidth=0.5)
plt.ylabel('Fraction of time')
plt.xlabel('CPU Utilization')

# ax1.set_yticks([6,12,18,24])
# ax1.set_ylim(0, 3.5)
# ax1.set_yscale('log')

# plt.legend(loc=2, numpoints=1, ncol=1)
# leg = plt.gca().get_legend()
# leg.draw_frame(False)
# ltext = leg.get_texts()
# plt.setp(ltext, fontsize=fontsize)
# plt.setp(leg.get_frame(), linewidth=0.5)

plt.savefig('google-server-utilization')
