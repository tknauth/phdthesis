import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import os.path

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("font", weight="medium")
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 1.9
height = width/1.3

pylab.rc("figure.subplot", left=(30/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(15/72.27)/height)
pylab.rc("figure.subplot", top=(height-5/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

fig = plt.figure(figsize=(width, height))
ax1 = fig.add_subplot(111)

plt.step([0, 20, 40, 60, 80, 100], [41.2, 41.2, 52.9, 74.6, 104.3, 122.6], linestyle='-', color='red', label='CPU')
# plt.step([0, 100], [10, 10], linestyle='-', color='blue', label='Disk')

# turn off ticks where there is no spine
ax1.xaxis.set_ticks_position('bottom')
ax1.yaxis.set_ticks_position('left')
ax1.set_xticks([])
# ax1.set_yticks([500,1000])
ax1.set_ylim(0)
# ax1.set_xlim(0, 100)
# ax1.set_yscale('log')
# locs, labels = plt.xticks()
# plt.xticks(locs, [[str(int(x)) for x in locs[2:]])
plt.xlabel('Load')
plt.ylabel('Power [W]')

# plt.text(80, 127, 'CPU')
# plt.text(80, 15, 'HDD')

plt.text(7, 44, 'P6')
plt.text(25, 56, 'P5')
plt.text(45, 77, 'P4')
plt.text(66, 107, 'P3')
plt.text(85, 125, 'P2')

# http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
for loc, spine in ax1.spines.iteritems():
    if loc in ['left','bottom']:
        pass
    elif loc in ['right','top']:
        spine.set_color('none') # don't draw spine
    else:
        raise ValueError('unknown spine location: %s'%loc)

# plt.legend(loc=0, numpoints=1, ncol=2)
# leg = plt.gca().get_legend()
# leg.draw_frame(False)
# ltext = leg.get_texts()
# plt.setp(ltext, fontsize=fontsize)
# plt.setp(leg.get_frame(), linewidth=0.5)

plt.savefig('performance-energy-functions')
