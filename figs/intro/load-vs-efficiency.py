import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import os.path
import datetime
from scipy import interpolate

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("font", weight="medium")
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 5.0
height = 0.43*width

pylab.rc("figure.subplot", left=(30/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(21/72.27)/height)
pylab.rc("figure.subplot", top=(height-3/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

def turn_off_spines(ax1) :
    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

def turn_off_ticks(ax1) :
    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_ticks_position('left')

    # ax1.set_xticks([1,2,4,8,16,32])
    # ax1.set_yticks([0,1])
    # ax1.set_ylim(0, 3.5)
# ax1.set_yscale('log')

fig = plt.figure(figsize=(width, height))
ax1 = fig.add_subplot(111)
turn_off_spines(ax1)
turn_off_ticks(ax1)
ax1.tick_params('both', length=2)

def pow10(x) :
    return 10. + (x/100.)*(100.-10.)
def pow50(x) :
    return 50. + (x/100.)*(100.-50.)

def pow10sub(x) :
    # http://arohatgi.info/WebPlotDigitizer/app/?
    values = [(0.0, 10), 
    (1.0601866927168313, 10.638901196278267),
    (3.543633995441237, 10.638901196278265),
    (6.704385107999574, 11.208603536472408),
    (9.18783241072398, 11.376163048294197),
    (11.671279713448387, 12.012889193217095),
    (14.154727016172796, 12.482055826318145),
    (16.638174318897207, 13.219317678334082),
    (19.12162162162161, 14.023603335078764),
    (21.605068924346025, 14.894912796552173),
    (24.08851622707043, 16.000805574576074),
    (26.571963529794843, 17.039674547871293),
    (29.055410832519247, 18.34663874008141),
    (31.538858135243657, 19.720626737020133),
    (34.02230543796806, 21.128126636323373),
    (36.50575274069249, 22.736697949812715),
    (38.98920004341691, 24.445804970395134),
    (41.47264734614131, 26.2219357957063),
    (43.95609464886573, 28.199138035203617),
    (46.43954195159009, 30.176340274700934),
    (48.92298925431453, 32.32110202602005),
    (51.406436557038916, 34.59991138679662),
    (53.88988385976337, 36.91223264993759),
    (56.37333116248773, 39.358601522535935),
    (58.856778465212145, 41.97252990695612),
    (61.340225767936545, 44.686993998469376),
    (63.823673070660966, 47.50199379707577),
    (66.3071203733854, 50.38401740041084),
    (68.79056767610982, 53.40008861320337),
    (71.27401497883417, 56.55020743545331),
    (73.75746228155865, 59.800861964796376),
    (76.24090958428297, 63.11854029886813),
    (78.72435688700737, 66.6372900471261),
    (81.20780418973179, 70.22306360011268),
    (83.69125149245622, 73.90937286019256),
    (86.17469879518066, 77.69621782736529),
    (88.65814609790507, 81.61711040399567),
    (91.14159340062945, 85.6385386877189),
    (93.62504070335389, 89.79401458089981),
    (96.10848800607833, 94.05002618117364),
    (98.52420492781926, 98.29933540097467),
    (100.0, 100.0)]
    xs = [a for (a,b) in values]
    ys = [b for (a,b) in values]

    return interpolate.interp1d(xs, ys)(x)

plt.plot([pow10(x) for x in range(100)], color='#377eb8', linewidth=0.5, label='pow10')
plt.plot([pow50(x) for x in range(100)], color='red', linewidth=0.5, label='pow50')
plt.plot([pow10sub(x) for x in range(100)], color='green', linewidth=0.5, label='pow10sub')
plt.plot([100.*x/pow10(x) for x in range(100)], color='#377eb8', linestyle='--', linewidth=0.5, label='eff10')
plt.plot([100.*x/pow50(x) for x in range(100)], color='red', linestyle='--', linewidth=0.5, label='eff50')
plt.plot([100.*x/pow10sub(x) for x in range(100)], color='green', linestyle='--', linewidth=0.5, label='eff10sub')
plt.ylabel('\%')
plt.xlabel('Load level [\% of peak]')

# ax1.set_yticks([6,12,18,24])
# ax1.set_ylim(0, 3.5)
# ax1.set_yscale('log')

plt.legend(loc=2, numpoints=1, ncol=1)
leg = plt.gca().get_legend()
leg.draw_frame(False)
ltext = leg.get_texts()
plt.setp(ltext, fontsize=fontsize)
plt.setp(leg.get_frame(), linewidth=0.5)

plt.savefig('load-vs-efficiency')
