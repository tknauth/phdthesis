import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import os.path

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("font", weight="medium")
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 1.9
height = 1.2

pylab.rc("figure.subplot", left=(30/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-5/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

fig = plt.figure(figsize=(width, height))
ax1 = fig.add_subplot(111)

plt.plot([0, 8/3.], [50, 50], linestyle='-', color='black', label='Dynamic', clip_on=False)
plt.plot([0, 8/3.], [20, 20], linestyle='-', color='blue', label='Static')

y1=np.array([50,50])
y2=np.array([20,20])
ax1.fill_between([0,8/3.], y1, y2, color='red', hatch='//', facecolor='none', linewidth=0.0)

# turn off ticks where there is no spine
ax1.xaxis.set_ticks_position('bottom')
ax1.yaxis.set_ticks_position('left')
ax1.set_xticks([1,2])
# ax1.set_yticks([500,1000])
ax1.set_ylim(0, 100)
ax1.set_xlim(0, 2.7)
# ax1.set_yscale('log')
# locs, labels = plt.xticks()
# plt.xticks(locs, [[str(int(x)) for x in locs[2:]])
plt.xlabel('Time [hours]')
plt.ylabel('Power [W]')

plt.text(1.58, 7, 'static')
plt.text(1.58, 55, 'total')
plt.text(0.72, 31, 'dynamic')

# http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
for loc, spine in ax1.spines.iteritems():
    if loc in ['left','bottom']:
        pass
    elif loc in ['right','top']:
        spine.set_color('none') # don't draw spine
    else:
        raise ValueError('unknown spine location: %s'%loc)

# plt.legend(loc=0, numpoints=1, ncol=2)
# leg = plt.gca().get_legend()
# leg.draw_frame(False)
# ltext = leg.get_texts()
# plt.setp(ltext, fontsize=fontsize)
# plt.setp(leg.get_frame(), linewidth=0.5)

plt.savefig('low-power-example')
