import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import os.path
import datetime

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("font", weight="medium")
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 1.9
height = width/1.3

pylab.rc("figure.subplot", left=(31/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-4/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

fig = plt.figure(figsize=(width, height))
ax1 = fig.add_subplot(111)


ys = []
xs = []
with open('eu-west-1a.Linux_UNIX.m3.xlarge.csv') as f :
    for line in f :
        line = line.strip()
        fields = line.split(',')
        ys.append(float(fields[1])*100.0)
        xs.append(datetime.datetime.strptime(fields[0], '%Y-%m-%d %H:%M:%S'))

plt.step(xs, ys, linestyle='-', color='#377eb8')

# turn off ticks where there is no spine
ax1.xaxis.set_ticks_position('bottom')
ax1.yaxis.set_ticks_position('left')
ax1.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%b %d'))
ax1.xaxis.set_major_locator(matplotlib.dates.WeekdayLocator(byweekday=matplotlib.dates.MO))
locs, labels = plt.xticks()
plt.setp(labels, rotation=45)
# ax1.set_xticks([1,2,4,8,16,32])
# ax1.set_yticks([6,12,18,24])
# ax1.set_ylim(0, 3.5)
# ax1.set_xlim(0, 100)
# ax1.set_yscale('log')

# plt.xlabel('Date')
plt.ylabel('Price [Cents USD]')

# http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
for loc, spine in ax1.spines.iteritems():
    if loc in ['left','bottom']:
        pass
    elif loc in ['right','top']:
        spine.set_color('none') # don't draw spine
    else:
        raise ValueError('unknown spine location: %s'%loc)

# plt.legend(loc=2, numpoints=1, ncol=1)
# leg = plt.gca().get_legend()
# leg.draw_frame(False)
# ltext = leg.get_texts()
# plt.setp(ltext, fontsize=fontsize)
# plt.setp(leg.get_frame(), linewidth=0.5)

plt.savefig('spot-prices')
