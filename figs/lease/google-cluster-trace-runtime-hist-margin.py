import math
import matplotlib
import matplotlib.mlab as mlab
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import os.path
import workload

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("font", weight="medium")
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 1.9
height = width/1.3

pylab.rc("figure.subplot", left=(30/72.27)/width)
pylab.rc("figure.subplot", right=(width-7/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-5/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

fig = plt.figure(figsize=(width, height))
ax1 = fig.add_subplot(111)

# Parse input
v1 = []
with open('/mnt/myvg-data/home/thomas/phd/cgc2012/data/0-googletrace-input.csv', 'r') as f :
    for line in f.readlines() :
        line = line.strip()
        recs = line.split()
        v1.append(int(recs[3])/60)

print np.percentile(v1, [90.,95.,99.])

n, bins, patches = pylab.hist(v1,
                              bins=range(0,100),
                              normed=True,
                              #log=True,
                              color='#377eb8',
                              label='Duration',
                              linewidth=0.0, align='left')

# turn off ticks where there is no spine
ax1.xaxis.set_ticks_position('bottom')
ax1.yaxis.set_ticks_position('left')
# ax1.set_xticks([-120,0,120,240])
# ax1.set_yticks([-100,0,100,200,300])
# ax1.set_ylim(0, 3.5)
# ax1.set_xlim(0, 100)
# ax1.set_yscale('log')

plt.xlabel('Job duration [hours]')
plt.ylabel('Fraction')

# http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
for loc, spine in ax1.spines.iteritems():
    if loc in ['left','bottom']:
        pass
    elif loc in ['right','top']:
        spine.set_color('none') # don't draw spine
    else:
        raise ValueError('unknown spine location: %s'%loc)

# plt.legend(loc=1, numpoints=1, ncol=1)
# leg = plt.gca().get_legend()
# leg.draw_frame(False)
# ltext = leg.get_texts()
# plt.setp(ltext, fontsize=fontsize)
# plt.setp(leg.get_frame(), linewidth=0.5)

plt.savefig('google-cluster-trace-runtime-hist-margin')
