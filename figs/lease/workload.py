#!/usr/bin/env python2.7

import math
import random
from collections import defaultdict
from numpy import mean
import matplotlib as mpl

vm_sizes = [1,2,4,8]

class WorkloadGenerator :

    def __init__(self, mu, sigma, max_total_size) :
        self.max_total_size=max_total_size
        self.t_max = 24 * 60 * 3
        self.vms = defaultdict(list)
        self.total_size = 0
        self.t = 0
        self.mu = mu
        self.lambd = float(self.max_total_size)/(float(self.mu)*mean(vm_sizes))
        self.sigma = sigma
        self.next_arrival = random.expovariate(self.lambd)

    def generate_workload(self) :
        resources = []
        while self.t < self.t_max :
            # arriving VMs
            while self.next_arrival < self.t :
                runtime_minutes = random.normalvariate(self.mu, self.sigma)
                if runtime_minutes < 0 : runtime_minutes = 60
                runtime_hours = int(math.floor(runtime_minutes/60.0 + 0.5))
                # runtime_hours = runtime_minutes/60.
                # print runtime_hours

                size = random.choice(vm_sizes)
                self.vms[self.t+int(runtime_hours*60)].append(size)
                self.total_size += size
                self.next_arrival += random.expovariate(self.lambd)

            # expiring VMs
            for vm in self.vms[self.t] :
                self.total_size -= vm

            resources.append(self.total_size)
            self.t += 1

        return resources
