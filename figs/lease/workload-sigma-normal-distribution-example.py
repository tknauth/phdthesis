import math
import matplotlib
import matplotlib.mlab as mlab
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import os.path
import workload

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("font", weight="medium")
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 1.9
height = width/1.3

pylab.rc("figure.subplot", left=(30/72.27)/width)
pylab.rc("figure.subplot", right=(width-2/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-5/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

fig = plt.figure(figsize=(width, height))
ax1 = fig.add_subplot(111)

mean = 120
variance = 1
sigma = np.sqrt(variance)
xs = np.linspace(-200, 400, 400)

def custom_pdf(x, mean, sigma) :
    if x < 60.0 :
        return 0
    elif x == 60.0 :
        return 0.5 * (1+math.erf((x-mean)/(sigma*math.sqrt(2))))
    else :
        return mlab.normpdf(x, mean, sigma)

# plt.plot(xs, map(lambda x : custom_pdf(x, mean, 0.5*mean), xs), linestyle='-', color='#377eb8', label='$\sigma=60$')
# plt.plot(xs, map(lambda x : custom_pdf(x, mean, 0.75*mean), xs), linestyle='-', color='#4daf4a', label='$\sigma=90$')
# plt.plot(xs, map(lambda x : custom_pdf(x, mean, 1.0*mean), xs), linestyle='-', color='#ff7f00', label='$\sigma=120$')

bins = range(10)
v1 = np.random.normal(120, 60, size=1000)
v1 = map(lambda x : int(math.floor(max(x,60.0)/60.0 + 0.5)), v1)

v2 = np.random.normal(120, 90, size=1000)
v2 = map(lambda x : int(math.floor(max(x,60.0)/60.0 + 0.5)), v2)

v3 = np.random.normal(120, 120, size=1000)
v3 = map(lambda x : int(math.floor(max(x,60.0)/60.0 + 0.5)), v3)
n, bins, patches = pylab.hist([v1,v2,v3], bins,
                              color=['#377eb8', '#4daf4a', '#ff7f00'],
                              label=['$\sigma=0.5\mu$', '$\sigma=0.75\mu$', '$\sigma=1.0\mu$'],
                              linewidth=0.5, align='left')

# ys = workload.WorkloadGenerator(60*2, 60*2/3).generate_workload()
# plt.plot(xs, ys, linestyle='-.', color='#377eb8', label='$\sigma=2/3\mu$')

# ys = gen.generate_workload()
# plt.plot(xs, ys, linestyle='-', color='#bfd3e6', label='')

# turn off ticks where there is no spine
ax1.xaxis.set_ticks_position('bottom')
ax1.yaxis.set_ticks_position('left')
# ax1.set_xticks([-120,0,120,240])
# ax1.set_yticks([-100,0,100,200,300])
# ax1.set_ylim(0, 3.5)
# ax1.set_xlim(0, 100)
# ax1.set_yscale('log')

plt.xlabel('Instance runtime [hours]')
plt.ylabel('Frequency')

# http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
for loc, spine in ax1.spines.iteritems():
    if loc in ['left','bottom']:
        pass
    elif loc in ['right','top']:
        spine.set_color('none') # don't draw spine
    else:
        raise ValueError('unknown spine location: %s'%loc)

plt.legend(loc=1, numpoints=1, ncol=1)
leg = plt.gca().get_legend()
leg.draw_frame(False)
ltext = leg.get_texts()
plt.setp(ltext, fontsize=fontsize)
plt.setp(leg.get_frame(), linewidth=0.5)

plt.savefig('workload-sigma-normal-distribution-example')
