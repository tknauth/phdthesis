import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import os.path

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("font", weight="medium")
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 3.2
height = width/1.3

pylab.rc("figure.subplot", left=(30/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-20/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

fig = plt.figure(figsize=(width, height))
ax1 = fig.add_subplot(111)

def savings_upper_bound(cmu_savings) :
    assert cmu_savings >= 0
    assert cmu_savings <= 100

    cmu_before = 1.0
    cmu_after = (100.-cmu_savings)/100.
    cmu_delta = cmu_before - cmu_after

    return 100.*cmu_delta/cmu_before

def savings_lower_bound(cmu_savings, peak2idle) :
    assert cmu_savings >= 0
    assert cmu_savings <= 100

    cmu_before = 1.0
    cmu_after = (100.-cmu_savings)/100.
    cmu_delta = cmu_before - cmu_after

    return 100.*cmu_delta/(peak2idle*cmu_after+cmu_delta)

xs = range(0, 100+1)
ys = map(savings_upper_bound, xs)
plt.plot(xs, ys, linestyle='-', color='#377eb8', label='upper bound')

ys = map(lambda x : savings_lower_bound(x, 2.0), xs)
plt.plot(xs, ys, linestyle='-.', color='#377eb8', label='lower bound;\nold server')

ys = map(lambda x : savings_lower_bound(x, 5.0), xs)
plt.plot(xs, ys, linestyle='--', color='#377eb8', label='lower bound;\nmodern server')

# turn off ticks where there is no spine
ax1.xaxis.set_ticks_position('bottom')
ax1.yaxis.set_ticks_position('left')
# ax1.set_xticks([1,2,4,8,16,32])
# ax1.set_yticks([6,12,18,24])
# ax1.set_ylim(0, 3.5)
# ax1.set_xlim(0, 100)
# ax1.set_yscale('log')

plt.xlabel('CMU savings [\%]')
plt.ylabel('Power savings [\%]')

# http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
for loc, spine in ax1.spines.iteritems():
    if loc in ['left','bottom']:
        pass
    elif loc in ['right','top']:
        spine.set_color('none') # don't draw spine
    else:
        raise ValueError('unknown spine location: %s'%loc)

plt.legend(loc=2, numpoints=1, ncol=1)
leg = plt.gca().get_legend()
leg.draw_frame(False)
ltext = leg.get_texts()
plt.setp(ltext, fontsize=fontsize)
# plt.setp(leg.get_frame(), linewidth=0.5)

plt.savefig('cmu-savings-vs-power-savings')
