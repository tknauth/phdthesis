import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import os.path
import workload

fontsize = 7

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("font", weight="medium")
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

width = 1.9
height = width/1.3

pylab.rc("figure.subplot", left=(30/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(25/72.27)/height)
pylab.rc("figure.subplot", top=(height-5/72.27)/height)

matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

fig = plt.figure(figsize=(width, height))
ax1 = fig.add_subplot(111)

t_max = 12*60

color = {2*60 : '#377eb8',
         4*60 : '#4daf4a',
         8*60 : '#ff7f00'}
for avg_runtime_minutes in [2*60, 4*60, 8*60] :
    xs = range(t_max)
    gen = workload.WorkloadGenerator(avg_runtime_minutes, avg_runtime_minutes/3., 500)
    gen.t_max=t_max
    ys = gen.generate_workload()
    plt.plot(xs, ys, linestyle='-', color=color[avg_runtime_minutes],
             label='$\mu=%d$'%(avg_runtime_minutes))

# # ys = workload.WorkloadGenerator(60*2, 60*2/3).generate_workload()
# # plt.plot(xs, ys, linestyle='-.', color='#377eb8', label='$\sigma=2/3\mu$')

# avg_runtime_minutes = 60 * 2
# gen = workload.WorkloadGenerator(avg_runtime_minutes, avg_runtime_minutes/3., 500)
# gen.t_max=t_max
# ys = gen.generate_workload()
# plt.plot(xs, ys, linestyle='-', color='#bfd3e6', label='$\sigma=180$')

# turn off ticks where there is no spine
ax1.xaxis.set_ticks_position('bottom')
ax1.yaxis.set_ticks_position('left')
ax1.set_xticks([200,400,600,800])
# ax1.set_yticks([500,1000])
# ax1.set_ylim(0, 3.5)
# ax1.set_xlim(0, 100)
# ax1.set_yscale('log')

plt.xlabel('Time [min]')
plt.ylabel('Load')

# http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
for loc, spine in ax1.spines.iteritems():
    if loc in ['left','bottom']:
        pass
    elif loc in ['right','top']:
        spine.set_color('none') # don't draw spine
    else:
        raise ValueError('unknown spine location: %s'%loc)

plt.legend(loc=0, numpoints=1, ncol=1)
leg = plt.gca().get_legend()
leg.draw_frame(False)
ltext = leg.get_texts()
plt.setp(ltext, fontsize=fontsize)
plt.setp(leg.get_frame(), linewidth=0.5)

plt.savefig('workload-rampup-time-margin')
