\chapter{Conclusion}
\label{chapter:conclusion}

Energy efficiency in data centers and energy efficient computing will remain on the agenda in the foreseeable future.
Especially providers of internet-scale services have a monetary interest to decrease their energy use in the absence of clean and cheap energy sources.
The focus of this thesis was to develop techniques and tools to improve the energy efficiency in cloud infrastructures.
In particular, the underlying research question was \emph{if and how hardware virtualization and related technologies can help to decrease the energy consumption}.
Our primary goal was to vacate physical servers and save energy by powering them off.

\newthought{As an introduction} to the topic,
we analyzed usage data of a 50 machine cluster with respect to its energy efficiency (Chapter~\ref{chapter:cluster-trace}).
Given the cluster's low utilization,
we proposed techniques to automatically and transparently power cycle the servers based on their actual usage.
In the context of infrastructure clouds,
we proposed novel VM types and schedulers (Chapter~\ref{chapter:lease-times}).
By optimizing the assignment of virtual to physical machines,
fewer live servers are required to handle the same workload.
We further developed new technologies for truly on-demand cloud computing (Chapter~\ref{chapter:dreamserver}) to switch off idle VMs as a precursor to powering off physical servers.
Last, we built tools for fast and efficient data synchronization (Chapter~\ref{chapter:dsync}).
Frequent data synchronization is necessary when turning servers off:
state stored on local hard disks becomes inaccessible once the server is offline.
Replicating the state to other live servers ensures the data's uninterrupted availability.

Each of the topics is individually summarized in the following sections together with proposals for future work.
A brief outlook on the future of energy efficient computing is given at the very end.

\section{Research Cluster Usage}

In Chapter~\ref{chapter:cluster-trace} we analyzed a research cluster of 50 servers according to its usage and energy efficiency.
More than four years of data revealed an average usage of only 15.09\%.
Accordingly, the energy efficiency is poor,
as the servers are permanently powered on even if nobody uses them.

The workloads and usage patterns suggest that power can be saved with minimal inconvenience to the users by putting unused servers into a low-power mode.
An automated solution, which in its current form only works for virtual environments,
can be set up to automatically and transparently transition the servers into and out of the low-power mode.

\subsection{Future Work}

We want to conduct a detailed analysis of the network traffic within the cluster.
This will allows us to better estimate the achievable power savings.
Even though servers are idle, i.e., no user is logged in,
they still receive periodic messages over the network.
Waking the server up for each message will leads to minimal energy savings.
Exact knowledge about the traffic and its patterns will allow us to determine when waking up the server is strictly necessary.

%% To answer this question, we started by examining the energy use and utilization of a 50-node research cluster in Chapter~\ref{chapter:cluster-trace}.
%% We found that the overall usage was low at 15.09\% and made suggestions on how to improve the energy efficiency of this cluster in the future.
%% In Chapter~\ref{chapter:lease-times} we introduces a new virtual machine type, \emph{timed instances},
%% to optimize the scheduling within infrastructure clouds.
%% Knowing a VM's runtime we co-located VMs with similar termination times.
%% This maximizes the time physical servers are eligible for shutoff,
%% which decreases the energy consumption.

\section[Online Scheduling with Known Resource Reservation Times]
{Online Scheduling with Known Resource Reservation\\
Times}

Cloud computing allows those in need of IT resources to acquire and release them swiftly.
The resources are typically delivered in the form of pre-packaged virtual machines, accessed over the internet.
This flexibility is good for the cloud user,
but also requires careful scheduling at the provider side to optimize his ``bottom line''.
It is unknown to the provider for how long each resource is acquired.
We were curious to see if and how a priori knowledge of the resource lease time helps the cloud provider to optimize the scheduling of resources.

The goal was to reduce the number of required physical machines to service a given workload.
Knowing the lease time, allows the scheduler to incorporate it into its decision making process.
The general idea is to co-locate virtual machines with coinciding termination times.
A physical server can only be turned off once all virtual machines are shut down.
Hence, aligning the VM's termination times creates more opportunities to shutdown the physical servers.

Through simulations across a diverse parameter space we established that reductions in physical machine uptime of up to 51.5\% are possible in very favorable scenarios.
The savings depend on a wide array of parameter, such as runtime distribution, VM sizes, data center sizes, server capacity, VM purchase types, and batch requests.
If the savings observed in our simulations would actually materialize in practice, strongly depends on how close our input parameters match those of real deployments.
As very little data of real-world infrastructure clouds is publicly available,
we cannot make an informed statement regarding potential real-world savings.

\subsection{Future work}

As an interesting extension we have identified the interplay between online scheduling and planning.
In online scheduling, decisions have to be made instantaneously.
For example, in the case of virtual machine scheduling,
the user's request must be processed as soon as possible to provide a satisfactory user experience.
On the other hand, planning does not have to operate within those tight time constraints.
In planning, all the inputs are known a priori and the time to come up with a good schedule is measured in minutes to hours, sometimes days.
The research question then becomes:
how much does scheduling improve if we allow for a certain slack time.
Instead of processing every resource request as it arrives,
requests are collected over the course of several minutes.
This gives the scheduler more time to compute an assignment as well as enables consider multiple requests at once.

\section[On-Demand Resource Provisioning in Cloud Environments]
{On-Demand Resource Provisioning in Cloud\\
Environments}

Virtualization in cloud data centers is used to isolate customers from each other and to provide a self-contained execution environment.
These virtual machines can be suspended,
halting their execution and storing all their associated state on persistent storage.
Suspending an idle VM frees up resources to be used by other active instances.
Once all VMs are either migrated away or suspended,
the physical server is shut down to save energy.

A challenge with suspending VMs is to do so transparently, i.e.,
their network presence must be maintained.
To this end, we proposed an overall architecture to monitor incoming requests,
either using an application layer proxy or an alternative SDN-based solution.
In cases where the VM must be resumed,
our modifications to the QEMU virtual machine monitor ensure that VM is responsible again quickly.

Our benchmarks revealed best case end-to-end request latencies as low as 0.8 seconds for the first request after a resume.
Depending on the application running within the VM,
we also observed worst case resume latencies of more than 2 seconds.

\subsection{Future work}

In the area of on-demand resource provisioning we want to expand from only managing virtual resources to managing the power of physical machines.
In Chapter~\ref{chapter:cluster-trace} we presented a usage analysis of a 50 node research cluster.
Before actually deploying our solution to manage the power of those 50 servers,
we first have to perform a more detailed network traffic analysis.
A deep understanding of the applications using the network is essential to estimate the potential for powering down servers.
Even a seemingly idle machine receives multiple packets per second.
Discerning between essential and non-essential packets is important to power off servers for reasonable time periods.

\section{Efficient Block-Level Synchronization of Binary Data}

When turning off physical servers to save energy,
the server's locally attached disks and all the data on them are no longer accessible.
In the case of a cloud infrastructure,
that state might include virtual machine disks.
To be able to start the VMs again on an alternative host,
the local state must be replicated to another live server.

With our ``dsync'' system, we presented a tool to efficiently synchronize data at the block-level.
Instead of relying on computationally expensive checksums to determine the updated state,
``dsync'' actively tracks modifications.
While some recent file systems poses similar functionality by means of differential snapshots,
implementing it at the block device layer makes this technique independent of the file system.
Regardless of whether a particular file system supports it out-of-the box,
dsync enables delta updates generically for all block devices.

We implemented the modification tracking functionality as a Linux kernel module.
The additional code is small, few hundred lines of code,
and localized.
Our empirical measurement with different hardware setups showed that dsync performs at least as good as its competitors.

\subsection{Future work}

For \texttt{dsync} it would be interesting so see if and how Bloom filters affect the memory overhead.
Currently, \texttt{dsync} uses one bit per $4\,\textrm{KiB}$ block arranged in a simple bit vector.
For large storage arrays, the memory overhead may become a problem,
which warrants a look at alternative data structures.
On the practical side, getting the source code accepted into the mainline Linux kernel is also on our agenda.

\section{Outlook}

In the absence of cheap and clean power sources,
energy will continue to be on the agenda for data center operators,
not just for economic reasons.
In the future, we see the biggest opportunity to save energy by furthering the adoption of cloud computing in general.
Instead of multiple cabinet-sized installations,
a single large data center is much more energy efficient.
All the supporting infrastructure, such as uninterruptible power supplies, backup generators, and the cooling system,
is typically more efficient in their XL-version.
Sharing a data center between multiple parties reduces the cost of constructing and operating the infrastructure for each individual partner.

The challenge is to convince management and CTOs that moving the IT infrastructure into ``the cloud'',
is no less secure than running it in on premise.
This will take more time,
as decision makers are still wary of other people snooping on their confidential and business-critical data.
Better technologies are required to give strong guarantees that no unauthorized party can access your private data in the shared environment.

\newthought{Once all the world's} computing is performed in the cloud,
better scheduling algorithms will decrease the total number of live servers.
Google is already pushing the boundaries in this regard.
Since their data center report from 2008,
the average server utilization has allegedly climbed from 20-30\% to around 50\%.
The goal here is to create a generic infrastructure to run any kind of job on;
be it an internal MapReduce job to invert the web index or a new virtual machine for one of the cloud customers.
mixing and matching jobs with complementing properties is the key to success in this regard.
