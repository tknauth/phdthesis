\chapter{Research Cluster Usage Analysis}
\label{chapter:cluster-trace}

\section{Introduction}

With energy being a central concern of our work,
we naturally asked how well we ourselves are doing in terms of energy efficiency.
The System's Engineering research group owns, administers, and uses a small cluster of 50 machines.
PhD students and associated members of the group use the cluster to run experiments for their research.
The cluster consists of 50 blade servers distributed across three racks,
which are housed within the university's high performance computing center.
The machines are powered on 24/7, irrespective of any actual user activity.

Each machine has two 4-core Intel Xeon E5405 CPUs, $8\,\textrm{GiB}$ of memory, two network interfaces, and a single HDD.
The idle power consumption for a single server,
as reported by the power distribution unit~(PDU),
is approximately $120-130\,\textrm{Watt}$.
No power management is performed, not even dynamic frequency or voltage scaling.

For a single day, the cluster's baseline energy consumption is \(150\,\textrm{kWh}\);
50 servers consuming \(125\,\textrm{Watt}\) for 24 hours.
The energy consumed during a whole year, roughly \(55\,\textrm{MWh}\),
is equivalent to the consumption of about 16 average German households during the same period~\sidenote[][-2cm]{http://shrinkthatfootprint.com/average-household-electricity-consumption}.
The German Federal Statistical Office publishes statistics on the average energy price for industrial consumers in Germany.
Table~\ref{table:cluster-trace:energy-price} shows the energy prices from the 2nd half of 2009 up to the 2nd half of 2013,
the most recent period for which they had published data at the point of writing this thesis.
Using the most recently published energy price of $12.73\,\textrm{Cents/kWh}$ as a basis,
it currently costs \(19.10\,\textrm{EUR}\) per day to power the 50 servers.
%% (125*24*50)/1000.*0.1273
Over its entire life span of 4.5 years the total server energy cost was at least \(30,000\,\textrm{EUR}\),
%% 6*30.5*24*125*50/1000.*(9.93+10.58+11.21+11.39+11.45+11.69+12.78+12.73+12.73)/100.+4*30.5*24*125*50/1000.*10.07/100.
based on the historic prices as reported by the German Federal Statistical Office.
The total energy to operate the cluster is of course higher,
as focusing only on the servers ignores auxiliary power consumers,
such as, for example, the network switches and the cooling infrastructure.

\begin{margintable}[-7.5cm]
\begin{tabular}{rl}
Period & Cents/kWh\\
\hline
2nd half 2009 & 10.07\\
1st half 2010 & 9.93\\
2nd half 2010 & 10.58\\
1st half 2011 & 11.21\\
2nd half 2011 & 11.39\\
1st half 2012 & 11.45\\
2nd half 2012 & 11.69\\
1st half 2013 & 12.78\\
2nd half 2013 & 12.73\\
\end{tabular}
\caption[Energy prices in Germany]{Average industrial energy prices (including excise duties, excluding VAT).
Data available from \url{https://www.destatis.de/DE/Publikationen/Thematisch/Preise/Energiepreise/EnergyPriceTrendsPDF_5619002.pdf?__blob=publicationFile}}
\label{table:cluster-trace:energy-price}
\end{margintable}

%% Energy price
%% https://www.destatis.de/DE/Publikationen/Thematisch/Preise/Energiepreise/EnergyPriceTrendsPDF_5619002.pdf?__blob=publicationFile
%% http://www.spiegel.de/international/germany/high-costs-and-errors-of-german-transition-to-renewable-energy-a-920288-2.html
%%  http://epp.eurostat.ec.europa.eu/portal/page/portal/energy/data/main_tables

% Calculating the overall energy price paid by consumers,
% be they private or industrial,
% is complicated by the various fees, levies, and taxes imposed on the market price.
% The European Commission maintains and publishes energy prices for the various member states,
% but the quoted prices are net prices, i.e.,
% excluding fees, levies, and taxes~\footnote{\url{http://epp.eurostat.ec.europa.eu/portal/page/portal/energy/data/main_tables}}.

According to an analysis by \citeauthor{barroso2009datacenter}~\cite[-0.4cm]{barroso2009datacenter},
the server power only accounts for 3--11\% of the total data center operating cost.
From a budgetary standpoint, the server energy costs may not worry the individual data center operator.
However, from the EPA report on Server and Data Center Energy Efficiency~\cite{epa-datacenter-report} we also know that the total energy consumption for ``server room''-sized clusters was on the order of 10 billion ($10^{12}$) kWh for the US alone in 2006.
The overall consumption of small scale clusters will only have grown since then,
making their aggregate consumption an even more worthwhile target for optimization.
At the scale of billions of kilowatt hours,
reducing the consumption only by a few percent will save millions of dollars in electricity cost and reduce CO2 emissions at the same time.

\section{Analysis} 

To determine the cluster usage statistics,
we analyzed machine reservation information maintained as entries in a shared Google Calendar.
Our research group coordinates access to the cluster machines using \emph{advisory locks}.
Even though the locks are advisory, i.e., a user can still log in and start processes without creating a calendar entry first,
users have an incentive to be honest about their intended machine usage.
If user A uses the machines without creating a reservation first,
he risks to disturb  other users.
Hence, even if user A does not require exclusive access to the machines,
it is still common practice to make reservations to prevent interference.
\footnote{One anonymous reviewer wrote: ``This reviewer found the notion of honest reservations
surprising from a game-theoretic perspective. An agent who accesses
the system without a reservation imposes all the costs on others and
receives all the utility.'' Well, the agent which does not duly and honestly mark his reservations will soon suffer the wrath of the other agents whose benchmarks were invalidated.}

\begin{marginfigure}
  \includegraphics[width=1.2in]{figs/google-calendar}
  \caption[Machine reservations as calendar entries]{Screenshot showing machine reservations as entries in a shared Google Calendar.}
  \label{fig:intro:reservation-calendar}
\end{marginfigure}

The shared calendar approach has some key benefits:
it is easy to create new and view existing reservations.
The user only needs a web browser, which means it is cross-platform and no tools must be installed on a user's machine.
Once a reservation is made, the user has guaranteed access to the resources.
One common complain about batch queuing systems is the lack of control over when exactly jobs are run and results can be expected.
With explicit reservations, it is each user's responsibility to reserve resources long enough for the tasks to finish within the reserved time slot.

If the number of users grows further, and contention issues arise,
we might eventually switch to a proper cluster manager and/or batch queuing system, for example, LSF.
So far, the group felt this was unnecessary.

\newthought{When talking about usage} and utilization we must be aware that the data actually only represents \emph{resource reservations}.
We do not have any information about the actual CPU, memory, network, or disk utilization for the reserved time periods.
It is still instructive to analyze the reservation data,
as it yields information on the time periods the servers were not used at all.
While we do not have any information concerning actual usage during for the reserved time periods,
there is no doubt that for time periods without any reservation,
the corresponding server was unused.
Hence, utilization or usage in the context of our analysis refers to the time frames for which machine reservations exist.

\pagebreak

The data set consists of about 4100 reservations, starting from late 2009 up to the present day,
representing about 4.5 years of data.
Figure~\ref{fig:intro:reservation-calendar} shows a screenshot clip from the Google Calendar to coordinate access.
Each reservation contains four basic pieces of information:
(a)~the start time, (b)~end time, (c)~user, and (d)~the machines.
In Figure~\ref{fig:intro:reservation-calendar}, for example,
user ``lenar'' reserves machines ``stream26--28'' from 09:30 to 13:30.
Based on those four items, we can perform a number of interesting analyses.

\begin{figure}[t]
  \includegraphics{figs/intro/usage-per-week}
  \caption[Weekly server utilization]{Weekly utilization for the entire trace period.
  Long stretches of low utilization are interspersed with brief periods of heightened utilization.
  The overall utilization is at 15.09\%.}
  \label{fig:intro:usage-per-week}
\end{figure}

First we determine the overall \emph{cluster usage}.
As discussed earlier, we refrain from calling it cluster utilization because we only have access to the reservation data.
The actual utilization of individual resources such as disk, network, and CPU, is unknown to us.
We calculate the cluster usage as the fraction of reserved machine time and available machine time.

$$\textrm{cluster usage} = \frac{\textrm{reserved machine time}}{\textrm{available machine time}}$$

We decided to plot the cluster usage on a weekly basis for the entire trace period.
A week-by-week breakdown is a reasonable compromise between granularity and the resulting number of data points.
Figure~\ref{fig:intro:usage-per-week} shows the usage over time for the 228 week-long observation period.
While the overall usage is low with 15.09\%,
this falls squarely into the range of previously published utilization statistics~\cite[-0.2cm]{barroso2009datacenter, cloud-factories, birke2012data}.
Because usage is an upper bound of the utilization, 
the cluster's real utilization is actually at the lower end of the IT industry's average.

%% Peaks correspond to project/paper deadlines.
The brief periods, sometimes only a single week,
where usage peaks at 40\% or more,
coincide with EU project or paper deadlines,
when people have to run lots of last minute experiments or prepare project demonstrators.
We confirmed this by talking to the persons responsible for the majority of the reservations during those periods and cross-referencing with our group's internal ``deadline calendar''.
For example, in early 2010, when usage peaked at close to 100\%,
a fellow PhD student used the cluster to participate in the yearly TeraSort (\url{http://sortbenchmark.org/}) competition.

We also looked at the number of servers and the time periods that people reserve machines for.
Figure~\ref{fig:intro:reservation-size-and-length} plots the result as two bar graphs.
For typical reservation sizes there is a cluster from a single machine up to 11 machines,
with further peaks at 21, 31, 41, and 50 servers.
The popularity of single machines can be explained by people debugging code or doing interactive development within the environment their code will eventually run in.
Reserving smaller numbers of machines is also easier than booking the entire cluster,
which explains the popularity of reservation sizes of up to and including 11 machines.
In fact, 53.6\% of the reservations are for 11 or fewer servers.
 
The peaks at 21, 31, and 41 server are best explained by people's laziness:
typing ``10--20'' requires slightly fewer finger movements than the probably intended alternative, ``10--19'',
for reserving exactly ten machines.
The human bias to favor exact multiples and ``round numbers'' was previously speculated on by \citealp[p. 10, sec. 3.2.3]{reiss2012trace}~\cite{reiss2012trace} in the context of task sizes for map-reduce computations.

\begin{figure*}[t]
  \includegraphics{figs/intro/reservation-size}
  \includegraphics{figs/intro/reservation-length}
  \caption[Distribution of reservation sizes and reservation lengths]{Distribution of reservation sizes (left).
Reserving sets of 9, 10, 31, and 50 machines is particularly popular.
In the reservation length histogram (right) each bar represents a 15 minute interval of the form $[t,t+15)$.
Reservations lasting for one hour are by far the most popular.}
  \label{fig:intro:reservation-size-and-length}
  \vspace{-0.2cm}
\end{figure*}

Turning to the time periods shows that 22\% of reservations are for periods of one hour.
The most likely explanation is that one hour is the default length when creating a new calendar entry within Google Calendar.
Even if users only require access to the machines for a shorter time frame,
they probably are too lazy to decrease the reservation length,
especially if resource contention is low.
Noteworthy are the 5\% reservation for time periods of up to 15 minutes (leftmost bar):
recently, people have started to automatically reserve resources to run continuous integration tests.
As these reservations are scripted,
it is easier to specify intervals that are not multiples of one hour.
Another effect of the Google Calendar interface is the lack of reservations between 15 to 30 minutes and 45 to 60 minutes,
plus their hourly offsets, e.g., 1:15 to 1:30, 1:45 to 2:00, and so on.
Changing the duration of a Google Calendar entry via the browser interface happens in 30 minute steps.
That is, adjusting the reservation to last for 1.5 hours instead of 1 hour is easily done by pointing, clicking, and dragging.
But setting an entry to 1 hour and 15 minutes would require more steps,
possibly involving the keyboard,
which is just too much inconvenience.

%% Why is it interesting so see how long machines are typically idle for?
\begin{figure}[t]
  \includegraphics{figs/intro/inter-reservation-times}
  \caption[Distribution of times between successive reservations]{Distribution of times between successive reservations.
In 20\% of all cases, the idle time is less than one hour (leftmost bar).
The peak for the $[23, 24)$-hour interval is due to daily integration tests being executed on the cluster in recent months.}
  \label{fig:intro:inter-reservation-times}
\end{figure}

Another metric one might be interested in is how much time typically passes between two successive uses of a single machine.
Put differently, this is the expected time for which servers could be transitioned into low-power sleep states.
Figure~\ref{fig:intro:inter-reservation-times} shows a histogram of the data with hour-long intervals, i.e.,
the first bar includes reservations with a gap of 0 up to 59 minutes,
the second bar of 60--119 minutes, and so on.
We make a number of interesting observations:
first, most reservations, about 20 percent,
happen within less than one hour of each other, i.e.,
servers are unused for less than one hour between two successive reservations.
In fact, 12.3 percent of the reservations are back-to-back without any spare time in between.
We also find that the time between two reservations does not exceed 24 hours in 85.3 percent of the cases.
Conversely, in almost 15\% of the cases servers are idle, i.e., no reservation exists,
for more than 24 hours.

The peak for the $[23,24)$-hour interval in Figure~\ref{fig:intro:inter-reservation-times} is again due to daily integration tests.
It turns out that a single user is responsible for most of the reservations in this particular bin.
As mentioned earlier, people have started using the cluster to run daily automated integration tests.
Those tests last less than an hour and are executed daily.
Hence the peak for inter-reservation times falling into the \([23,24)\) hours interval.

We also note that the distribution of inter-reservation times has a long tail.
While the time between server reservations is less than 24 hours for 85.3 percent of reservations,
in 14.7 percent of the cases a machine is unused for 24 hours and longer.
Particularly, the $95th$ percentile is $88.5\,\textrm{hours}$,
still covered in Figure~\ref{fig:intro:inter-reservation-times}, but barely discernible.
The $99th$ percentile is a bit more than 12 days.

\section{Conclusion}

Based on the analysis of our cluster's usage we conclude that there is ample opportunity to exploit existing coarse grain power saving modes,
for example, suspend-to-disk or suspend-to-memory, available to us.
As the servers in our cluster are unused for hours, up to days,
it appears acceptable to have delays on the order of a few seconds to transition the servers into and out of full-system idle low-power modes.

One solution to transparently activate virtual servers was developed as part of this thesis.
Chapter~\ref{chapter:dreamserver} will present a system to activate and deactivate virtual machines based on their observed network (in)activity.
The mechanism can be extended to work with physical servers too.
On the theory side a deeper analysis of the network traffic is needed to understand which network protocols might require the server to be up.
Frequent wakeups shorten the sleep intervals and reduce the achievable energy savings.
Low level network protocols, for example, group membership protocols, and periodic application-level ``pings'',
for example, by infrastructure monitoring tools,
may prevent an otherwise idle system to enter a low-power state.

Based on the usage data and patterns found in our cluster,
we are confident that extending the technologies to be presented in Chapter~\ref{chapter:dreamserver} to physical server,
will enable us to save significant amounts of energy in our research cluster.
At the same time, usability is preserved because the mechanisms to enter and resume the servers from power-saving modes are transparent to the user.
